# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QWidget
from PySide2.QtGui import QPainter, QColor, QPen
from PySide2.QtCore import Qt

class MyWidget(QWidget):
    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        # 设置绘制矩形的样式
        pen = QPen(Qt.blue)
        pen.setWidth(2)
        painter.setPen(pen)

        # 绘制矩形
        rect = self.rect().adjusted(10, 10, -10, -10)  # 调整矩形的大小
        painter.drawRect(rect)