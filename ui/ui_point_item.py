# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'point_item.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QHBoxLayout, QLabel,
    QPushButton, QRadioButton, QSizePolicy, QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(585, 286)
        self.horizontalLayoutWidget = QWidget(Form)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 20, 481, 171))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.radioButton_id = QRadioButton(self.horizontalLayoutWidget)
        self.radioButton_id.setObjectName(u"radioButton_id")
        self.radioButton_id.setStyleSheet(u"color:red")

        self.horizontalLayout.addWidget(self.radioButton_id)

        self.label_id = QLabel(self.horizontalLayoutWidget)
        self.label_id.setObjectName(u"label_id")

        self.horizontalLayout.addWidget(self.label_id)

        self.label_location = QLabel(self.horizontalLayoutWidget)
        self.label_location.setObjectName(u"label_location")

        self.horizontalLayout.addWidget(self.label_location)

        self.comboBox_type = QComboBox(self.horizontalLayoutWidget)
        self.comboBox_type.addItem("")
        self.comboBox_type.addItem("")
        self.comboBox_type.addItem("")
        self.comboBox_type.setObjectName(u"comboBox_type")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox_type.sizePolicy().hasHeightForWidth())
        self.comboBox_type.setSizePolicy(sizePolicy)
        self.comboBox_type.setMinimumSize(QSize(80, 0))
        self.comboBox_type.setMaximumSize(QSize(80, 16777215))

        self.horizontalLayout.addWidget(self.comboBox_type)

        self.pushButton_redraw_point = QPushButton(self.horizontalLayoutWidget)
        self.pushButton_redraw_point.setObjectName(u"pushButton_redraw_point")
        self.pushButton_redraw_point.setMinimumSize(QSize(50, 0))

        self.horizontalLayout.addWidget(self.pushButton_redraw_point)

        self.horizontalLayout.setStretch(1, 10)
        self.horizontalLayout.setStretch(3, 20)
        self.horizontalLayout.setStretch(4, 5)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.radioButton_id.setText("")
        self.label_id.setText(QCoreApplication.translate("Form", u"id", None))
        self.label_location.setText(QCoreApplication.translate("Form", u"location", None))
        self.comboBox_type.setItemText(0, QCoreApplication.translate("Form", u"0:\u5185", None))
        self.comboBox_type.setItemText(1, QCoreApplication.translate("Form", u"1: \u5916", None))
        self.comboBox_type.setItemText(2, QCoreApplication.translate("Form", u"2: \u7a7a", None))

        self.pushButton_redraw_point.setText(QCoreApplication.translate("Form", u"\u91cd\u753b", None))
    # retranslateUi

