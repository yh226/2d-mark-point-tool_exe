# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file '2D_mark_point_mainlsEFhf.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form_2D_mark_point_main(object):
    def setupUi(self, Form_2D_mark_point_main):
        if not Form_2D_mark_point_main.objectName():
            Form_2D_mark_point_main.setObjectName(u"Form_2D_mark_point_main")
        Form_2D_mark_point_main.resize(1559, 1092)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form_2D_mark_point_main.sizePolicy().hasHeightForWidth())
        Form_2D_mark_point_main.setSizePolicy(sizePolicy)
        self.action_set_point = QAction(Form_2D_mark_point_main)
        self.action_set_point.setObjectName(u"action_set_point")
        self.actionqq = QAction(Form_2D_mark_point_main)
        self.actionqq.setObjectName(u"actionqq")
        self.action_history_show = QAction(Form_2D_mark_point_main)
        self.action_history_show.setObjectName(u"action_history_show")
        self.action_new = QAction(Form_2D_mark_point_main)
        self.action_new.setObjectName(u"action_new")
        self.actiond_show_tools = QAction(Form_2D_mark_point_main)
        self.actiond_show_tools.setObjectName(u"actiond_show_tools")
        self.action_save = QAction(Form_2D_mark_point_main)
        self.action_save.setObjectName(u"action_save")
        self.action_help = QAction(Form_2D_mark_point_main)
        self.action_help.setObjectName(u"action_help")
        self.action_set_mod = QAction(Form_2D_mark_point_main)
        self.action_set_mod.setObjectName(u"action_set_mod")
        self.centralwidget = QWidget(Form_2D_mark_point_main)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_11 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_11.setSpacing(0)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.horizontalLayout_11.setContentsMargins(0, 0, 0, 0)
        self.dockWidget_tools = QDockWidget(self.centralwidget)
        self.dockWidget_tools.setObjectName(u"dockWidget_tools")
        self.dockWidget_tools.setEnabled(True)
        self.dockWidgetContents_2 = QWidget()
        self.dockWidgetContents_2.setObjectName(u"dockWidgetContents_2")
        self.verticalLayout_12 = QVBoxLayout(self.dockWidgetContents_2)
        self.verticalLayout_12.setSpacing(0)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.groupBox_file = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_file.setObjectName(u"groupBox_file")
        font = QFont()
        font.setBold(True)
        font.setWeight(75)
        self.groupBox_file.setFont(font)
        self.verticalLayout = QVBoxLayout(self.groupBox_file)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.lineEdit_folder = QLineEdit(self.groupBox_file)
        self.lineEdit_folder.setObjectName(u"lineEdit_folder")
        self.lineEdit_folder.setEnabled(True)
        font1 = QFont()
        font1.setBold(False)
        font1.setWeight(50)
        self.lineEdit_folder.setFont(font1)
        self.lineEdit_folder.setReadOnly(False)

        self.verticalLayout.addWidget(self.lineEdit_folder)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setSpacing(0)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.pushButton_new = QPushButton(self.groupBox_file)
        self.pushButton_new.setObjectName(u"pushButton_new")
        self.pushButton_new.setFont(font1)

        self.horizontalLayout_8.addWidget(self.pushButton_new)

        self.checkBox_read_only = QCheckBox(self.groupBox_file)
        self.checkBox_read_only.setObjectName(u"checkBox_read_only")
        self.checkBox_read_only.setFont(font1)

        self.horizontalLayout_8.addWidget(self.checkBox_read_only)

        self.horizontalLayout_8.setStretch(0, 90)

        self.verticalLayout.addLayout(self.horizontalLayout_8)

        self.pushButton_load_point_result = QPushButton(self.groupBox_file)
        self.pushButton_load_point_result.setObjectName(u"pushButton_load_point_result")
        self.pushButton_load_point_result.setEnabled(True)
        self.pushButton_load_point_result.setFont(font1)

        self.verticalLayout.addWidget(self.pushButton_load_point_result)


        self.verticalLayout_12.addWidget(self.groupBox_file)

        self.groupBox_image_tools = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_image_tools.setObjectName(u"groupBox_image_tools")
        self.groupBox_image_tools.setFont(font)
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_image_tools)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.radioButton_inherit_scale = QRadioButton(self.groupBox_image_tools)
        self.radioButton_inherit_scale.setObjectName(u"radioButton_inherit_scale")
        self.radioButton_inherit_scale.setFont(font1)

        self.verticalLayout_2.addWidget(self.radioButton_inherit_scale)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setSpacing(0)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.label_10 = QLabel(self.groupBox_image_tools)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font1)
        self.label_10.setAlignment(Qt.AlignCenter)

        self.verticalLayout_10.addWidget(self.label_10)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_angle_value = QLabel(self.groupBox_image_tools)
        self.label_angle_value.setObjectName(u"label_angle_value")
        sizePolicy.setHeightForWidth(self.label_angle_value.sizePolicy().hasHeightForWidth())
        self.label_angle_value.setSizePolicy(sizePolicy)
        self.label_angle_value.setMaximumSize(QSize(16777215, 30))
        self.label_angle_value.setFont(font1)
        self.label_angle_value.setTextFormat(Qt.AutoText)
        self.label_angle_value.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_10.addWidget(self.label_angle_value)

        self.label_12 = QLabel(self.groupBox_image_tools)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setFont(font1)

        self.horizontalLayout_10.addWidget(self.label_12)


        self.verticalLayout_10.addLayout(self.horizontalLayout_10)

        self.dial_rotate = QDial(self.groupBox_image_tools)
        self.dial_rotate.setObjectName(u"dial_rotate")
        sizePolicy.setHeightForWidth(self.dial_rotate.sizePolicy().hasHeightForWidth())
        self.dial_rotate.setSizePolicy(sizePolicy)
        self.dial_rotate.setMaximumSize(QSize(16777215, 70))
        self.dial_rotate.setMaximum(360)
        self.dial_rotate.setSingleStep(1)
        self.dial_rotate.setPageStep(10)
        self.dial_rotate.setValue(0)
        self.dial_rotate.setOrientation(Qt.Horizontal)

        self.verticalLayout_10.addWidget(self.dial_rotate)


        self.horizontalLayout_9.addLayout(self.verticalLayout_10)

        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.label = QLabel(self.groupBox_image_tools)
        self.label.setObjectName(u"label")
        self.label.setFont(font1)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.label)

        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setSpacing(0)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.label_zoom_value = QLabel(self.groupBox_image_tools)
        self.label_zoom_value.setObjectName(u"label_zoom_value")
        sizePolicy.setHeightForWidth(self.label_zoom_value.sizePolicy().hasHeightForWidth())
        self.label_zoom_value.setSizePolicy(sizePolicy)
        self.label_zoom_value.setMaximumSize(QSize(16777215, 30))
        self.label_zoom_value.setFont(font1)
        self.label_zoom_value.setTextFormat(Qt.AutoText)
        self.label_zoom_value.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_12.addWidget(self.label_zoom_value)

        self.label_13 = QLabel(self.groupBox_image_tools)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setFont(font1)

        self.horizontalLayout_12.addWidget(self.label_13)


        self.verticalLayout_11.addLayout(self.horizontalLayout_12)

        self.dial_zoom = QDial(self.groupBox_image_tools)
        self.dial_zoom.setObjectName(u"dial_zoom")
        self.dial_zoom.setEnabled(True)
        sizePolicy.setHeightForWidth(self.dial_zoom.sizePolicy().hasHeightForWidth())
        self.dial_zoom.setSizePolicy(sizePolicy)
        self.dial_zoom.setMaximumSize(QSize(16777215, 70))
        self.dial_zoom.setMinimum(50)
        self.dial_zoom.setMaximum(500)
        self.dial_zoom.setSingleStep(10)
        self.dial_zoom.setValue(100)
        self.dial_zoom.setSliderPosition(100)
        self.dial_zoom.setOrientation(Qt.Horizontal)

        self.verticalLayout_11.addWidget(self.dial_zoom)


        self.horizontalLayout_9.addLayout(self.verticalLayout_11)


        self.verticalLayout_2.addLayout(self.horizontalLayout_9)

        self.pushButton_roate_left = QPushButton(self.groupBox_image_tools)
        self.pushButton_roate_left.setObjectName(u"pushButton_roate_left")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.pushButton_roate_left.sizePolicy().hasHeightForWidth())
        self.pushButton_roate_left.setSizePolicy(sizePolicy1)
        self.pushButton_roate_left.setMaximumSize(QSize(0, 0))

        self.verticalLayout_2.addWidget(self.pushButton_roate_left)

        self.pushButton_roate_right = QPushButton(self.groupBox_image_tools)
        self.pushButton_roate_right.setObjectName(u"pushButton_roate_right")
        sizePolicy1.setHeightForWidth(self.pushButton_roate_right.sizePolicy().hasHeightForWidth())
        self.pushButton_roate_right.setSizePolicy(sizePolicy1)
        self.pushButton_roate_right.setMaximumSize(QSize(0, 0))

        self.verticalLayout_2.addWidget(self.pushButton_roate_right)

        self.pushButton_reset_view = QPushButton(self.groupBox_image_tools)
        self.pushButton_reset_view.setObjectName(u"pushButton_reset_view")
        self.pushButton_reset_view.setFont(font1)

        self.verticalLayout_2.addWidget(self.pushButton_reset_view)


        self.verticalLayout_12.addWidget(self.groupBox_image_tools)

        self.groupBox_ai_control = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_ai_control.setObjectName(u"groupBox_ai_control")
        self.groupBox_ai_control.setFont(font)
        self.verticalLayout_9 = QVBoxLayout(self.groupBox_ai_control)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.pushButton_ai_start = QPushButton(self.groupBox_ai_control)
        self.pushButton_ai_start.setObjectName(u"pushButton_ai_start")
        self.pushButton_ai_start.setFont(font1)

        self.verticalLayout_9.addWidget(self.pushButton_ai_start)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setSpacing(0)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.radioButton_full_speed = QRadioButton(self.groupBox_ai_control)
        self.radioButton_full_speed.setObjectName(u"radioButton_full_speed")
        self.radioButton_full_speed.setFont(font1)
        self.radioButton_full_speed.setChecked(True)

        self.horizontalLayout_14.addWidget(self.radioButton_full_speed)

        self.radioButton_half_speed = QRadioButton(self.groupBox_ai_control)
        self.radioButton_half_speed.setObjectName(u"radioButton_half_speed")
        self.radioButton_half_speed.setFont(font1)

        self.horizontalLayout_14.addWidget(self.radioButton_half_speed)

        self.pushButton_ai_draw = QPushButton(self.groupBox_ai_control)
        self.pushButton_ai_draw.setObjectName(u"pushButton_ai_draw")
        self.pushButton_ai_draw.setFont(font1)

        self.horizontalLayout_14.addWidget(self.pushButton_ai_draw)

        self.horizontalLayout_14.setStretch(0, 10)
        self.horizontalLayout_14.setStretch(1, 10)
        self.horizontalLayout_14.setStretch(2, 80)

        self.verticalLayout_9.addLayout(self.horizontalLayout_14)


        self.verticalLayout_12.addWidget(self.groupBox_ai_control)

        self.groupBox_traget_ai = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_traget_ai.setObjectName(u"groupBox_traget_ai")
        self.groupBox_traget_ai.setFont(font)
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_traget_ai)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setSpacing(0)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.label_3 = QLabel(self.groupBox_traget_ai)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font1)

        self.horizontalLayout_13.addWidget(self.label_3)

        self.label_ai_mod = QLabel(self.groupBox_traget_ai)
        self.label_ai_mod.setObjectName(u"label_ai_mod")
        self.label_ai_mod.setFont(font1)

        self.horizontalLayout_13.addWidget(self.label_ai_mod)

        self.horizontalLayout_13.setStretch(0, 10)
        self.horizontalLayout_13.setStretch(1, 90)

        self.verticalLayout_5.addLayout(self.horizontalLayout_13)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setSpacing(0)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.label_11 = QLabel(self.groupBox_traget_ai)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font1)

        self.horizontalLayout_15.addWidget(self.label_11)


        self.verticalLayout_5.addLayout(self.horizontalLayout_15)

        self.listWidget_target_ai = QListWidget(self.groupBox_traget_ai)
        self.listWidget_target_ai.setObjectName(u"listWidget_target_ai")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.listWidget_target_ai.sizePolicy().hasHeightForWidth())
        self.listWidget_target_ai.setSizePolicy(sizePolicy2)
        self.listWidget_target_ai.setMinimumSize(QSize(0, 0))
        self.listWidget_target_ai.setMaximumSize(QSize(16777215, 100))
        self.listWidget_target_ai.setFont(font1)

        self.verticalLayout_5.addWidget(self.listWidget_target_ai)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setSpacing(0)
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.pushButton_add_yolo_rectangle = QPushButton(self.groupBox_traget_ai)
        self.pushButton_add_yolo_rectangle.setObjectName(u"pushButton_add_yolo_rectangle")
        self.pushButton_add_yolo_rectangle.setFont(font1)

        self.horizontalLayout_16.addWidget(self.pushButton_add_yolo_rectangle)

        self.pushButton_delect_target_ai = QPushButton(self.groupBox_traget_ai)
        self.pushButton_delect_target_ai.setObjectName(u"pushButton_delect_target_ai")
        self.pushButton_delect_target_ai.setFont(font1)

        self.horizontalLayout_16.addWidget(self.pushButton_delect_target_ai)


        self.verticalLayout_5.addLayout(self.horizontalLayout_16)

        self.pushButton_new_yolo_ai = QPushButton(self.groupBox_traget_ai)
        self.pushButton_new_yolo_ai.setObjectName(u"pushButton_new_yolo_ai")
        self.pushButton_new_yolo_ai.setFont(font1)

        self.verticalLayout_5.addWidget(self.pushButton_new_yolo_ai)


        self.verticalLayout_12.addWidget(self.groupBox_traget_ai)

        self.groupBox_traget_manual = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_traget_manual.setObjectName(u"groupBox_traget_manual")
        self.groupBox_traget_manual.setFont(font)
        self.verticalLayout_6 = QVBoxLayout(self.groupBox_traget_manual)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 5)
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_6 = QLabel(self.groupBox_traget_manual)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font1)

        self.horizontalLayout_7.addWidget(self.label_6)

        self.comboBox_model = QComboBox(self.groupBox_traget_manual)
        self.comboBox_model.setObjectName(u"comboBox_model")
        self.comboBox_model.setFont(font1)

        self.horizontalLayout_7.addWidget(self.comboBox_model)

        self.horizontalLayout_7.setStretch(0, 10)
        self.horizontalLayout_7.setStretch(1, 90)

        self.verticalLayout_6.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_9 = QLabel(self.groupBox_traget_manual)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font1)

        self.horizontalLayout_5.addWidget(self.label_9)

        self.label_8 = QLabel(self.groupBox_traget_manual)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setFont(font1)

        self.horizontalLayout_5.addWidget(self.label_8)

        self.label_7 = QLabel(self.groupBox_traget_manual)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font1)

        self.horizontalLayout_5.addWidget(self.label_7)

        self.horizontalLayout_5.setStretch(0, 35)
        self.horizontalLayout_5.setStretch(1, 60)
        self.horizontalLayout_5.setStretch(2, 5)

        self.verticalLayout_6.addLayout(self.horizontalLayout_5)

        self.pushButton_target_next = QPushButton(self.groupBox_traget_manual)
        self.pushButton_target_next.setObjectName(u"pushButton_target_next")
        sizePolicy1.setHeightForWidth(self.pushButton_target_next.sizePolicy().hasHeightForWidth())
        self.pushButton_target_next.setSizePolicy(sizePolicy1)
        self.pushButton_target_next.setMaximumSize(QSize(0, 0))

        self.verticalLayout_6.addWidget(self.pushButton_target_next)

        self.pushButton_target_prev = QPushButton(self.groupBox_traget_manual)
        self.pushButton_target_prev.setObjectName(u"pushButton_target_prev")
        sizePolicy1.setHeightForWidth(self.pushButton_target_prev.sizePolicy().hasHeightForWidth())
        self.pushButton_target_prev.setSizePolicy(sizePolicy1)
        self.pushButton_target_prev.setMaximumSize(QSize(0, 0))

        self.verticalLayout_6.addWidget(self.pushButton_target_prev)

        self.listWidget_target_manual = QListWidget(self.groupBox_traget_manual)
        self.listWidget_target_manual.setObjectName(u"listWidget_target_manual")
        sizePolicy2.setHeightForWidth(self.listWidget_target_manual.sizePolicy().hasHeightForWidth())
        self.listWidget_target_manual.setSizePolicy(sizePolicy2)
        self.listWidget_target_manual.setMaximumSize(QSize(16777215, 100))
        self.listWidget_target_manual.setFont(font1)

        self.verticalLayout_6.addWidget(self.listWidget_target_manual)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.pushButton_add_target = QPushButton(self.groupBox_traget_manual)
        self.pushButton_add_target.setObjectName(u"pushButton_add_target")
        self.pushButton_add_target.setFont(font1)

        self.horizontalLayout_4.addWidget(self.pushButton_add_target)

        self.pushButton_remove_target = QPushButton(self.groupBox_traget_manual)
        self.pushButton_remove_target.setObjectName(u"pushButton_remove_target")
        self.pushButton_remove_target.setFont(font1)

        self.horizontalLayout_4.addWidget(self.pushButton_remove_target)


        self.verticalLayout_6.addLayout(self.horizontalLayout_4)


        self.verticalLayout_12.addWidget(self.groupBox_traget_manual)

        self.groupBox_3 = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.groupBox_3.setFont(font)
        self.verticalLayout_4 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_17 = QHBoxLayout()
        self.horizontalLayout_17.setSpacing(0)
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.label_14 = QLabel(self.groupBox_3)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font1)

        self.horizontalLayout_17.addWidget(self.label_14)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_17.addItem(self.horizontalSpacer)

        self.lineEdit_current_target_ai = QLineEdit(self.groupBox_3)
        self.lineEdit_current_target_ai.setObjectName(u"lineEdit_current_target_ai")
        self.lineEdit_current_target_ai.setFont(font1)
        self.lineEdit_current_target_ai.setReadOnly(True)

        self.horizontalLayout_17.addWidget(self.lineEdit_current_target_ai)

        self.horizontalLayout_17.setStretch(0, 1)
        self.horizontalLayout_17.setStretch(1, 6)
        self.horizontalLayout_17.setStretch(2, 93)

        self.verticalLayout_4.addLayout(self.horizontalLayout_17)

        self.horizontalLayout_18 = QHBoxLayout()
        self.horizontalLayout_18.setSpacing(0)
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.label_15 = QLabel(self.groupBox_3)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setFont(font1)

        self.horizontalLayout_18.addWidget(self.label_15)

        self.lineEdit_current_target_manual = QLineEdit(self.groupBox_3)
        self.lineEdit_current_target_manual.setObjectName(u"lineEdit_current_target_manual")
        self.lineEdit_current_target_manual.setFont(font1)
        self.lineEdit_current_target_manual.setReadOnly(True)

        self.horizontalLayout_18.addWidget(self.lineEdit_current_target_manual)

        self.horizontalLayout_18.setStretch(0, 1)
        self.horizontalLayout_18.setStretch(1, 99)

        self.verticalLayout_4.addLayout(self.horizontalLayout_18)

        self.pushButton_compare = QPushButton(self.groupBox_3)
        self.pushButton_compare.setObjectName(u"pushButton_compare")
        self.pushButton_compare.setFont(font1)

        self.verticalLayout_4.addWidget(self.pushButton_compare)


        self.verticalLayout_12.addWidget(self.groupBox_3)

        self.groupBox_image = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_image.setObjectName(u"groupBox_image")
        self.groupBox_image.setFont(font)
        self.groupBox_image.setCheckable(False)
        self.verticalLayout_3 = QVBoxLayout(self.groupBox_image)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.listWidget_image = QListWidget(self.groupBox_image)
        self.listWidget_image.setObjectName(u"listWidget_image")
        sizePolicy.setHeightForWidth(self.listWidget_image.sizePolicy().hasHeightForWidth())
        self.listWidget_image.setSizePolicy(sizePolicy)
        self.listWidget_image.setMaximumSize(QSize(16777215, 200))
        self.listWidget_image.setFont(font1)

        self.verticalLayout_3.addWidget(self.listWidget_image)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton_image_prev = QPushButton(self.groupBox_image)
        self.pushButton_image_prev.setObjectName(u"pushButton_image_prev")
        self.pushButton_image_prev.setFont(font1)

        self.horizontalLayout_2.addWidget(self.pushButton_image_prev)

        self.pushButton_image_next = QPushButton(self.groupBox_image)
        self.pushButton_image_next.setObjectName(u"pushButton_image_next")
        self.pushButton_image_next.setFont(font1)

        self.horizontalLayout_2.addWidget(self.pushButton_image_next)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.pushButton_save = QPushButton(self.groupBox_image)
        self.pushButton_save.setObjectName(u"pushButton_save")
        self.pushButton_save.setFont(font1)

        self.verticalLayout_3.addWidget(self.pushButton_save)

        self.pushButton_save_all = QPushButton(self.groupBox_image)
        self.pushButton_save_all.setObjectName(u"pushButton_save_all")
        self.pushButton_save_all.setFont(font1)

        self.verticalLayout_3.addWidget(self.pushButton_save_all)


        self.verticalLayout_12.addWidget(self.groupBox_image)

        self.dockWidget_tools.setWidget(self.dockWidgetContents_2)

        self.horizontalLayout_11.addWidget(self.dockWidget_tools)

        self.dockWidget_show = QDockWidget(self.centralwidget)
        self.dockWidget_show.setObjectName(u"dockWidget_show")
        self.dockWidgetContents_4 = QWidget()
        self.dockWidgetContents_4.setObjectName(u"dockWidgetContents_4")
        self.verticalLayout_8 = QVBoxLayout(self.dockWidgetContents_4)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.dockWidget_show.setWidget(self.dockWidgetContents_4)

        self.horizontalLayout_11.addWidget(self.dockWidget_show)

        self.dockWidget_history = QDockWidget(self.centralwidget)
        self.dockWidget_history.setObjectName(u"dockWidget_history")
        self.dockWidgetContents_3 = QWidget()
        self.dockWidgetContents_3.setObjectName(u"dockWidgetContents_3")
        self.verticalLayout_7 = QVBoxLayout(self.dockWidgetContents_3)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.tabWidget_history = QTabWidget(self.dockWidgetContents_3)
        self.tabWidget_history.setObjectName(u"tabWidget_history")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.tabWidget_history.sizePolicy().hasHeightForWidth())
        self.tabWidget_history.setSizePolicy(sizePolicy3)
        self.tab_manual = QWidget()
        self.tab_manual.setObjectName(u"tab_manual")
        self.verticalLayout_14 = QVBoxLayout(self.tab_manual)
        self.verticalLayout_14.setSpacing(0)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_19 = QHBoxLayout()
        self.horizontalLayout_19.setSpacing(0)
        self.horizontalLayout_19.setObjectName(u"horizontalLayout_19")
        self.label_16 = QLabel(self.tab_manual)
        self.label_16.setObjectName(u"label_16")

        self.horizontalLayout_19.addWidget(self.label_16)

        self.label_17 = QLabel(self.tab_manual)
        self.label_17.setObjectName(u"label_17")

        self.horizontalLayout_19.addWidget(self.label_17)

        self.label_18 = QLabel(self.tab_manual)
        self.label_18.setObjectName(u"label_18")

        self.horizontalLayout_19.addWidget(self.label_18)

        self.horizontalLayout_19.setStretch(0, 20)
        self.horizontalLayout_19.setStretch(1, 50)
        self.horizontalLayout_19.setStretch(2, 30)

        self.verticalLayout_14.addLayout(self.horizontalLayout_19)

        self.listWidget_point_hostory = QListWidget(self.tab_manual)
        self.listWidget_point_hostory.setObjectName(u"listWidget_point_hostory")

        self.verticalLayout_14.addWidget(self.listWidget_point_hostory)

        self.tabWidget_history.addTab(self.tab_manual, "")
        self.tab_ai = QWidget()
        self.tab_ai.setObjectName(u"tab_ai")
        self.verticalLayout_13 = QVBoxLayout(self.tab_ai)
        self.verticalLayout_13.setSpacing(0)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_13.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_20 = QHBoxLayout()
        self.horizontalLayout_20.setSpacing(0)
        self.horizontalLayout_20.setObjectName(u"horizontalLayout_20")
        self.label_19 = QLabel(self.tab_ai)
        self.label_19.setObjectName(u"label_19")

        self.horizontalLayout_20.addWidget(self.label_19)

        self.label_20 = QLabel(self.tab_ai)
        self.label_20.setObjectName(u"label_20")

        self.horizontalLayout_20.addWidget(self.label_20)

        self.label_21 = QLabel(self.tab_ai)
        self.label_21.setObjectName(u"label_21")

        self.horizontalLayout_20.addWidget(self.label_21)

        self.horizontalLayout_20.setStretch(0, 20)
        self.horizontalLayout_20.setStretch(1, 50)
        self.horizontalLayout_20.setStretch(2, 30)

        self.verticalLayout_13.addLayout(self.horizontalLayout_20)

        self.listWidget_point_hostory_ai = QListWidget(self.tab_ai)
        self.listWidget_point_hostory_ai.setObjectName(u"listWidget_point_hostory_ai")

        self.verticalLayout_13.addWidget(self.listWidget_point_hostory_ai)

        self.tabWidget_history.addTab(self.tab_ai, "")

        self.verticalLayout_7.addWidget(self.tabWidget_history)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pushButton_point_next = QPushButton(self.dockWidgetContents_3)
        self.pushButton_point_next.setObjectName(u"pushButton_point_next")

        self.horizontalLayout_3.addWidget(self.pushButton_point_next)

        self.pushButton_redraw_finish = QPushButton(self.dockWidgetContents_3)
        self.pushButton_redraw_finish.setObjectName(u"pushButton_redraw_finish")

        self.horizontalLayout_3.addWidget(self.pushButton_redraw_finish)

        self.horizontalLayout_3.setStretch(0, 50)
        self.horizontalLayout_3.setStretch(1, 50)

        self.verticalLayout_7.addLayout(self.horizontalLayout_3)

        self.dockWidget_history.setWidget(self.dockWidgetContents_3)

        self.horizontalLayout_11.addWidget(self.dockWidget_history)

        self.horizontalLayout_11.setStretch(1, 70)
        Form_2D_mark_point_main.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(Form_2D_mark_point_main)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1559, 26))
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName(u"menu")
        self.menu_2 = QMenu(self.menubar)
        self.menu_2.setObjectName(u"menu_2")
        self.menu_3 = QMenu(self.menubar)
        self.menu_3.setObjectName(u"menu_3")
        Form_2D_mark_point_main.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(Form_2D_mark_point_main)
        self.statusbar.setObjectName(u"statusbar")
        self.statusbar.setStyleSheet(u"COLOR:rgb(255, 0, 4)")
        Form_2D_mark_point_main.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())
        self.menubar.addAction(self.menu_2.menuAction())
        self.menu.addAction(self.action_new)
        self.menu.addAction(self.action_save)
        self.menu_2.addAction(self.action_set_mod)
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.action_help)
        self.menu_3.addAction(self.action_history_show)
        self.menu_3.addAction(self.actiond_show_tools)

        self.retranslateUi(Form_2D_mark_point_main)

        self.tabWidget_history.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form_2D_mark_point_main)
    # setupUi

    def retranslateUi(self, Form_2D_mark_point_main):
        Form_2D_mark_point_main.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"MainWindow", None))
        self.action_set_point.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6807\u8bb0\u70b9", None))
        self.actionqq.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8fde\u63a5\u7ebf", None))
        self.action_history_show.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u663e\u793a\u5386\u53f2\u8bb0\u5f55", None))
        self.action_new.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65b0\u5efa(ctrl+N)", None))
        self.actiond_show_tools.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u663e\u793a\u5de5\u5177", None))
        self.action_save.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5168\u90e8\u4fdd\u5b58(ctrl+S)", None))
        self.action_help.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5e2e\u52a9", None))
        self.action_set_mod.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6a21\u578b", None))
        self.dockWidget_tools.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5de5\u5177", None))
        self.groupBox_file.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6587\u4ef6\uff1a", None))
        self.pushButton_new.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65b0\u5efa(ctrl+N)", None))
        self.checkBox_read_only.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u53ea\u8bfb", None))
        self.pushButton_load_point_result.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8bfb\u53d6\u8bb0\u5f55", None))
        self.groupBox_image_tools.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u56fe\u50cf\uff1a", None))
        self.radioButton_inherit_scale.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7ee7\u627f\u5750\u6807\u5c3a", None))
        self.label_10.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65cb\u8f6c", None))
        self.label_angle_value.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"0", None))
        self.label_12.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u00b0", None))
        self.label.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7f29\u653e", None))
        self.label_zoom_value.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"100", None))
        self.label_13.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"%", None))
        self.pushButton_roate_left.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"-", None))
        self.pushButton_roate_right.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"+", None))
        self.pushButton_reset_view.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u590d\u539f(b)", None))
        self.groupBox_ai_control.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"AI", None))
        self.pushButton_ai_start.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u542f\u52a8", None))
        self.radioButton_full_speed.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5168\u81ea\u52a8", None))
        self.radioButton_half_speed.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u534a\u81ea\u52a8", None))
        self.pushButton_ai_draw.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8bc6\u522b", None))
        self.groupBox_traget_ai.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"Ai\u5c42", None))
        self.label_3.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"Ai Mod:", None))
        self.label_ai_mod.setText("")
        self.label_11.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u76ee\u6807\u540d\uff1a", None))
        self.pushButton_add_yolo_rectangle.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6dfb\u52a0AI\u5c42", None))
        self.pushButton_delect_target_ai.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5220\u9664AI\u5c42", None))
        self.pushButton_new_yolo_ai.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"New Yolo Go", None))
        self.groupBox_traget_manual.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u624b\u52a8\u5c42:", None))
        self.label_6.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6a21\u578b\uff1a", None))
        self.label_9.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u76ee\u6807\u540d", None))
        self.label_8.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6a21\u578b", None))
        self.label_7.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5df2\u6807\u8bb0", None))
        self.pushButton_target_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"+", None))
        self.pushButton_target_prev.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"-", None))
        self.pushButton_add_target.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6dfb\u52a0(\u7a7a\u683c)", None))
        self.pushButton_remove_target.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5220\u9664(Del)", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5bf9\u6bd4", None))
        self.label_14.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"ai:", None))
        self.label_15.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4eba\u5de5:", None))
        self.pushButton_compare.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5bf9\u6bd4", None))
        self.groupBox_image.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u56fe\u7247:", None))
        self.pushButton_image_prev.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"<(ctrl+A)", None))
        self.pushButton_image_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u">(ctrl+D)", None))
        self.pushButton_save.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4fdd\u5b58", None))
        self.pushButton_save_all.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5168\u90e8\u4fdd\u5b58(ctrl+s)", None))
        self.dockWidget_history.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5386\u53f2\u8bb0\u5f55", None))
        self.label_16.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"id", None))
        self.label_17.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4f4d\u7f6e", None))
        self.label_18.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7c7b\u578b", None))
        self.tabWidget_history.setTabText(self.tabWidget_history.indexOf(self.tab_manual), QCoreApplication.translate("Form_2D_mark_point_main", u"Manual\uff1a", None))
        self.label_19.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"id", None))
        self.label_20.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4f4d\u7f6e", None))
        self.label_21.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7c7b\u578b", None))
        self.tabWidget_history.setTabText(self.tabWidget_history.indexOf(self.tab_ai), QCoreApplication.translate("Form_2D_mark_point_main", u"AI\uff1a", None))
        self.pushButton_point_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5feb\u901f\u91cd\u753b\uff08Tab\uff09", None))
        self.pushButton_redraw_finish.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5b8c\u6210(ctrl+Tab)", None))
        self.menu.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6587\u4ef6", None))
        self.menu_2.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8bbe\u7f6e", None))
        self.menu_3.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u89c6\u56fe", None))
    # retranslateUi

