# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_paint.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
# import paint_class.global_var as var

class CurcleLable(QLabel):
    myMouseDrag = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.flag = None
        self.new_x = 0
        self.new_y = 0
        self.center_x = 20
        self.center_y = 20
        self.last_x = 0
        self.info = None

    # 鼠标点击事件
    def mousePressEvent(self, event):
        self.flag = True
        self.last_x = (event.x(), event.y())

    # 鼠标移动事件
    def mouseMoveEvent(self, event):
        # scale = var.global_scale
        if self.flag:
            x, y = event.x(), event.y()
            dx, dy = x - self.last_x[0], y - self.last_x[1]
            geom = self.geometry()
            self.new_x = geom.x() + dx
            self.new_y = geom.y() + dy
            # self.setGeometry(new_x,new_y,geom.width(),geom.height())
            self.center_x, self.center_y = self.new_x + geom.width() / 2, self.new_y + geom.height() / 2
            self.move(self.new_x, self.new_y)
            self.myMouseDrag.emit()
            # print(self.new_x, self.new_y)
            # print(self.center_x, self.center_y)

    # 鼠标释放事件
    def mouseReleaseEvent(self, event):
        self.flag = False
        # print(self.new_x, self.new_y)
        # print(self.center_x, self.center_y)


    def mouseDoubleClickEvent(self, event):
        mouse_pos = self.mapToParent(event.pos())
        if mouse_pos.x() > self.center_x+3:
            self.center_x += 1
            self.new_x += 1
        elif mouse_pos.x() < self.center_x-3:
            self.center_x -= 1
            self.new_x -= 1
        elif mouse_pos.y() > self.center_y+3:
            self.center_y += 1
            self.new_y += 1
        elif mouse_pos.y() < self.center_y-3:
            self.center_y -= 1
            self.new_y -= 1

        self.myMouseDrag.emit()
        self.move(self.new_x,self.new_y)


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        # Form.resize(574, 471)
        self.mark_cursor_1 = CurcleLable(Form)
        self.mark_cursor_1.setObjectName(u"mark_cursor_1")
        self.mark_cursor_1.setGeometry(QRect(150, 140, 41, 41))
        self.mark_cursor_1.setLayoutDirection(Qt.LeftToRight)
        self.mark_cursor_1.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_1.setScaledContents(True)
        self.mark_cursor_1.setAlignment(Qt.AlignCenter)
        


        self.mark_cursor_2 = CurcleLable(Form)
        self.mark_cursor_2.setObjectName(u"mark_cursor_2")
        self.mark_cursor_2.setGeometry(QRect(360, 140, 41, 41))
        self.mark_cursor_2.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.mark_cursor_2.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_2.setScaledContents(True)
        self.mark_cursor_2.setAlignment(Qt.AlignCenter)


        self.mark_cursor_3 = CurcleLable(Form)
        self.mark_cursor_3.setObjectName(u"mark_cursor_3")
        self.mark_cursor_3.setGeometry(QRect(360, 250, 41, 41))
        self.mark_cursor_3.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.mark_cursor_3.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_3.setScaledContents(True)
        self.mark_cursor_3.setAlignment(Qt.AlignCenter)

        self.mark_cursor_4 = CurcleLable(Form)
        self.mark_cursor_4.setObjectName(u"mark_cursor_4")
        self.mark_cursor_4.setGeometry(QRect(150, 250, 41, 41))
        self.mark_cursor_4.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.mark_cursor_4.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_4.setScaledContents(True)
        self.mark_cursor_4.setAlignment(Qt.AlignCenter)

        # 5-6-------------------------------------------

        self.mark_cursor_5 = CurcleLable(Form)
        self.mark_cursor_5.setObjectName(u"mark_cursor_5")
        self.mark_cursor_5.setGeometry(QRect(360+210, 140, 41, 41))
        self.mark_cursor_5.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.mark_cursor_5.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_5.setScaledContents(True)
        self.mark_cursor_5.setAlignment(Qt.AlignCenter)

        self.mark_cursor_6 = CurcleLable(Form)
        self.mark_cursor_6.setObjectName(u"mark_cursor_6")
        self.mark_cursor_6.setGeometry(QRect(360+210, 250, 41, 41))
        self.mark_cursor_6.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.mark_cursor_6.setPixmap(QPixmap(u"./aim_sight.png"))
        self.mark_cursor_6.setScaledContents(True)
        self.mark_cursor_6.setAlignment(Qt.AlignCenter)


        font1 = QFont()
        font1.setPointSize(10)

        self.label_1_inf = QLabel(Form)
        self.label_1_inf.setObjectName(u"label_1_inf")
        self.label_1_inf.setGeometry(QRect(370, 320, 100, 20))
        self.label_1_inf.setFont(font1)
        self.label_1_inf.setStyleSheet(u"color: rgb(255, 0, 0);")
       

        self.label_2_inf = QLabel(Form)
        self.label_2_inf.setObjectName(u"label_2_inf")
        self.label_2_inf.setGeometry(QRect(370+400, 320, 100, 20))
        self.label_2_inf.setFont(font1)
        self.label_2_inf.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.label_3_inf = QLabel(Form)
        self.label_3_inf.setObjectName(u"label_3_inf")
        self.label_3_inf.setGeometry(QRect(370+400, 250+300, 100, 20))
        self.label_3_inf.setFont(font1)
        self.label_3_inf.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.label_4_inf = QLabel(Form)
        self.label_4_inf.setObjectName(u"label_4_inf")
        self.label_4_inf.setGeometry(QRect(370, 250+300, 100, 20))
        self.label_4_inf.setFont(font1)
        self.label_4_inf.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.label_5_inf = QLabel(Form)
        self.label_5_inf.setObjectName(u"label_5_inf")
        self.label_5_inf.setGeometry(QRect(370, 250+300, 100, 20))
        self.label_5_inf.setFont(font1)
        self.label_5_inf.setStyleSheet(u"color: rgb(255, 0, 0);")

        self.label_6_inf = QLabel(Form)
        self.label_6_inf.setObjectName(u"label_6_inf")
        self.label_6_inf.setGeometry(QRect(370, 250+300, 100, 20))
        self.label_6_inf.setFont(font1)
        self.label_6_inf.setStyleSheet(u"color: rgb(255, 0, 0);")


        font11 = QFont()
        font11.setPointSize(10)


        self.width_x = QLabel(Form)
        self.width_x.setObjectName(u"width_x")
        self.width_x.setGeometry(QRect(250, 150, 60, 20))
        self.width_x.setFont(font11)
        self.width_x.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.width_x.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.width_y = QLabel(Form)
        self.width_y.setObjectName(u"width_y")
        self.width_y.setGeometry(QRect(150, 210, 60, 20))
        self.width_y.setFont(font11)
        self.width_y.setStyleSheet(u"color: rgb(0, 255, 0);")


        self.width_5_6 = QLabel(Form)
        self.width_5_6.setObjectName(u"width_5_6")
        self.width_5_6.setGeometry(QRect(400, 150, 60, 20))
        self.width_5_6.setFont(font11)
        self.width_5_6.setStyleSheet(u"color: rgb(0, 255, 0);")
        self.width_5_6.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.high_5_6 = QLabel(Form)
        self.high_5_6.setObjectName(u"high_5_6")
        self.high_5_6.setGeometry(QRect(360+210, 210, 60, 20))
        self.high_5_6.setFont(font11)
        self.high_5_6.setStyleSheet(u"color: rgb(0, 255, 0);")



    
        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"\u753b\u70b9", None))
        # self.width_x.setText(QCoreApplication.translate("Form", u"5.1", None))
        # self.width_y.setText(QCoreApplication.translate("Form", u"3.1", None))

        self.label_1_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_2_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_3_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_4_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))

        self.label_5_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_6_inf.setText(QCoreApplication.translate("Form", u"TextLabel", None))
    # retranslateUi