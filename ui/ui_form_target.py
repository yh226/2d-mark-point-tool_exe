# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_targetCoJUfl.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(400, 300)
        self.horizontalLayoutWidget = QWidget(Form)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(70, 110, 232, 80))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.label_target_name = QLabel(self.horizontalLayoutWidget)
        self.label_target_name.setObjectName(u"label_target_name")

        self.horizontalLayout.addWidget(self.label_target_name)

        self.label_mode_type = QLabel(self.horizontalLayoutWidget)
        self.label_mode_type.setObjectName(u"label_mode_type")

        self.horizontalLayout.addWidget(self.label_mode_type)

        self.label_point_number = QLabel(self.horizontalLayoutWidget)
        self.label_point_number.setObjectName(u"label_point_number")

        self.horizontalLayout.addWidget(self.label_point_number)

        self.horizontalLayout.setStretch(0, 30)
        self.horizontalLayout.setStretch(1, 75)
        self.horizontalLayout.setStretch(2, 5)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.label_target_name.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_mode_type.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_point_number.setText(QCoreApplication.translate("Form", u"TextLabel", None))
    # retranslateUi

