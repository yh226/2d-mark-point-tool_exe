/*
 * @Author: your name
 * @Date: 2020-09-07 10:22:06
 * @LastEditTime: 2021-06-25 18:12:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \node_point_test\common\js\config.js
 */
const config = {
    colors: [
        'ff6000',
        'ffff40',
        '00ff40',
        '20ffff',
        '663300',
        'FF00CC',
        'CC6633',
        '99FFFF',
        'FF6666',
        'a0ffc0',
        'a000ff',
    ],
    icons: {
        left: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTVweCIgaGVpZ2h0PSIxNnB4IiB2aWV3Qm94PSIwIDAgMTUgMTYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ3ICg0NTM5NikgLSBodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2ggLS0+CiAgICA8dGl0bGU+aWNfbW91c2VfbGVmdDwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSLnlLvmnb8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJBcnRib2FyZC0zIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTYuMDAwMDAwLCAtNDIuMDAwMDAwKSI+CiAgICAgICAgICAgIDxnIGlkPSJpY19tb3VzZV9sZWZ0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1Ni4wMDAwMDAsIDQyLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlNoYXBlIiBwb2ludHM9IjAgMCAxNSAwIDE1IDE2IDAgMTYiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik04LjEyNSwwLjcxMzMzMzMzMyBMOC4xMjUsNiBMMTIuNSw2IEMxMi41LDMuMjggMTAuNTkzNzUsMS4wNCA4LjEyNSwwLjcxMzMzMzMzMyBaIE0yLjUsMTAgQzIuNSwxMi45NDY2NjY3IDQuNzM3NSwxNS4zMzMzMzMzIDcuNSwxNS4zMzMzMzMzIEMxMC4yNjI1LDE1LjMzMzMzMzMgMTIuNSwxMi45NDY2NjY3IDEyLjUsMTAgTDEyLjUsNy4zMzMzMzMzMyBMMi41LDcuMzMzMzMzMzMgTDIuNSwxMCBaIiBpZD0iU2hhcGUiIGZpbGw9IiM5RkEyQTgiIGZpbGwtcnVsZT0ibm9uemVybyI+PC9wYXRoPgogICAgICAgICAgICAgICAgPHBhdGggZD0iTTYuODc1LDAuNzEzMzMzMzMzIEM0LjQwNjI1LDEuMDQgMi41LDMuMjggMi41LDYgTDYuODc1LDYgTDYuODc1LDAuNzEzMzMzMzMzIFoiIGlkPSJQYXRoIiBmaWxsPSIjRkZBOTAwIj48L3BhdGg+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==',
        middle: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4IiB2aWV3Qm94PSIwIDAgMTYgMTYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ3ICg0NTM5NikgLSBodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2ggLS0+CiAgICA8dGl0bGU+aWNfbW91c2U8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz48L2RlZnM+CiAgICA8ZyBpZD0i55S75p2/IiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0iQXJ0Ym9hcmQtMiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIwMi4wMDAwMDAsIC0zNi4wMDAwMDApIj4KICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIwMi4wMDAwMDAsIDM2LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPGcgaWQ9ImljL21vdXNlL2dyZXk2MDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuNTAwMDAwLCAwLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgIDxnIGlkPSJpY19tb3VzZV8yNHB4Ij4KICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlNoYXBlIiBwb2ludHM9IjAgMCAxNSAwIDE1IDE2IDAgMTYiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTguMTI1LDAuNzEzMzMzMzMzIEw4LjEyNSw2IEwxMi41LDYgQzEyLjUsMy4yOCAxMC41OTM3NSwxLjA0IDguMTI1LDAuNzEzMzMzMzMzIFogTTIuNSwxMCBDMi41LDEyLjk0NjY2NjcgNC43Mzc1LDE1LjMzMzMzMzMgNy41LDE1LjMzMzMzMzMgQzEwLjI2MjUsMTUuMzMzMzMzMyAxMi41LDEyLjk0NjY2NjcgMTIuNSwxMCBMMTIuNSw3LjMzMzMzMzMzIEwyLjUsNy4zMzMzMzMzMyBMMi41LDEwIFogTTYuODc1LDAuNzEzMzMzMzMzIEM0LjQwNjI1LDEuMDQgMi41LDMuMjggMi41LDYgTDYuODc1LDYgTDYuODc1LDAuNzEzMzMzMzMzIFoiIGlkPSJTaGFwZSIgZmlsbD0iIzhDOTA5NyIgZmlsbC1ydWxlPSJub256ZXJvIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgPHJlY3QgaWQ9IlJlY3RhbmdsZS0yIiBzdHJva2U9IiNGRkZGRkYiIGZpbGw9IiNGRkE5MDAiIHg9IjYuNSIgeT0iMiIgd2lkdGg9IjMiIGhlaWdodD0iNyIgcng9IjEuNSI+PC9yZWN0PgogICAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=',
        right: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTVweCIgaGVpZ2h0PSIxNnB4IiB2aWV3Qm94PSIwIDAgMTUgMTYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDQ3ICg0NTM5NikgLSBodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2ggLS0+CiAgICA8dGl0bGU+aWNfbW91c2VfbGVmdDwvdGl0bGU+CiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4KICAgIDxkZWZzPjwvZGVmcz4KICAgIDxnIGlkPSLnlLvmnb8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJBcnRib2FyZC0zIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTYuMDAwMDAwLCAtNDIuMDAwMDAwKSI+CiAgICAgICAgICAgIDxnIGlkPSJpY19tb3VzZV9sZWZ0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1Ni4wMDAwMDAsIDQyLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlNoYXBlIiBwb2ludHM9IjAgMCAxNSAwIDE1IDE2IDAgMTYiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik04LjEyNSwwLjcxMzMzMzMzMyBMOC4xMjUsNiBMMTIuNSw2IEMxMi41LDMuMjggMTAuNTkzNzUsMS4wNCA4LjEyNSwwLjcxMzMzMzMzMyBaIE0yLjUsMTAgQzIuNSwxMi45NDY2NjY3IDQuNzM3NSwxNS4zMzMzMzMzIDcuNSwxNS4zMzMzMzMzIEMxMC4yNjI1LDE1LjMzMzMzMzMgMTIuNSwxMi45NDY2NjY3IDEyLjUsMTAgTDEyLjUsNy4zMzMzMzMzMyBMMi41LDcuMzMzMzMzMzMgTDIuNSwxMCBaIiBpZD0iU2hhcGUiIGZpbGw9IiM5RkEyQTgiIGZpbGwtcnVsZT0ibm9uemVybyI+PC9wYXRoPgogICAgICAgICAgICAgICAgPHBhdGggZD0iTTYuODc1LDAuNzEzMzMzMzMzIEM0LjQwNjI1LDEuMDQgMi41LDMuMjggMi41LDYgTDYuODc1LDYgTDYuODc1LDAuNzEzMzMzMzMzIFoiIGlkPSJQYXRoIiBmaWxsPSIjRkZBOTAwIj48L3BhdGg+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg=='
    },
    MAX_scale_offest: 6.0,
    MIN_scale_offest: 0.5,
    confidence_base: 0.6,
    points_lines: [
        {
            name: "Basketball_29",
            points_num: 29,
             pt_line: {
            left: [[3,13],[3,4],[4,5],[5,21],[5,22],[21,22],[9,10],[10,11],[11,25],[11,26],[25,26],[14,16],[16,18]],
            head: [[12,14],[14,13],[6,9],[13,27],[27,28],[28,6],[28,9],[13,28]],
            right: [[0,13],[0,1],[1,2],[2,19],[2,20],[19,20],[6,7],[7,8],[8,23],[8,24],[23,24],[14,15],[15,17]]
            },
            color: {
             left: '#FF0000',
             head: '#0000FF',
             right: '#00FF00'
             },
            label:{
             contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SP_33",
            points_num: 33,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28]],
                head: [[12,14],[14,13],[6,9],[29,30],[6,29],[9,30],[31,32],[32,29],[32,30],[13,31],[13,32]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "BengChuang",
            points_num: 25,
             pt_line: {
            left: [[3,13],[3,4],[4,5],[5,17],[5,18],[17,18],[9,10],[10,11],[11,21],[11,22],[21,22]],
            head: [[12,14],[14,13],[6,9],[13,23],[23,24],[24,6],[24,9],[13,24]],
            right: [[0,13],[0,1],[1,2],[2,15],[2,16],[15,16],[6,7],[7,8],[8,19],[8,20],[19,20]]
            },
            color: {
             left: '#FF0000',
             head: '#0000FF',
             right: '#00FF00'
             },
            label:{
             contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "coco17+18",
            points_num: 35,
            pt_line: {
            left: [[0,1],[1,3],[5,7],[7,9],[11,13],[13,15],[21,22],[21,9],[22,9],[15,26],[26,27],[15,28],[18,5]],
            head: [[17,18],[29,30],[18,31],[18,32],[31,32],[32,33],[32,34],[33,12],[34,11],[11,12]],
            right: [[0,2],[2,4],[6,8],[8,10],[12,14],[14,16],[19,10],[20,10],[19,20],[23,24],[23,16],[16,25],[18,6]]
            },
            color: {
            left: '#FF0000',
            head: '#0000FF',
            right: '#00FF00'
            }
        },
        {
            name: "LT_SP_31_rm",
            points_num: 31,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[13,30]],
                head: [[12,14],[14,13],[6,9],[29,30],[6,29],[9,30]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[13,29]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "foot2",
            points_num: 2,
            pt_line: {
                left: [],
                head: [],
                right: []
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SP_iliac_foot_shoulder",
            points_num: 10,
            pt_line: {
                left: [[1,3],[3,5],[8,9]],
                head: [[0,1],[2,3],[4,5]],
                right: [[0,2],[2,4],[6,7]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SS_33",
            points_num: 33,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[13,9],[31,32]],
                head: [[12,14],[14,13],[6,9]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[13,6],[29,30]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SK_33",
            points_num: 33,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[13,9],[31,32]],
                head: [[12,14],[14,13],[6,9]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[13,6],[29,30]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_AS_19",
            points_num: 19,
            pt_line: {
                left: [[3,13],[3,4],[4,5],[9,10],[10,11],[11,16]],
                head: [[12,14],[14,13],[13,17],[17,18],[6,18],[9,18],[6,9],[13,18]],
                right: [[0,13],[0,1],[1,2],[6,7],[7,8],[8,15]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SS_37",
            points_num: 37,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[14,30],[35,36]],
                head: [[12,14],[14,13],[13,31],[31,32],[6,32],[9,32],[6,9],[13,32]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[14,29],[33,34]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_AS_41",
            points_num: 41,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[14,30],[37,38],[39,40],[37,40],[38,39]],
                head: [[12,14],[14,13],[13,31],[31,32],[6,32],[9,32],[6,9]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[14,29],[33,34],[35,36],[33,36],[34,35]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_FS_33",
            points_num: 33,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[14,30]],
                head: [[12,14],[14,13],[13,31],[31,32],[6,32],[9,32],[6,9]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[14,29]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_AS_43",
            points_num: 43,
            pt_line: {
                left:  [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28],[39,40],[41,42],[18,34]],
                head:  [[0,5],[17,18],[32,33],[32,34],[35,38],[36,37],[39,42],[40,41],[6,31],[31,32],[29,30]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25],[35,36],[37,38],[17,33]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "hand_35",
            points_num: 35,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,29],[5,30],[9,10],[10,11],[11,26],[26,19],[11,20],[14,22],[9,34]],
                head: [[12,14],[14,13],[13,31],[31,32]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,27],[2,28],[6,7],[7,8],[8,23],[23,24],[8,25],[14,21],[6,33]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "LT_SS_39",
            points_num: 39,
            pt_line: {
                left:  [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28],[31,32],[18,38]],
                head:  [[0,5],[33,34],[35,36],[36,37],[36,38],[17,18]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25],[29,30],[17,37]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "huabing_33",
            points_num: 33,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28],[31,32]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25],[29,30]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "MC-33_new",
            points_num: 33,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28]],
                head: [[0,5],[0,6],[6,29],[29,30],[6,31],[31,32],[30,32]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "jiawei-31",
            points_num: 31,
            pt_line: {
                left: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25]],
                head: [[0,5],[0,6],[6,35],[6,29],[29,30]],
                right: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "jiawei_14_and_21",
            points_num: 35,
            pt_line: {
                left: [[14,16],[16,18],[3,13],[3,4],[4,5],[5,21],[5,22],[9,10],[10,11],[11,26],[26,27],[11,28],[14,30],[9,34],[32,34]],
                head: [[12,14],[14,13],[13,31],[31,32],[6,9],[33,34]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,7],[7,8],[8,23],[23,24],[8,25],[14,29],[6,33],[32,33]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "MC-39_new",
            points_num: 39,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28],[14,38],[33,34],[34,35],[35,36],[36,33]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25],[13,37],[29,30],[30,31],[31,32],[32,29]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[29,30,31,32,33,34,35,36,37,38],
            }
        },
        {
            name: "MC-29_new",
            points_num: 29,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "MC-21",
            points_num: 21,
            pt_line: {
                left: [[12,9],[9,10],[10,11],[3,4],[4,5],[5,19],[5,15],[15,17]],
                head: [[13,20],[20,12]],
                right: [[12,8],[8,7],[7,6],[2,1],[1,0],[0,18],[0,14],[14,16]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[14,15,16,17,18,19],
            }
        },
        {
            name: "OP-25b",
            points_num: 25,
            pt_line: {
                left: [[1,3],[0,1],[17,5],[5,7],[7,9],[17,11],[11,13],[13,15],[15,21],[15,19],[19,20]],
                head: [[0,18],[0,17]],
                right: [[0,2],[2,4],[17,6],[6,8],[8,10],[17,12],[12,14],[14,16],[16,24],[16,22],[22,23]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[19,20,21,22,23,24],
            }
        },
        {
            name: "Ralph-31",
            points_num: 31,
            pt_line: {
                left: [[15,14],[12,13],[18,20],[26,27],[28,29],[28,30],[11,18],[1,2],[10,11]],
                head: [[3,2],[2,10]],
                right: [[5,6],[7,8],[23,24],[23,25],[21,22],[17,19],[9,17],[0,2],[9,10]],
                part_left: [[11,13],[13,15],[15,16],[4,6],[6,8],[8,9],[20,26],[26,28],[19,22],[22,23]],
                part_right: [[11,12],[12,14],[14,16],[4,5],[5,7],[7,9],[20,27],[27,28],[19,21],[21,23]]
                
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00',
                part_left: '#FFF100',
                part_right: '#128EF2'
            },
            label:{
                contact:[24,25,29,30],
            }
        },
        {
            name: "aic14_add_mc29",
            points_num: 15,
            pt_line: {
                left: [[0,2],[2,4],[7,8],[12,13],[12,14]],
                head: [],
                right: [[0,1],[1,3],[5,6],[9,10],[9,11]]
                
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
        },
        {
            name: "MC-29",
            points_num: 29,
            pt_line: {
                left: [[14,16],[16,18],[13,3],[3,4],[4,5],[5,21],[5,22],[9,13],[9,10],[10,11],[11,26],[26,27],[11,28]],
                head: [[12,14],[14,13]],
                right: [[14,15],[15,17],[0,13],[0,1],[1,2],[2,19],[2,20],[6,13],[6,7],[7,8],[8,25],[8,23],[23,24]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[23,24,25,26,27,28],
            }
        },
        {
            name: "MC-12_to_29",
            points_num: 17,
            pt_line: {
                left: [[0,2],[2,4],[8,10],[14,15],[14,16]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[7,9],[11,12],[11,13]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[11,12,13,14,15,16],
            }
        },
        {
            name: "MC-27",
            points_num: 27,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,24],[24,25],[20,26]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,22],[19,23]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[21,22,23,24,25,26],
            }
        },
        
        {
            name: "OP-29",
            points_num: 29,
            pt_line: {
                left: [[1,3],[0,1],[17,5],[5,7],[7,9],[9,25],[9,27],[17,11],[11,13],[13,15],[15,21],[15,19],[19,20]],
                head: [[0,18],[0,17]],
                right: [[0,2],[2,4],[17,6],[6,8],[8,10],[10,26],[10,28],[17,12],[12,14],[14,16],[16,24],[16,22],[22,23]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "MC-20",
            points_num: 20,
            pt_line: {
                left: [[6,7],[7,8],[8,12],[12,2],[2,1],[1,0],[0,18],[0,14],[14,16]],
                head: [[12,13]],
                right: [[12,9],[9,10],[10,11],[12,3],[3,4],[4,5],[5,19],[5,15],[15,17]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "OP-25",
            points_num: 25,
            pt_line: {
                left: [[0,16],[16,18],[1,5],[5,6],[6,7],[8,12],[12,13],[13,14],[14,21],[14,19],[19,20]],
                head: [[0,1],[1,8]],
                right: [[0,15],[15,17],[1,2],[2,3],[3,4],[8,9],[9,10],[10,11],[11,24],[11,22],[22,23]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "VP/H36-M17",
            points_num: 17,
            pt_line: {
                left: [[8,11],[11,12],[12,13],[0,4],[4,5],[5,6]],
                head: [[0,7],[7,8],[8,9],[9,10]],
                right: [[8,14],[14,15],[15,16],[0,1],[1,2],[2,3]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "CoCo",
            points_num: 17,
            pt_line: {
                left: [[0,1],[1,3],[5,7],[7,9],[11,13],[13,15]],
                head: [],
                right: [[0,2],[2,4],[6,8],[8,10],[12,14],[14,16]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "CMU19",
            points_num: 19,
            pt_line: {
                left: [[1,15],[15,16],[0,3],[3,4],[4,5],[2,6],[6,7],[7,8]],
                head: [[0,2]],
                right: [[1,17],[17,18],[0,9],[9,10],[10,11],[2,12],[12,13],[13,14]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "LSP",
            points_num: 14,
            pt_line: {
                left: [[12,9],[9,10],[10,11],[12,3],[3,4],[4,5]],
                head: [[12,13]],
                right: [[12,8],[8,7],[7,6],[12,2],[2,1],[1,0]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        },
        {
            name: "jw27",
            points_num: 29,
            pt_line: {
                left: [[0,2],[2,4],[6,8],[8,10],[10,12],[12,14],[12,16],[18,20],[20,22],[22,26],[26,27],[22,28],[14,38],[33,34],[34,35],[35,36],[36,33]],
                head: [[0,5],[0,6]],
                right: [[0,1],[1,3],[6,7],[7,9],[9,11],[11,13],[11,15],[17,19],[19,21],[21,23],[23,24],[21,25],[13,37],[29,30],[30,31],[31,32],[32,29]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            },
            label:{
                contact:[29,30,31,32,33,34,35,36,37,38],
            }
        },
        {
            name: "MP-II",
            points_num: 16,
            pt_line: {
                left: [[7,13],[13,14],[14,15],[6,3],[3,4],[4,5]],
                head: [[6,7],[7,8],[8,9]],
                right: [[7,12],[12,11],[11,10],[6,2],[2,1],[1,0]]
            },
            color: {
                left: '#FF0000',
                head: '#0000FF',
                right: '#00FF00'
            }
        }
    ]
}
