# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'image_item.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QPushButton,
    QRadioButton, QSizePolicy, QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(400, 300)
        self.horizontalLayoutWidget = QWidget(Form)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(100, 70, 199, 80))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.radioButton = QRadioButton(self.horizontalLayoutWidget)
        self.radioButton.setObjectName(u"radioButton")

        self.horizontalLayout.addWidget(self.radioButton)

        self.label_image_name = QLabel(self.horizontalLayoutWidget)
        self.label_image_name.setObjectName(u"label_image_name")

        self.horizontalLayout.addWidget(self.label_image_name)

        self.pushButton_review = QPushButton(self.horizontalLayoutWidget)
        self.pushButton_review.setObjectName(u"pushButton_review")
        self.pushButton_review.setMinimumSize(QSize(10, 0))

        self.horizontalLayout.addWidget(self.pushButton_review)

        self.horizontalLayout.setStretch(0, 5)
        self.horizontalLayout.setStretch(1, 85)
        self.horizontalLayout.setStretch(2, 10)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.radioButton.setText("")
        self.label_image_name.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.pushButton_review.setText(QCoreApplication.translate("Form", u">", None))
    # retranslateUi

