# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
import os
import sys
import numpy as np
import cv2
from mcpose2d import POSE2D_CFG, MCPose2dRunner
from mcpose2d import list_providers

cuda_providers = list_providers()

cfg = POSE2D_CFG.clone()
##yolo
cfg.yolo.model = "//192.168.100.200/9.upload3_14T/20220523-syp/12.models/yolo/yolov5m_fixed_w640_h480.onnx"
cfg.yolo.iou_thres = 0.7
cfg.yolo.providers = cuda_providers[0]
##x2ds
cfg.x2ds.model = "//192.168.100.200/9.upload3_14T/xiga/12.models/mcpose2d/hybrid3/jw27_2662/20230529-darknetv2-i320x320v2662-jw27-ep438loss0.0005792801625436304.onnx"
cfg.x2ds.preprocessor.ext_scale = 1.3
cfg.x2ds.providers = cuda_providers[0]
cfg.tracker.interval = 1
cfg.x2ds.person.kp_thres = 0.5


def pose2d_runner(input_queue, output_queue, meta):
    #--convert config start--#
    cfg = meta
    #--convert config done--#
    
    # mcpose2d runner init
    mcpose2d_runner = MCPose2dRunner(cfg)
    output_queue.put([2,100])

    # read input data
    while True:
        if not input_queue.empty(): # if or while ??
            opcode, opdata = input_queue.get(False)
            if opcode == 1:
                img = cv2.imread(opdata)
                img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
                ret_msg = mcpose2d_runner.forward(img)
                if ret_msg is not None:
                    # x2ds,xyxy
                    output_queue.put([1, ret_msg])
                else:
                    output_queue.put([0, None])
            else:
                output_queue.put([0, None])