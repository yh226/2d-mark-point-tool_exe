# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_main_paint.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGraphicsView, QSizePolicy, QVBoxLayout,
    QWidget)

class Ui_PaintForm(object):
    def setupUi(self, PaintForm):
        if not PaintForm.objectName():
            PaintForm.setObjectName(u"PaintForm")
        PaintForm.resize(862, 697)
        self.verticalLayout = QVBoxLayout(PaintForm)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.graphicsView = QGraphicsView(PaintForm)
        self.graphicsView.setObjectName(u"graphicsView")

        self.verticalLayout.addWidget(self.graphicsView)


        self.retranslateUi(PaintForm)

        QMetaObject.connectSlotsByName(PaintForm)
    # setupUi

    def retranslateUi(self, PaintForm):
        PaintForm.setWindowTitle(QCoreApplication.translate("PaintForm", u"Form", None))
    # retranslateUi

