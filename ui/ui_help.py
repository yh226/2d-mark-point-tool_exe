# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'helpRXxenu.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form_help(object):
    def setupUi(self, Form_help):
        if not Form_help.objectName():
            Form_help.setObjectName(u"Form_help")
        Form_help.resize(1311, 921)
        self.horizontalLayout = QHBoxLayout(Form_help)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_help_image = QLabel(Form_help)
        self.label_help_image.setObjectName(u"label_help_image")

        self.verticalLayout.addWidget(self.label_help_image)

        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setHorizontalSpacing(10)
        self.formLayout.setVerticalSpacing(10)
        self.label_5 = QLabel(Form_help)
        self.label_5.setObjectName(u"label_5")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_5)

        self.label = QLabel(Form_help)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label)

        self.label_2 = QLabel(Form_help)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.label_2)

        self.label_7 = QLabel(Form_help)
        self.label_7.setObjectName(u"label_7")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.label_7)

        self.label_8 = QLabel(Form_help)
        self.label_8.setObjectName(u"label_8")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.label_8)

        self.label_9 = QLabel(Form_help)
        self.label_9.setObjectName(u"label_9")

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.label_9)

        self.label_10 = QLabel(Form_help)
        self.label_10.setObjectName(u"label_10")

        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.label_10)

        self.label_3 = QLabel(Form_help)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(6, QFormLayout.FieldRole, self.label_3)

        self.label_11 = QLabel(Form_help)
        self.label_11.setObjectName(u"label_11")

        self.formLayout.setWidget(7, QFormLayout.FieldRole, self.label_11)

        self.label_12 = QLabel(Form_help)
        self.label_12.setObjectName(u"label_12")

        self.formLayout.setWidget(8, QFormLayout.FieldRole, self.label_12)

        self.label_13 = QLabel(Form_help)
        self.label_13.setObjectName(u"label_13")

        self.formLayout.setWidget(9, QFormLayout.FieldRole, self.label_13)

        self.label_6 = QLabel(Form_help)
        self.label_6.setObjectName(u"label_6")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.label_6)

        self.label_4 = QLabel(Form_help)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_4)

        self.label_14 = QLabel(Form_help)
        self.label_14.setObjectName(u"label_14")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_14)

        self.label_15 = QLabel(Form_help)
        self.label_15.setObjectName(u"label_15")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_15)

        self.label_16 = QLabel(Form_help)
        self.label_16.setObjectName(u"label_16")

        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.label_16)

        self.label_17 = QLabel(Form_help)
        self.label_17.setObjectName(u"label_17")

        self.formLayout.setWidget(6, QFormLayout.LabelRole, self.label_17)

        self.label_18 = QLabel(Form_help)
        self.label_18.setObjectName(u"label_18")

        self.formLayout.setWidget(7, QFormLayout.LabelRole, self.label_18)

        self.label_19 = QLabel(Form_help)
        self.label_19.setObjectName(u"label_19")

        self.formLayout.setWidget(8, QFormLayout.LabelRole, self.label_19)

        self.label_20 = QLabel(Form_help)
        self.label_20.setObjectName(u"label_20")

        self.formLayout.setWidget(9, QFormLayout.LabelRole, self.label_20)

        self.label_21 = QLabel(Form_help)
        self.label_21.setObjectName(u"label_21")

        self.formLayout.setWidget(10, QFormLayout.LabelRole, self.label_21)

        self.label_22 = QLabel(Form_help)
        self.label_22.setObjectName(u"label_22")

        self.formLayout.setWidget(11, QFormLayout.LabelRole, self.label_22)

        self.label_23 = QLabel(Form_help)
        self.label_23.setObjectName(u"label_23")

        self.formLayout.setWidget(12, QFormLayout.LabelRole, self.label_23)

        self.label_24 = QLabel(Form_help)
        self.label_24.setObjectName(u"label_24")

        self.formLayout.setWidget(13, QFormLayout.LabelRole, self.label_24)

        self.label_25 = QLabel(Form_help)
        self.label_25.setObjectName(u"label_25")

        self.formLayout.setWidget(14, QFormLayout.LabelRole, self.label_25)

        self.label_26 = QLabel(Form_help)
        self.label_26.setObjectName(u"label_26")

        self.formLayout.setWidget(15, QFormLayout.LabelRole, self.label_26)


        self.verticalLayout.addLayout(self.formLayout)

        self.verticalLayout.setStretch(0, 80)
        self.verticalLayout.setStretch(1, 20)

        self.horizontalLayout.addLayout(self.verticalLayout)


        self.retranslateUi(Form_help)

        QMetaObject.connectSlotsByName(Form_help)
    # setupUi

    def retranslateUi(self, Form_help):
        Form_help.setWindowTitle(QCoreApplication.translate("Form_help", u"Help", None))
        self.label_help_image.setText("")
        self.label_5.setText(QCoreApplication.translate("Form_help", u"I :\u754c\u9762", None))
        self.label.setText(QCoreApplication.translate("Form_help", u"1. Tab: \u5feb\u901f\u91cd\u753b", None))
        self.label_2.setText(QCoreApplication.translate("Form_help", u"20. \u5de6\u5efa: \u79fb\u52a8\u70b9", None))
        self.label_7.setText(QCoreApplication.translate("Form_help", u"21. \u6eda\u8f6e\uff1a \u653e\u5927", None))
        self.label_8.setText(QCoreApplication.translate("Form_help", u"22. \u53f3\u952e\uff1a\u6dfb\u52a0\u5185\u90e8\u65b0\u70b9", None))
        self.label_9.setText(QCoreApplication.translate("Form_help", u"22-1\uff1a\u53f3\u952e+shift:\u6dfb\u52a0\u7a7a\u90e8\u65b0\u70b9", None))
        self.label_10.setText(QCoreApplication.translate("Form_help", u"22-2\uff1a\u53f3\u952e+ctrl:\u6dfb\u52a0\u5916\u90e8\u65b0\u70b9", None))
        self.label_3.setText(QCoreApplication.translate("Form_help", u"5. A: \u6807\u8bb0\u70b9\u5411\u5de6\u5e73\u79fb", None))
        self.label_11.setText(QCoreApplication.translate("Form_help", u"6. S: \u6807\u8bb0\u70b9\u5411\u4e0b\u5e73\u79fb", None))
        self.label_12.setText(QCoreApplication.translate("Form_help", u"7. D: \u6807\u8bb0\u70b9\u5411\u53f3\u5e73\u79fb", None))
        self.label_13.setText(QCoreApplication.translate("Form_help", u"16. W: \u6807\u8bb0\u70b9\u5411\u4e0a\u5e73\u79fb", None))
        self.label_6.setText(QCoreApplication.translate("Form_help", u"II\uff1a \u70b9\u76f8\u5173", None))
        self.label_4.setText(QCoreApplication.translate("Form_help", u"2. Ctrl+Tab: \u5b8c\u6210\u91cd\u753b", None))
        self.label_14.setText(QCoreApplication.translate("Form_help", u"8. Ctrl+A: \u56fe\u7247\u5411\u524d\u9884\u89c8", None))
        self.label_15.setText(QCoreApplication.translate("Form_help", u"10. Ctrl+D: \u56fe\u7247\u5411\u540e\u9884\u89c8", None))
        self.label_16.setText(QCoreApplication.translate("Form_help", u"9. Ctrl+S: \u4fdd\u5b58\u6807\u8bb0\u70b9\u7ed3\u679c", None))
        self.label_17.setText(QCoreApplication.translate("Form_help", u"11. \u65b9\u5411\u5efa\u5de6: \u5411\u5de6\u5e73\u79fb\u653e\u5927\u56fe\u7247", None))
        self.label_18.setText(QCoreApplication.translate("Form_help", u"12. \u65b9\u5411\u952e\u4e0b: \u5411\u4e0b\u5e73\u79fb\u653e\u5927\u56fe\u7247", None))
        self.label_19.setText(QCoreApplication.translate("Form_help", u"13. \u65b9\u5411\u952e\u53f3: \u5411\u53f3\u5e73\u79fb\u653e\u5927\u56fe\u7247", None))
        self.label_20.setText(QCoreApplication.translate("Form_help", u"14. \u65b9\u5411\u5efa\u4e0a: \u5411\u4e0a\u5e73\u79fb\u653e\u5927\u56fe\u7247", None))
        self.label_21.setText(QCoreApplication.translate("Form_help", u"15. Q: \u4e0a\u4e00\u6807\u8bb0\u5c42", None))
        self.label_22.setText(QCoreApplication.translate("Form_help", u"16. E: \u4e0b\u4e00\u6807\u8bb0\u5c42", None))
        self.label_23.setText(QCoreApplication.translate("Form_help", u"18. R: \u5de6\u65cb\u8f6c", None))
        self.label_24.setText(QCoreApplication.translate("Form_help", u"19. T: \u53f3\u65cb\u8f6c", None))
        self.label_25.setText(QCoreApplication.translate("Form_help", u"23. SPACE: \u65b0\u5efa\u6807\u8bb0\u5c42 ", None))
        self.label_26.setText(QCoreApplication.translate("Form_help", u"24.\uff1a DEL: \u5220\u9664\u6807\u8bb0\u5c42", None))
    # retranslateUi

