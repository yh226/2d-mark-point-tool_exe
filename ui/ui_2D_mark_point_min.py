# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file '2D_mark_point_minGPjaxo.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form_2D_mark_point_min(object):
    def setupUi(self, Form_2D_mark_point_min):
        if not Form_2D_mark_point_min.objectName():
            Form_2D_mark_point_min.setObjectName(u"Form_2D_mark_point_min")
        Form_2D_mark_point_min.resize(1006, 835)
        self.horizontalLayout_2 = QHBoxLayout(Form_2D_mark_point_min)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_image_show_min = QLabel(Form_2D_mark_point_min)
        self.label_image_show_min.setObjectName(u"label_image_show_min")

        self.verticalLayout.addWidget(self.label_image_show_min)

        self.label = QLabel(Form_2D_mark_point_min)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.comboBox_object = QComboBox(Form_2D_mark_point_min)
        self.comboBox_object.setObjectName(u"comboBox_object")

        self.verticalLayout.addWidget(self.comboBox_object)

        self.pushButton_exit = QPushButton(Form_2D_mark_point_min)
        self.pushButton_exit.setObjectName(u"pushButton_exit")

        self.verticalLayout.addWidget(self.pushButton_exit)

        self.verticalLayout.setStretch(0, 98)
        self.verticalLayout.setStretch(1, 1)
        self.verticalLayout.setStretch(2, 1)

        self.horizontalLayout_2.addLayout(self.verticalLayout)


        self.retranslateUi(Form_2D_mark_point_min)

        QMetaObject.connectSlotsByName(Form_2D_mark_point_min)
    # setupUi

    def retranslateUi(self, Form_2D_mark_point_min):
        Form_2D_mark_point_min.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_min", u"2D_mark_point_min", None))
        self.label_image_show_min.setText(QCoreApplication.translate("Form_2D_mark_point_min", u"TextLabel", None))
        self.label.setText(QCoreApplication.translate("Form_2D_mark_point_min", u"\u76ee\u6807\u540d\uff1a", None))
        self.pushButton_exit.setText(QCoreApplication.translate("Form_2D_mark_point_min", u"\u5173\u95ed", None))
    # retranslateUi

