# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_pose2d_dialogpONbLo.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Ai_Dialog(object):
    def setupUi(self, Ai_Dialog):
        if not Ai_Dialog.objectName():
            Ai_Dialog.setObjectName(u"Ai_Dialog")
        Ai_Dialog.resize(665, 208)
        self.verticalLayout = QVBoxLayout(Ai_Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(2, 2, 2, 2)
        self.txbTxtLog = QTextBrowser(Ai_Dialog)
        self.txbTxtLog.setObjectName(u"txbTxtLog")

        self.verticalLayout.addWidget(self.txbTxtLog)

        self.prbAutoDetect = QProgressBar(Ai_Dialog)
        self.prbAutoDetect.setObjectName(u"prbAutoDetect")
        self.prbAutoDetect.setValue(0)

        self.verticalLayout.addWidget(self.prbAutoDetect)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.btnCancel = QPushButton(Ai_Dialog)
        self.btnCancel.setObjectName(u"btnCancel")

        self.horizontalLayout.addWidget(self.btnCancel)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(Ai_Dialog)

        QMetaObject.connectSlotsByName(Ai_Dialog)
    # setupUi

    def retranslateUi(self, Ai_Dialog):
        Ai_Dialog.setWindowTitle(QCoreApplication.translate("Ai_Dialog", u"Ai \u70b9\u706b...", None))
        self.btnCancel.setText(QCoreApplication.translate("Ai_Dialog", u"Cancel", None))
    # retranslateUi

