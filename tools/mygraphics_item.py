# %% [markdown]
# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
# from tools.basic_methods import save_json
from tools.my_QGraphicsItemGroup import my_QGraphicsItemGroup

class MyGraphicsItem(QGraphicsItem):
    def __init__(self, x, y, width, scene, num, spacing, font_size, _point_type=0,parent=None):
    # def __init__(self, x, y, width, height, scene, num, spacing, font_size, _point_type=0,parent=None):
        super().__init__(parent)
        self.scene=scene
        self.num = num
        self.spacing = spacing
        self.width = width
        self.font_size = font_size
        self.point_type = _point_type
        transparency=180
        self.is_link =False

        if self.point_type==0:  
            # yellow
            color=QColor (255,255,0,transparency)
        elif self.point_type==1:
            # skyblue
            color=QColor(255,99,71,transparency)
        elif self.point_type==2:  
            # blueviolet
            color=QColor(138,43,226,transparency)

        brush=QBrush(color)

        # point
        self.item = QGraphicsEllipseItem(0, 0, self.width, self.width)
        self.item.setBrush(brush)  
        self.item.setPen(color)
        self.item.setZValue(2)
        # point id
        self.item.setData(self.num, self.num)  
        self.scene.addItem(self.item)
        self.item_x, self.item_y = self.item.boundingRect().x(), self.item.boundingRect().y()
        self.text = QGraphicsSimpleTextItem(str(self.num))
        font = self.text.font()
        font.setPointSize(self.font_size)
        # self.text.setBrush(QColor(255,0,0))  
        font.setBold(True)
        self.text.setFont(font)
        self.text.setZValue(2)
        self.text.setPos(self.item_x + self.spacing, self.item_y)
        self.scene.addItem(self.text)


        self.group = my_QGraphicsItemGroup()
        self.group.set_init(num ,self.scene)
        self.scene.addItem(self.group)
        self.group.addToGroup(self.text)
        self.group.addToGroup(self.item)
        self.group.setData(2, self.num)
        self.group.setPos(x-(self.width/2), y-(self.width/2))
        self.group.setZValue(2)
        self.group.setFlag(QGraphicsItem.ItemIsFocusable)
        self.group.setFlag(QGraphicsItem.ItemIsMovable)
        self.group.setFlag(QGraphicsItem.ItemIsSelectable)
        self.scene.clearSelection()
        self.group.setSelected(True)

        self.setFlag(QGraphicsItem.ItemIsFocusable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setCursor(Qt.SizeAllCursor)


    def set_is_link(self,is_link):
        # self.is_link = is_link
        self.group.set_group_is_link(is_link)

    def get_point_type(self):
        return self.point_type

    def set_point_type(self,_point_type):
        
        self.point_type=_point_type

    def set_point_psoition(self, x,y):
        self.group.setPos(x, y)

    def set_mouse_left(self,_is_read):
        # self.group.setFlag(QGraphicsItem.ItemIsFocusable,_is_read)
        self.group.setFlag(QGraphicsItem.ItemIsMovable,_is_read)
        self.group.setFlag(QGraphicsItem.ItemIsSelectable,_is_read)



# QGraphicsEllipseItem


    
