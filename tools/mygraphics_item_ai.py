# %% [markdown]
# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class MyGraphicsItem_ai(QGraphicsItem):
    # FONT_SIZE=6
    def __init__(self, x, y, width, scene, num, spacing, _point_type=0,parent=None):
    # def __init__(self, x, y, width, height, scene, num, spacing, font_size, _point_type=0,parent=None):
        super().__init__(parent)

        self.num = num
        self.spacing = spacing
        self.width = width
        self.point_type = _point_type
        transparency=180
        self.scene=scene
    
    
        self.width=self.width+1
        color=QColor (255,255,0,transparency)
        brush=QBrush(color)

    

        # point
        self.item = QGraphicsEllipseItem(0, 0, self.width, self.width)
        self.item.setPen(color)
        self.item.setZValue(2)
 

        # point id
        self.item.setData(self.num, self.num)  
        self.scene.addItem(self.item)
        self.item_x, self.item_y = self.item.boundingRect().x(), self.item.boundingRect().y()



        self.group = QGraphicsItemGroup()
        # self.group.set_init(num ,self.scene)
        self.scene.addItem(self.group)
        self.group.addToGroup(self.item)
        self.group.setData(2, self.num)
        self.group.setPos(x-(self.width/2), y-(self.width/2))
        self.group.setZValue(2)
        # self.group.setFlag(QGraphicsItem.ItemIsFocusable)
        # self.group.setFlag(QGraphicsItem.ItemIsMovable)
        # self.group.setFlag(QGraphicsItem.ItemIsSelectable)
        scene.clearSelection()
        self.group.setSelected(True)

        self.group.setFlag(QGraphicsItem.ItemIsMovable,False)



    # def get_clicked_item(self):
    #     self.scene. . group.focusItem()
    #     #  focusItem()

    def get_point_type(self):
        return self.point_type


    def set_point_psoition(self, x,y):
        self.group.setPos(x, y)





    # def set_mouse_left(self,_is_read):
    #     # self.group.setFlag(QGraphicsItem.ItemIsFocusable,_is_read)
    #     self.group.setFlag(QGraphicsItem.ItemIsMovable,_is_read)
    #     self.group.setFlag(QGraphicsItem.ItemIsSelectable,_is_read)

    # def mouseReleaseEvent(self, event):
    #     if event.button() == Qt.LeftButton:
    #         print("111")
    #         # df=open_json(self.path)
    #         # # print(df,self.id)
    #         # if self.cross_id==0:
    #         #     df['yolo'][self.id][0]=self.pos().x()
    #         #     df['yolo'][self.id][1]=self.pos().y()
    #         # elif self.cross_id==1:
    #         #     df['yolo'][self.id][2]=self.pos().x()
    #         #     df['yolo'][self.id][3]=self.pos().y()
    #         # 

    #     super().mouseReleaseEvent(event)
