# %% [markdown]
# v1.4.5
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
import sys
from tkinter.tix import Tree

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


from ui.ui_main_paint import Ui_PaintForm
from tools.mygraphics_item import MyGraphicsItem
from tools.mygraphics_item_ai import MyGraphicsItem_ai
from tools.mygraphics_line import MyGraphicsLine
from tools.mygraphics_line_2 import MyGraphicsLine_2
from tools.my_roi_rect import MyRoiRect
from tools.basic_methods import save_json,open_json,PATH_CONFIGURATION

from concurrent.futures import ThreadPoolExecutor

class Graphics_widget(QWidget):
    signal_return_new_point = Signal(list)
    signal_apply_get_is_lock = Signal(bool)
    signal_return_zoom_value_main = Signal(list)
    signal_return_new_yolo= Signal(list)

    value_changed = Signal(list)


    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_PaintForm()
        self.ui.setupUi(self)


        self.pix = None
        self.item_pic = None
        self.num = -1
        self.image_path=""
        
        self.point_obj_dict = {}
        self.point_obj_dict_ai = {}
        self.num_ai=0
        self.is_ai_model =False

        self.point_size = 6
        self.line_obj_dict = {}
        
        self.current_mode=None

        self.color_left=""
        self.color_right=""
        self.color_head=""

        self.pen_size=2

        self.spacing = 8
        self.font_size = 6

        self.lock_point=0
        self.original_scale = 1.0
        # self.is_edit_able=False
        self.temp_pos=[]
        self.is_running=True
        self.edit_able=True


        self.ui.graphicsView.mouseClicked.connect(self.onCreatePoint)
        self.ui.graphicsView.keyPress.connect(self.onChangePoint)
        self.ui.graphicsView.signal_return_zoom_value.connect(self.get_zoom_value)
        self.ui.graphicsView.mouseMove.connect(self.onLeavePos)

        
        self.scene = QGraphicsScene()  # 创建画布
        self.ui.graphicsView.setScene(self.scene)  # 把画布添加到窗口
        self.ui.graphicsView.show()
        

        # self.yolo_rect_list ={}
    
        self.setMouseTracking(True)

   
        self.yolo_roirect=None


        

    def set_read_only(self,_is_read):
        self.edit_able=_is_read
        for i in range(self.num + 1):
            self.point_obj_dict[i].set_mouse_left(_is_read)

    def create_yolo_rect(self,df_yolo,_id):
    
        self.yolo_roirect=MyRoiRect(self.scene,df_yolo[:2],df_yolo[2:],_id)



    def get_point_location(self,index):
        point_pos = self.point_obj_dict[index].item.scenePos()
        point = self.ui.graphicsView.mapFromScene(point_pos)
        point = self.mapToGlobal(point)
        return [point.x() + (self.point_size / 2),point.y()+(self.point_size / 2)]


    def set_model(self, model):
        self.current_mode=model
        self.point_num = model["points_num"]
        self.color_left=model["color"]["left"]
        self.color_right=model["color"]["right"]
        self.color_head=model["color"]["head"]
        self.create_item_line(model["pt_line"])

    def adjust_point_size(self, pic):
        if pic.width() >= 1920 or pic.height() >= 1080:
            self.point_size = 15
            self.spacing = 25
            self.font_size = 15
            self.pen_size=3
        else:
            self.pen_size=2
            self.point_size = 6
            self.spacing = 8
            self.font_size = 6


    def set_ai_model(self,is_ai):
        self.is_ai_model =is_ai

    def onLeavePos(self):
        if self.is_ai_model:
            if self.yolo_roirect!=None:
                yolo_pos=self.yolo_roirect.get_roi_pos()
                self.signal_return_new_yolo.emit([yolo_pos,self.yolo_roirect.get_yolo_id()]) 
                # print("1111",yolo_pos)
                # if yolo_pos!=

            return
        

        if self.edit_able==False:
            return
        select_item = self.scene.selectedItems()
        if not len(select_item):
            return
        select_id = select_item[0].data(2)
        if select_id is None:
            return 
        
        point_pos = self.point_obj_dict[select_id].item.scenePos()
        if self.temp_pos!=[point_pos.x() + (self.point_size / 2), point_pos.y() + (self.point_size / 2)]:
            self.temp_pos=[point_pos.x() + (self.point_size / 2), point_pos.y() + (self.point_size / 2)]

            new_point=[False,self.temp_pos[0],self.temp_pos[1],self.point_obj_dict[select_id].get_point_type(),select_id]
            self.signal_return_new_point.emit(new_point) 

    #    MyRoiRect


    @Slot()
    def open_image(self,fname):
        # fname = QFileDialog.getOpenFileName(self, "Open Image", self.open_image_dir, "*.jpg *.png *.JPG *.PNG")[
        #     0]
        # if len(fname) <= 0:
        #     return

        self.image_path = fname
        self.close_image()
        self.pic = QPixmap(fname)
        self.item_pic = QGraphicsPixmapItem(self.pic)
        # item.setFlags(QGraphicsItem.ItemIsMovable)
        self.item_pic.setZValue(0)
        self.scene.addItem(self.item_pic)
        # # 平滑缩放，平滑后看不到像素点块
        self.item_pic.setTransformationMode(Qt.SmoothTransformation)
        # 让image填充显示窗口
        self.ui.graphicsView.fitInView(self.item_pic, Qt.KeepAspectRatio)
        self.num = -1
        # print(self.item_pic.sceneBoundingRect())
        self.scene.setSceneRect(0, 0, self.pic.width(), self.pic.height())
        self.adjust_point_size(self.pic)

    
    # @Slot()
    # def zoom_control(self, scale):
    #     self.signal_return_zoom_value.emit(scale) 
    
    def get_zoom_value(self, _new_zoom_value):
        new_zoom_value=[_new_zoom_value[0],_new_zoom_value[1],True]

        self.signal_return_zoom_value_main.emit(new_zoom_value) 
        # print("new_zoom_value",new_zoom_value)


    @Slot()
    def view_rotate(self,value):
        self.ui.graphicsView.rotate(value)
    def clear_point(self):
        self.open_image(self.image_path)

    @Slot()
    def view_reset(self):
        self.ui.graphicsView.setTransform(QTransform())
        self.ui.graphicsView.fitInView(self.item_pic, Qt.KeepAspectRatio)
        self.ui.graphicsView.set_current_zoom_value([100,100])
        self.view_rotate(0)

    @Slot()
    def close_image(self):
        # self.ui.graphicsView.setTransform(QTransform())
        # self.scene.clear()

        self.ui.graphicsView.setTransform(QTransform())
        self.ui.graphicsView.scale(1, 1)
        self.ui.graphicsView.current_view_scale = 100
        self.ui.graphicsView.original_view_scale = 1
        self.scene.clear()

    def resizeEvent(self, event):
        self.ui.graphicsView.sceneRect()

    def create_item_line(self, pt_data):

        left = [self.generate_only_id(i[0], i[1]) for i in pt_data['left']]
        right = [self.generate_only_id(i[0], i[1]) for i in pt_data['right']]
        head = [self.generate_only_id(i[0], i[1]) for i in pt_data['head']]
        self.total_list = left + right + head

        for i in self.total_list:
            self.line_obj_dict[i] = MyGraphicsLine(self.scene)


    def generate_only_id(self, a, b):
        return max(a, b) + (min(a, b) << 32) 

    def get_lock_point(self,return_data):
        self.lock_point=return_data

    def load_scale(self, current_scale, flag):
        # print()
        # current_scale=current_scale/100
        if flag:
            self.ui.graphicsView.scale(current_scale, current_scale)
            self.ui.graphicsView.current_view_scale = current_scale * 100
        else:
            self.ui.graphicsView.scale(self.original_scale, self.original_scale)
   
       
        self.ui.graphicsView.original_view_scale = 1
    def zoom_scene(self, current_scale):
        # print(current_scale)
        self.ui.graphicsView.zoom(current_scale)
    
    def set_zoom(self,initial_zoom_value):
        self.ui.graphicsView.set_zooming(initial_zoom_value)


    @Slot()
    def onCreatePoint(self,point):
        if self.edit_able==False:
            return
        self.signal_apply_get_is_lock.emit(self.lock_point) 
        if self.lock_point>0:
            pt = self.ui.graphicsView.mapToScene(point)
            border_width = self.item_pic.sceneBoundingRect().width()
            border_height = self.item_pic.sceneBoundingRect().height()
            if pt.x() < 0 or pt.y() < 0 or pt.x() > border_width or pt.y() > border_height:
                return

            self.num += 1
            if self.num >= self.point_num:
                return

            point_type=0
            if QApplication.keyboardModifiers() == Qt.CTRL:  
                point_type=1
            elif QApplication.keyboardModifiers() == Qt.SHIFT:  
                point_type=2


            self.point_obj_dict[self.num] = MyGraphicsItem(pt.x(), pt.y(),
                                                        self.point_size,
                                                        self.scene,
                                                        self.num,
                                                        self.spacing,
                                                        self.font_size,
                                                        point_type)
            
            new_point=[True,pt.x(),pt.y(),point_type,self.num]
            self.signal_return_new_point.emit(new_point) 

    


    def load_point(self, index,point,pointy_type):
        self.num=int(index)

        self.point_obj_dict[self.num] = MyGraphicsItem(point[0], point[1],self.point_size,
                                                       self.scene,
                                                       self.num,
                                                       self.spacing,
                                                       self.font_size,
                                                       int(pointy_type))
        # self.point_obj_dict[self.num].group.signal_mouseRelease_update_id.connect(self.mouseRelease_update_id)
        
 
        
    def load_point_ai(self, index,point,pointy_type):
        self.num_ai=int(index)
        self.point_obj_dict_ai[self.num_ai] = MyGraphicsItem_ai(point[0], point[1],self.point_size,
                                                       self.scene,
                                                       self.num_ai,
                                                       self.spacing,
                                                       int(pointy_type))


    def compare_link_manual_ai(self,is_link):
        for i in range(len(self.point_obj_dict)):
            self.point_obj_dict[i].set_is_link(is_link)


    
    @Slot()
    def onChangePoint(self, event):
       
        select_item = self.scene.selectedItems()
        if not len(select_item):
            return
        item_group = select_item[0]
        key = event.key()
        select_id = item_group.data(2)
        color=QColor (255,255,0,180)
        if key == Qt.Key_3:
            self.point_obj_dict[select_id].set_point_type(2)
            color=QColor(138,43,226,180)

        elif key == Qt.Key_2:
            self.point_obj_dict[select_id].set_point_type(1)
            color=QColor(225,99,71,180)

        elif key == Qt.Key_1:
            self.point_obj_dict[select_id].set_point_type(0)
            

        # is_ai=self.point_obj_dict[select_id].get_is_ai()
        # if is_ai==False:
        self.point_obj_dict[select_id].item.setBrush(QBrush(color))
        self.point_obj_dict[select_id].item.setPen(QPen(color))

      
        point_pos=self.point_obj_dict[select_id].item.scenePos()
        new_point=[False,point_pos.x(),point_pos.y(),self.point_obj_dict[select_id].get_point_type(),select_id]
        self.signal_return_new_point.emit(new_point) 

    def get_all_point(self):
        df={}
        for i in range(0,self.num+1):
            point_pos = self.point_obj_dict[i].item.scenePos()
         
            tem={str(i):[point_pos.x() + (self.point_size / 2), point_pos.y() + (self.point_size / 2),self.point_obj_dict[i].get_point_type()]}
            df.update(tem)
            
        return df

    def return_point_type(self,point_type):
        if point_type[1]==0:
            new_point_type=0
            color=QColor(255,255,0,180)
        elif point_type[1]==1:
            new_point_type=1
            color=QColor(255,99,71,180)
        elif point_type[1]==2:
            new_point_type=2
            color=QColor(138,43,226,180)
        self.point_obj_dict[point_type[0]].set_point_type(new_point_type)
        if self.point_obj_dict[point_type[0]].get_is_ai()==False:
            self.point_obj_dict[point_type[0]].item.setBrush(QBrush(color))
        self.point_obj_dict[point_type[0]].item.setPen(QPen(color))


    def paintEvent(self, event):
        if self.current_mode==None:
            return 
        if self.is_running==False:
            return
 
        self.pen_left = QPen(QColor(self.color_left), self.pen_size, Qt.SolidLine)
        self.pen_right = QPen(QColor(self.color_right), self.pen_size, Qt.SolidLine)
        self.pen_head = QPen(QColor(self.color_head), self.pen_size, Qt.SolidLine)
       

        for i in self.current_mode["pt_line"]["left"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = self.pen_left
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

        for i in self.current_mode["pt_line"]["right"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = self.pen_right
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

        for i in self.current_mode["pt_line"]["head"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = self.pen_head
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

        self.update()


    def draw_line_2(self,model,color):
        left = [self.generate_only_id(i[0], i[1]) for i in model['left']]
        right = [self.generate_only_id(i[0], i[1]) for i in model['right']]
        head = [self.generate_only_id(i[0], i[1]) for i in model['head']]
        total_list = left + right + head
        # print(total_list)

        for i in total_list:
            # print (i)
            self.line_obj_dict[i] = MyGraphicsLine_2(self.scene)
        
        

        self.pen_size=2
        pen_head= QPen(QColor(color["head"]), self.pen_size, Qt.SolidLine)
        pen_left = QPen(QColor(color["left"]), self.pen_size, Qt.SolidLine)
        pen_right= QPen(QColor(color["right"]), self.pen_size, Qt.SolidLine)

        for i in model["left"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_left
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))
                # print("1111")

        for i in model["right"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_right
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

        for i in model["head"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_head
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

    def draw_line(self,model):
        self.create_item_line(model["pt_line"])
        pen_head= QPen(QColor(model["color"]["head"]), self.pen_size, Qt.SolidLine)
        pen_left = QPen(QColor(model["color"]["left"]), self.pen_size, Qt.SolidLine)
        pen_right= QPen(QColor(model["color"]["right"]), self.pen_size, Qt.SolidLine)

        for i in model["pt_line"]["left"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_left
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))


        for i in model["pt_line"]["right"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_right
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))

        for i in model["pt_line"]["head"]:
            if i[0] <= self.num and i[1] <= self.num:
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].pen = pen_head
                self.line_obj_dict[self.generate_only_id(i[0], i[1])].update_positions(
                    self.point_obj_dict[i[0]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[0]].item.scenePos().y() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().x() + (self.point_size / 2),
                    self.point_obj_dict[i[1]].item.scenePos().y() + (self.point_size / 2))


    def get_point_id(self):
        select_item = self.scene.selectedItems()
        if not len(select_item):
            return
        point_id = select_item[0].data(2)
        return point_id
        

    def keyPressEvent(self, event):

        select_id=self.get_point_id()
        point_pos = self.point_obj_dict[select_id].item.scenePos()
        if event.key()== Qt.Key_A:
            self.point_obj_dict[select_id].set_point_psoition(point_pos.x()-1, point_pos.y())
        elif event.key()== Qt.Key_D:
            self.point_obj_dict[select_id].set_point_psoition(point_pos.x()+1, point_pos.y())
        elif event.key()== Qt.Key_W:
            self.point_obj_dict[select_id].set_point_psoition(point_pos.x(), point_pos.y()-1)
        elif event.key()== Qt.Key_S:
            self.point_obj_dict[select_id].set_point_psoition(point_pos.x(), point_pos.y()+1)

        new_point=[False,point_pos.x(),point_pos.y(),self.point_obj_dict[select_id].get_point_type(),select_id]
        self.signal_return_new_point.emit(new_point) 
      

    def check_point_pos(self,index,position):
        if position[0]<0 or position[0]>self.pic.width() or position[1]<0  or position[1]>self.pic.height():
            self.point_obj_dict[index].set_point_psoition(0, 0)
            # point_pos = self.point_obj_dict[index].item.scenePos()
            new_point=[False,0,0,self.point_obj_dict[index].get_point_type(),index]
            self.signal_return_new_point.emit(new_point) 
            # self.draw_line(self.current_mode)


    def isShowPoint(self, select_id, state=True):
        for i in range(self.num+1):
            if i == select_id:
                self.point_obj_dict[i].group.setVisible(state)
            else:
                self.point_obj_dict[i].group.setVisible(not state)

    def show_all_point(self):
        try:
            for i in range(len(self.point_obj_dict)+1):
                self.point_obj_dict[i].group.setVisible(True)
        except:
            pass

    def isShowLine(self, state):
        for i in self.total_list:
            self.line_obj_dict[i].setVisible(state)






