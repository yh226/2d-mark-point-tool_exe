# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
import os
import sys
import numpy as np
import cv2
from multiprocessing import Process, Queue

class Pose2dManager:
    def __init__(self, buffer_size=20):
        self.buffer_size = buffer_size
        self.input_queue = None
        self.output_queue = None
        self.process = None
    
    def openProcess(self, pose2d_runner, meta):
        self.closeProcess()
        self.input_queue = Queue(self.buffer_size)
        self.output_queue = Queue(self.buffer_size)
        self.process = Process(target=pose2d_runner,
                               args=(self.input_queue,
                                     self.output_queue,
                                     meta))
        self.process.start()
    
    def clearInput(self):
        if self.input_queue is None:
            return
        for _ in range(self.input_queue.qsize()):
            try:
                queue.get(False)
            except:
                pass
            
    def clearOutput(self):
        if self.output_queue is None:
            return
        for _ in range(self.output_queue.qsize()):
            try:
                self.output_queue.get(False)
            except:
                pass
    
    def closeProcess(self):
        if self.input_queue is not None:
            self.clearInput()
        if self.output_queue is not None:
            self.clearOutput()
        if self.process is not None:
            self.process.terminate()
        self.process = None
        self.input_queue = None
        self.output_queue = None
    
    def readMsg(self):
        if not self.process.is_alive():
            return [0, None]
        ret = [0, None]
        if not self.output_queue.empty():
            ret = self.output_queue.get(False)
        return ret