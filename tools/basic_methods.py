# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
import numpy as np
import json

PATH_CONFIGURATION='./resources/files/config.json'
manual_position_TEMP_PATH='./resources/files/temp/ai_position.json'
PATH_NEW_MANUAL_POSITION='./resources/files/temp/manual_new_position.json'
def open_json_basic(path, key=None):
    try:
        file = open(path, 'r')
        content = file.read()
        df = json.loads(content)
        file.close()
        if key != None:
            return df[key]
        else:
            return df
    except:
        pass


def open_json(path,key=None):
    try:
        file = open(path, 'r')
        content = file.read()
        df = json.loads(content)

        file.close()
        if key!=None:
            return df[key]
        else:
            return df
    except:
        return False
        pass

def save_json(df, path):
    json_file = json.dumps(df)
    file = open(path, 'w')
    file.write(json_file)
    file.close()

def convert_old_json(self,df):
        if "im_name" in df.keys():
            tem={}
            tem2={}
            for i in df["person"]:
                point_id=0
                for j in i["kp"]:
                    tem.update({str(point_id):[j[0],j[1],0]})
                    point_id+=1
                tem2.setdefault(str(i["id"]), {}).update(tem)
            df={"image_id": df["im_name"],"point":tem2,"model":self.ui.comboBox_model.currentText()}
            
        return df

def convert(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0
    y = (box[2] + box[3]) / 2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return x, y, w, h
def save_text_yolo(yolo_id,yolo, size,path):
    # df=[x,y,x,y]
    df=convert(size,yolo)
    with open(path,"w") as f:
        f.write(str(yolo_id)+" "+str(df[0])+" "+ str(df[1])+" "+str(df[2])+" "+str(df[3]))

