
import os,sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

# sys.path.append('../ui/')
from ui.ui_settings_dialog import Ui_settingsDialog
from tools.basic_methods import open_json_basic,save_json,PATH_CONFIGURATION

class SettingsDialog(QDialog):
    # changeEnglishSignal = Signal()
    # changeChineseSignal = Signal()
    saveConfigSignal = Signal()
    PATH_CONFIGURATION=PATH_CONFIGURATION

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_settingsDialog()
        self.ui.setupUi(self)

        self.yoloModelDir = None
        self.pose2dDir = None

        self.setting_trans = QTranslator()
        self.open_config()


        # yolo
        self.ui.btnYoloOpen.clicked.connect(self.openYoloModelClicked)
        # pose2d
        self.ui.btnOpenKP.clicked.connect(self.openPoss2DClicked)

        self.ui.pushButton_save.clicked.connect(self.save_config)
        self.ui.pushButton_cancel.clicked.connect(self.cancel)

        
    def open_config(self):
        df=open_json_basic(self.PATH_CONFIGURATION)
        self.setSettingsConfig(df)

    def save_config(self):
        self.getSettingsConfig()
        save_json(self.setting_config,self.PATH_CONFIGURATION)
        self.close()

    def cancel(self):
        self.open_config()
        self.close()


    def openYoloModelClicked(self):
        self.yoloModelDir = os.path.dirname(self.ui.lblYoloModel.text())
        fname = QFileDialog.getOpenFileName(self, "Open Yolo Model", self.yoloModelDir, "*.onnx")[0]
        if os.path.exists(fname):
            self.ui.lblYoloModel.setText(fname)
            self.ui.lblWarning.setText("")
            self.yoloModelDir = os.path.dirname(fname)
        else:
            self.ui.lblWarning.setText(">>>>Please choose available Yolo model.<<<<")

    def openPoss2DClicked(self):
        self.pose2dDir = os.path.dirname(self.ui.lblModelKP.text())
        fname = QFileDialog.getOpenFileName(self, "Open Poss2d Model", self.pose2dDir, "*.onnx")[0]
        if os.path.exists(fname):
            self.ui.lblModelKP.setText(fname)
            self.ui.lblWarning.setText("")
            self.pose2dDir = os.path.dirname(fname)
        else:
            self.ui.lblWarning.setText(">>>>Please choose available Pose2d model.<<<<")



   

    # def getCmbLanguageCurrentIndex(self):
    #     return self.ui.cmbLanguage.currentIndex()

    def getSettingsConfig(self):
        self.setting_config = {}
        # yolo
        self.setting_config["yoloModelPath"] = self.ui.lblYoloModel.text()
        self.setting_config["yoloIOU"] = self.ui.spbYoloIOU.value()
        self.setting_config["yoloConf"] = self.ui.spbYoloConf.value()
        # pose2d
        self.setting_config["pose2dModelPath"] = self.ui.lblModelKP.text()

        self.setting_config["pose2dNumMaxPeople"] = self.ui.spbNumMaxPeople.value()

        # tdto
        # setting_config["tdtoModelPath"] = self.ui.lblTdToModel.text()
        # # mqtt
        # setting_config["mqttHost"] = self.ui.lblHost.text()
        # setting_config["mqttPort"] = self.ui.spbPort.value()
        # setting_config["mqttlTopic"] = self.ui.lblTopic.text()
        # # setting_config["openMqtt"] = self.ui.chkOpenMqtt.isChecked()
        # # language
        # setting_config["language"] = self.ui.cmbLanguage.currentText()
        # setting_config["newVideoFmtState"] = self.ui.chkNewVideoFmtState.isChecked()
        return self.setting_config

    def setSettingsConfig(self, setting_config):
        # yolo
        self.ui.lblYoloModel.setText(setting_config["yoloModelPath"])
        self.ui.spbYoloIOU.setValue(setting_config["yoloIOU"])
        self.ui.spbYoloConf.setValue(setting_config["yoloConf"])
        # pose2d
        self.ui.lblModelKP.setText(setting_config["pose2dModelPath"])

        self.ui.spbNumMaxPeople.setValue(setting_config["pose2dNumMaxPeople"])


        # # tdto
        # self.ui.lblTdToModel.setText(setting_config["tdtoModelPath"])
        # # mqtt
        # self.ui.lblHost.setText(setting_config["mqttHost"])
        # self.ui.spbPort.setValue(setting_config["mqttPort"])
        # self.ui.lblTopic.setText(setting_config["mqttlTopic"])
        # # self.ui.chkOpenMqtt.setChecked(setting_config["openMqtt"])
        # # language
        # self.setCmbLanguage(self.ui.cmbLanguage, setting_config["language"])
        # self.ui.chkNewVideoFmtState.setChecked(setting_config["newVideoFmtState"])

    # def setCmbLanguage(self, cmb, language: str):
    #     if language == "汉语":
    #         language = "Chinese"
    #     elif language == "英语":
    #         language = "English"
    #     for i in range(cmb.count()):
    #         text = cmb.itemText(i)
    #         if text == language:
    #             cmb.setCurrentIndex(i)

    def accept(self):
        if not os.path.exists(self.ui.lblYoloModel.text()):
            self.ui.lblWarning.setText("yoloModelPath Model File is null")
            return
        if not os.path.exists(self.ui.lblModelKP.text()):
            self.ui.lblWarning.setText("pose2dModelPath Model File is null")
            return

        # self.saveConfigSignal.emit()
        # super().accept()
        # self.ui.lblWarning.setText("")
