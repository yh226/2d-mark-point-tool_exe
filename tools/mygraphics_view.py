# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class MyGraphicsView(QGraphicsView):
    mouseClicked = Signal(QPoint)
    keyPress = Signal(QKeyEvent)
    # scaleView = Signal(int)
    mouseMove = Signal()
    signal_return_zoom_value=Signal(list)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        # self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        # self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor(self.AnchorUnderMouse)
        self.original_view_scale = 1
        self.current_view_scale = 100
        self.start_value=100
        self.current_zoom_value=[100,100]
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton:
            point = event.pos()
            self.mouseClicked.emit(point)
        super().mousePressEvent(event)

    def keyPressEvent(self, event):
        self.keyPress.emit(event)
        super().keyPressEvent(event)

    def mouseMoveEvent(self, event):
        self.mouseMove.emit()
        super().mouseMoveEvent(event)
   

    def zoom(self,zoom_value_list):
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        # self.current_zoom=zoom_value
        if zoom_value_list[1]> zoom_value_list[0]:
            self.scale(1.1, 1.1)
        elif zoom_value_list[1]< zoom_value_list[0]:
            self.scale(0.9, 0.9)
        self.set_current_zoom_value(zoom_value_list)
        
       
        self.setTransformationAnchor(self.AnchorUnderMouse)
    def set_current_zoom_value(self,zoom_value_list):
        self.current_zoom_value=zoom_value_list



    def wheelEvent(self, event):
        # self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        if event.angleDelta().y() > 0:
            self.current_zoom_value[0]=self.current_zoom_value[1]
            self.current_zoom_value[1]=self.current_zoom_value[1]+10
            if self.current_zoom_value[1]>500:
                self.current_zoom_value[1]=500

            # self.signal_return_zoom_value.emit(self.current_zoom_value)
        elif event.angleDelta().y() < 0:
            self.current_zoom_value[0]=self.current_zoom_value[1]
            self.current_zoom_value[1]=self.current_zoom_value[1]-10
            if self.current_zoom_value[1]<50:
                self.current_zoom_value[1]=50
        self.signal_return_zoom_value.emit(self.current_zoom_value)

    def set_zooming(self,initial_zoom_value):
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        # self.current_zoom=zoom_value
        if initial_zoom_value>0:
            i=int(initial_zoom_value)
            while i:
                self.scale(1.1, 1.1)
                i-=1
        elif initial_zoom_value<0:
            i=int(abs(initial_zoom_value))
            while i:
                self.scale(0.9, 0.9)
                i-=1
            
       
        self.setTransformationAnchor(self.AnchorUnderMouse)
        
        



    def onScaleView(self, scale):
        self.original_view_scale *= scale
        return int((self.original_view_scale / 1) * 100)







