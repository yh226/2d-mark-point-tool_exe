# %% [markdown]
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2023-09-06


from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from tools.mygraphics_line_ai import MyGraphicsLine_ai
from tools.basic_methods import open_json,save_json,manual_position_TEMP_PATH,PATH_NEW_MANUAL_POSITION

class my_QGraphicsItemGroup(QGraphicsItemGroup):
    
    # signal_mouseRelease_update_id= Signal(int)
    def set_init(self,_id,_scene):
        self.id=_id
        self.scene=_scene
        self.point_size=3
        self.is_link=False
        self.path=None
        # self.new_position={}
    
    def set_group_is_link(self,is_link):
        self.is_link=is_link
        
 

    def mouseReleaseEvent(self, event):
        # print("mouse") 
        if self.is_link and event.button() == Qt.LeftButton:
            df_manual=open_json(manual_position_TEMP_PATH)[str(self.id)]
 
            for item in self.scene.items():
                if isinstance(item, MyGraphicsLine_ai):
                    self.scene.removeItem(item)

            dis=(((self.pos().x()+self.point_size) - df_manual[0])**2 + ((self.pos().y()+self.point_size) - df_manual[1])**2)**0.5
            if dis>1:
                
                self.line_manual_ai=MyGraphicsLine_ai(self.scene)
                self.line_manual_ai.pen = QPen(QColor(128,128,128), 1, Qt.DashLine)
                
                self.line_manual_ai.update_positions(
                        self.pos().x()+self.point_size,
                        self.pos().y()+self.point_size,
                        df_manual[0],
                        df_manual[1]
                        )
            # df[str(self.id)]=[self.pos().x(), self.pos().y()]
            # self.setPos(self.pos().x(), self.pos().y())
            # self.new_position[self.id]=[self.pos().x(),self.pos().y()]
            df=open_json(PATH_NEW_MANUAL_POSITION)
            if df==False:
                save_json({self.id:[self.pos().x(),self.pos().y(),0]},PATH_NEW_MANUAL_POSITION)
            else:
                df[self.id]=[self.pos().x(),self.pos().y(),0]
                save_json(df,PATH_NEW_MANUAL_POSITION)
        super().mouseReleaseEvent(event)