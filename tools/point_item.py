# v0.1

# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > 2022-08-01
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
import sys
# sys.path.append('./tools')
# from feature_edit import Feature_Edit
# from x import x

class Point_Item(QWidget):

    signal_return_point_type=Signal(list)
    signal_return_redraw_point_id=Signal(int)
    # signal_edit_main_id=Signal(str)
    def __init__(self):
        super().__init__()
        # QThread.__init__(self)

        self.item = QListWidgetItem()
        # self.feature_edit=Feature_Edit()
        # self.signal_item_edit.connect(self.track_edit.get_data)

        self.initUI()
        self.point_list=[]


    
    
    def initUI(self):
        self.horizontalLayoutWidget = QWidget()

        # -----UI INSERT--------------------------------
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 20, 481, 171))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.radioButton_id = QRadioButton(self.horizontalLayoutWidget)
        self.radioButton_id.setObjectName(u"radioButton_id")
        self.radioButton_id.setStyleSheet(u"color:red")

        self.horizontalLayout.addWidget(self.radioButton_id)

        self.label_id = QLabel(self.horizontalLayoutWidget)
        self.label_id.setObjectName(u"label_id")

        self.horizontalLayout.addWidget(self.label_id)

        self.label_location = QLabel(self.horizontalLayoutWidget)
        self.label_location.setObjectName(u"label_location")

        self.horizontalLayout.addWidget(self.label_location)

        self.comboBox_type = QComboBox(self.horizontalLayoutWidget)
        self.comboBox_type.addItem("")
        self.comboBox_type.addItem("")
        self.comboBox_type.addItem("")
        self.comboBox_type.setObjectName(u"comboBox_type")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBox_type.sizePolicy().hasHeightForWidth())
        self.comboBox_type.setSizePolicy(sizePolicy)
        self.comboBox_type.setMinimumSize(QSize(80, 0))
        self.comboBox_type.setMaximumSize(QSize(80, 16777215))

        self.horizontalLayout.addWidget(self.comboBox_type)

        self.pushButton_redraw_point = QPushButton(self.horizontalLayoutWidget)
        self.pushButton_redraw_point.setObjectName(u"pushButton_redraw_point")
        self.pushButton_redraw_point.setMinimumSize(QSize(50, 0))

        self.horizontalLayout.addWidget(self.pushButton_redraw_point)

        self.horizontalLayout.setStretch(1, 10)
        self.horizontalLayout.setStretch(2, 55)
        self.horizontalLayout.setStretch(3, 20)
        self.horizontalLayout.setStretch(4, 5)


        # -----END UI INSERT--------------------------------
        # self.comboBox_type.setEnabled(False)
        self.radioButton_id.setVisible(False)
        self.radioButton_id.setText("")
        self.comboBox_type.setItemText(0, QCoreApplication.translate("Form", u"0:\u5185", None))
        self.comboBox_type.setItemText(1, QCoreApplication.translate("Form", u"1:\u5916", None))
        self.comboBox_type.setItemText(2, QCoreApplication.translate("Form", u"2:\u7a7a", None))
        

        self.pushButton_redraw_point.setText(QCoreApplication.translate("Form", u"\u91cd\u753b", None))
        # self.pushButton_done.setText(QCoreApplication.translate("Form", u"\u5b8c\u6210", None))

        self.setLayout(self.horizontalLayout)

        # self.pushButton_done.setEnabled(False)

        '''button'''
        self.comboBox_type.currentIndexChanged.connect(self.post_point_type)
        self.pushButton_redraw_point.clicked.connect(self.redraw_point)
        # self.pushButton_done.clicked.connect(self.redraw_point_finish)
    

    
    '''set'''
    def create_point_item(self,id, _point_dict):

        self.point_list=_point_dict
        self.label_id.setText(str(id))

        self.label_location.setText(str([round(_point_dict[0],2),round(_point_dict[1],2)]))
        # if _point_dict[2]
        self.comboBox_type.setCurrentIndex(_point_dict[2])
    def set_point_group(self,group):
        self.label_location.setText(str([round(group[0],2),round(group[1],2)]))
        self.point_list=group


    '''get'''
    def get_point_id(self):
        return self.label_id.text()
    def get_point_position(self):
        return self.point_list
        # return self.label_location.text()
    def get_point_widget(self):
        return self


    def post_point_type(self):
        if self.comboBox_type.currentText()== u"0:\u5185":
            return_data=0
        elif self.comboBox_type.currentText()== u"1:\u5916":
            return_data=1
        else:
            return_data=2
        # id=self.label_id.text().replace("_ai","")
        self.signal_return_point_type.emit([int(self.label_id.text()),return_data])

    def redraw_point(self):
        # id=self.label_id.text().replace("_ai","")
        self.signal_return_redraw_point_id.emit(int(self.label_id.text()))

        # self.pushButton_redraw_point.setEnabled(False)
        # self.pushButton_done.setEnabled(True)

    # def redraw_point_finish(self):
      
    #     self.signal_return_redraw_point_id.emit([int(self.label_id.text()),False,int(self.label_id.text())])
        
    def set_available(self,is_lock):
        
        self.comboBox_type.setEnabled(is_lock)
        self.pushButton_redraw_point.setEnabled(is_lock)
    def high_light(self):
        self.radioButton_id.setVisible(True)
        self.radioButton_id.setChecked(True)

      
    def un_high_light(self):
        self.radioButton_id.setChecked(False)
        self.radioButton_id.setVisible(False)
       
        # 
        # return self.label_id.text()
    
    # def keyPressEvent(self): 
      
    #     if QApplication.keyboardModifiers() == Qt.CTRL:
    #         print("a")
    #                     # self.update_x+=1
    #         # # self.update_y+=1
    #         # # print(self.update_x,self.update_y)
    #         # self.signal_post_point_new_location.emit([10,self.update_x,self.update_x]) 
