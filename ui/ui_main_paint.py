# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_main_paintPzGYeg.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from tools.mygraphics_view import MyGraphicsView 


class Ui_PaintForm(object):
    def setupUi(self, PaintForm):
        if not PaintForm.objectName():
            PaintForm.setObjectName(u"PaintForm")
        PaintForm.resize(862, 697)
        self.verticalLayout = QVBoxLayout(PaintForm)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.graphicsView = MyGraphicsView(PaintForm)
        self.graphicsView.setObjectName(u"graphicsView")

        self.verticalLayout.addWidget(self.graphicsView)


        self.retranslateUi(PaintForm)

        QMetaObject.connectSlotsByName(PaintForm)
    # setupUi

    def retranslateUi(self, PaintForm):
        PaintForm.setWindowTitle(QCoreApplication.translate("PaintForm", u"Form", None))
    # retranslateUi

