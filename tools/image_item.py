# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > 2022-08-01
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
import sys
# sys.path.append('./tools')
# from feature_edit import Feature_Edit
# from x import x

class Image_Item(QWidget):

    # signal_remove_main_id = Signal(str) 
    signal_main_image_path=Signal(str)
    def __init__(self):
        super().__init__()
        # QThread.__init__(self)

        self.item = QListWidgetItem()
        # self.feature_edit=Feature_Edit()
        # self.signal_item_edit.connect(self.track_edit.get_data)

        self.initUI()
        self.path=""

    
    
    def initUI(self):
        self.horizontalLayoutWidget = QWidget()

        # -----UI INSERT--------------------------------
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(100, 70, 199, 80))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.radioButton = QRadioButton(self.horizontalLayoutWidget)
        self.radioButton.setObjectName(u"radioButton")

        self.horizontalLayout.addWidget(self.radioButton)

        self.label_image_name = QLabel(self.horizontalLayoutWidget)
        self.label_image_name.setObjectName(u"label_image_name")

        self.horizontalLayout.addWidget(self.label_image_name)

        self.pushButton_review = QPushButton(self.horizontalLayoutWidget)
        self.pushButton_review.setObjectName(u"pushButton_review")
        self.pushButton_review.setMinimumSize(QSize(10, 0))

        self.horizontalLayout.addWidget(self.pushButton_review)

        self.horizontalLayout.setStretch(0, 5)
        self.horizontalLayout.setStretch(1, 85)
        self.horizontalLayout.setStretch(2, 10)

        # -----END UI INSERT--------------------------------
        self.label_image_name.setWordWrap(True) 
        self.radioButton.setVisible(False)
        self.pushButton_review.setText(QCoreApplication.translate("Form", u">", None))
        

        self.pushButton_review.clicked.connect(self.review)  


        self.setLayout(self.horizontalLayout)

    '''button'''
    def review(self):
        self.signal_main_image_path.emit(self.path)


        # return 
        
        # self.bt = QPushButton("111")
        # self.dockWidget_new = QDockWidget(self.path, self)
        # self.dockWidget_new.setAllowedAreas(Qt.LeftDockWidgetArea |Qt.RightDockWidgetArea)
        # self.dockWidget_new.setWidget(self.bt)
   

    '''set'''
    def set_image_name(self,name):
        self.label_image_name.setText(name)
    def set_image_path(self,path):
        self.path=path



    '''get'''
    def get_image_name(self):
        return self.label_image_name.text()
    def get_image_path(self):
        return self.path
    def get_image_widget(self):
        return self

    def high_light(self):

        self.radioButton.setVisible(True)
        self.radioButton.setChecked(True)
        self.radioButton.setStyleSheet("color: red")
        # self.label_image_name.setStyleSheet(u"color:red")
        # self.pushButton_review.setStyleSheet(u"color:red")
        # self.label_image_name.setWordWrap(True) 
      
    def un_high_light(self):
        self.radioButton.setChecked(False)
        self.radioButton.setVisible(False)
        # self.widget.setStyleSheet("border:0px solid red")
        # self.label_image_name.setStyleSheet(u"color:black")
        # self.pushButton_review.setStyleSheet(u"color:black")
        # self.label_image_name.setWordWrap(True) 
 


    
