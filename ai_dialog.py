import time
from PySide2.QtWidgets import QApplication, QDialog, QVBoxLayout, QProgressBar, QPushButton
from PySide2.QtCore import QThread, Signal, Slot

class WorkerThread(QThread):
    progress_signal = Signal(int)
    finished_signal = Signal()
    def __init__(self):
        super().__init__()
        self._running = True
    def run(self):
        i=0
        while self._running:
            time.sleep(0.5)
            self.progress_signal.emit(i)
            if i<99:
                i=i+1
            # if i == 99:
            #     print("Worker thread")
                # self.finished_signal.emit()
                # self.quit()
                # cancelTask

    def stop(self):
        # if is_run==False:
        #     self.finished_signal.emit()
        #     self.quit()
        self._running = False

class ProgressDialog(QDialog):
    stop_signal = Signal(bool)
    def __init__(self):
        super().__init__()

        self.setWindowTitle("AI 点火..")
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.progressBar = QProgressBar()
        self.layout.addWidget(self.progressBar)

        self.cancelButton = QPushButton("Cancel")
        self.cancelButton.clicked.connect(self.cancelTask)
        self.layout.addWidget(self.cancelButton)

        self.is_run=True

        self.workerThread = WorkerThread()
        self.workerThread.progress_signal.connect(self.updateProgressBar)
        
        self.workerThread.finished_signal.connect(self.taskFinished)
        self.stop_signal.connect(self.workerThread.stop)



    def startTask(self):
        self.progressBar.setValue(0)
        self.workerThread.start()

    def updateProgressBar(self, value):
        self.progressBar.setValue(value)

    def taskFinished(self):
        self.progressBar.setValue(100)
        # self.cancelButton.setEnabled(False)
        self.cancelButton.setText("Finished")
        self.cancelTask()


    def cancelTask(self):
        self.workerThread.quit()
        self.close()
        

    
    def get_is_finished(self,is_run):
        # self.is_run=is_run
        if is_run==False:
            self.workerThread.stop()
            self.taskFinished()
    # def exit_di)


            # self.stop_signal.emit(is_run) 
        # print(self.is_run)
        # if self.is_run==False:
        #     self.taskFinished()
    # def send_stop_signal(self,is_done):
    #     return self.is_done


# if __name__ == "__main__":
#     app = QApplication([])

#     progressDialog = ProgressDialog()
#     progressDialog.startTask()
#     progressDialog.exec_()

#     app.exec_()

# import time

# from PySide2.QtCore import *
# from PySide2.QtGui import *
# from PySide2.QtWidgets import *

# from ui.ui_pose2d_dialog import Ui_Ai_Dialog


# # from PySide2.QtCore import QThread, Signal, Slot



# class WorkerThread(QThread):
#     progress_signal = Signal(int)
#     finished_signal = Signal()

#     def run(self):
#         for i in range(101):
#             time.sleep(0.1)
#             self.progress_signal.emit(i)
#             if i == 100:
#                 self.finished_signal.emit()

# class Ai_Dialog(QDialog):
#     def __init__(self):
#         super().__init__()

#         self.ui_detect = Ui_Ai_Dialog()
#         self.ui_detect.setupUi(self)

#         self.setWindowTitle("Progress")
#         # self.layout = QVBoxLayout()
#         # self.setLayout(self.layout)

#         # self.progressBar = QProgressBar()
#         # self.layout.addWidget(self.progressBar)

#         # self.cancelButton = QPushButton("Cancel")
#         # self.cancelButton.clicked.connect(self.cancelTask)
#         # self.layout.addWidget(self.cancelButton)

#         self.workerThread = WorkerThread()
#         self.workerThread.progress_signal.connect(self.updateProgressBar)
#         self.workerThread.finished_signal.connect(self.taskFinished)

#     def startTask(self):
#         self.progressBar.setValue(0)
#         self.workerThread.start()

#     def updateProgressBar(self, value):
#         self.progressBar.setValue(value)

#     def taskFinished(self):
#         self.progressBar.setValue(100)
#         self.cancelButton.setEnabled(False)
#         self.cancelButton.setText("Finished")


#     def cancelTask(self):
#         self.workerThread.quit()
#         self.close()


#     if __name__ == "__main__":
#         app = QApplication([])

#         progressDialog = Ai_Dialog()
#         progressDialog.startTask()
#         progressDialog.exec_()

#         app.exec_()
#     # mySendStartEndFrameSignal = Signal(list)
#     # myAutoDetectEndFlagSignal = Signal()
#     # myModelChooseErrorSignal = Signal(int)

#     # def __init__(self, parent=None):
#     #     super().__init__(parent)

#     #     self.ui_detect = Ui_Ai_Dialog()
#     #     self.ui_detect.setupUi(self)

#     #     # self.start_end_frame_list = []
#     #     # self.auto_detect_flag = 0
#     #     # self.error_state_flag = False
#     #     # self.pv = 0

#     #     self.detect_trans = QTranslator()

#     #     self.ui_detect.btnCancel.clicked.connect(self.onCancelClicked)

#     # # def setAutoDetectArgs(self, MultiProcessAutoDetectStartEndFrame, json_data):
#     # #     self.process_manager = MultiProcessAutoDetectStartEndFrame(json_data)

#     # def startAutoDetect(self):
#     #     self.timer = QTimer(self)
#     #     self.timer.timeout.connect(self.onReceiveDataTimeout)
#     #     self.timer.start(30)

#     # @Slot()
#     # def onReceiveDataTimeout(self):
#     #     ret = self.process_manager.readMessage()
#     #     if ret['state']:
#     #         if self.pv == 100:
#     #             self.timer.stop()
#     #             self.process_manager.stopProcess()
#     #             super().accept()

#     #     elif ret['state'] == False:
#     #         self.timer.stop()
#     #         self.process_manager.stopProcess()
#     #         self.myAutoDetectEndFlagSignal.emit()
#     #         if not self.error_state_flag:
#     #             QMessageBox.warning(self, "warning",
#     #                                     "Error: MultiProcessAutoDetectStartEndFrame state is false, Start end frame detection failed")
#     #             self.error_state_flag = False
#     #         super().accept()

#     #     elif ret['log']:
#     #         self.ui_detect.txbTxtLog.append(ret['log'])

#     #     elif ret['progress_bar']:
#     #         self.pv = ret['progress_bar'] * 100
#     #         self.ui_detect.prbAutoDetect.setValue(self.pv)

#     #     elif ret['start_end_frame']:
#     #         self.start_end_frame_list.append(ret['start_end_frame'])
#     #         if len(self.start_end_frame_list) == 2:
#     #             self.mySendStartEndFrameSignal.emit(self.start_end_frame_list)

#     #     elif ret['error_state']:
#     #         self.error_state_flag = True
#     #         self.myModelChooseErrorSignal.emit(ret['error_state'])

#     # @Slot()
#     # def onCancelClicked(self):
#     #     self.close()
#     #     # self.start_end_frame_list = []
#     #     # self.auto_detect_flag = 0
#     #     # self.error_state_flag = False
#     #     # self.pv = 0

#     # def closeEvent(self, event):
#     #     self.timer.stop()
#     #     self.myAutoDetectEndFlagSignal.emit()
#     #     self.process_manager.stopProcess()

