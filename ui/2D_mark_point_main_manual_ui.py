# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file '2D_mark_point_main_manual.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QComboBox, QDial,
    QDockWidget, QGroupBox, QHBoxLayout, QLabel,
    QLineEdit, QListWidget, QListWidgetItem, QMainWindow,
    QMenu, QMenuBar, QPushButton, QRadioButton,
    QSizePolicy, QStatusBar, QVBoxLayout, QWidget)

class Ui_Form_2D_mark_point_main(object):
    def setupUi(self, Form_2D_mark_point_main):
        if not Form_2D_mark_point_main.objectName():
            Form_2D_mark_point_main.setObjectName(u"Form_2D_mark_point_main")
        Form_2D_mark_point_main.resize(1309, 956)
        self.action_set_point = QAction(Form_2D_mark_point_main)
        self.action_set_point.setObjectName(u"action_set_point")
        self.actionqq = QAction(Form_2D_mark_point_main)
        self.actionqq.setObjectName(u"actionqq")
        self.action_history_show = QAction(Form_2D_mark_point_main)
        self.action_history_show.setObjectName(u"action_history_show")
        self.action_new = QAction(Form_2D_mark_point_main)
        self.action_new.setObjectName(u"action_new")
        self.actiond_show_tools = QAction(Form_2D_mark_point_main)
        self.actiond_show_tools.setObjectName(u"actiond_show_tools")
        self.action_save = QAction(Form_2D_mark_point_main)
        self.action_save.setObjectName(u"action_save")
        self.action_help = QAction(Form_2D_mark_point_main)
        self.action_help.setObjectName(u"action_help")
        self.centralwidget = QWidget(Form_2D_mark_point_main)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.dockWidget_tools = QDockWidget(self.centralwidget)
        self.dockWidget_tools.setObjectName(u"dockWidget_tools")
        self.dockWidget_tools.setEnabled(True)
        self.dockWidgetContents_2 = QWidget()
        self.dockWidgetContents_2.setObjectName(u"dockWidgetContents_2")
        self.verticalLayout_5 = QVBoxLayout(self.dockWidgetContents_2)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.groupBox_file = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_file.setObjectName(u"groupBox_file")
        self.verticalLayout = QVBoxLayout(self.groupBox_file)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 5)
        self.lineEdit_folder = QLineEdit(self.groupBox_file)
        self.lineEdit_folder.setObjectName(u"lineEdit_folder")
        self.lineEdit_folder.setEnabled(True)
        self.lineEdit_folder.setReadOnly(False)

        self.verticalLayout.addWidget(self.lineEdit_folder)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setSpacing(0)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.pushButton_new = QPushButton(self.groupBox_file)
        self.pushButton_new.setObjectName(u"pushButton_new")

        self.horizontalLayout_8.addWidget(self.pushButton_new)

        self.checkBox_read_only = QCheckBox(self.groupBox_file)
        self.checkBox_read_only.setObjectName(u"checkBox_read_only")

        self.horizontalLayout_8.addWidget(self.checkBox_read_only)

        self.horizontalLayout_8.setStretch(0, 90)
        self.horizontalLayout_8.setStretch(1, 10)

        self.verticalLayout.addLayout(self.horizontalLayout_8)

        self.pushButton_load_point_result = QPushButton(self.groupBox_file)
        self.pushButton_load_point_result.setObjectName(u"pushButton_load_point_result")
        self.pushButton_load_point_result.setEnabled(True)

        self.verticalLayout.addWidget(self.pushButton_load_point_result)


        self.verticalLayout_5.addWidget(self.groupBox_file)

        self.groupBox_image_tools = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_image_tools.setObjectName(u"groupBox_image_tools")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_image_tools)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 5)
        self.pushButton_roate_right = QPushButton(self.groupBox_image_tools)
        self.pushButton_roate_right.setObjectName(u"pushButton_roate_right")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_roate_right.sizePolicy().hasHeightForWidth())
        self.pushButton_roate_right.setSizePolicy(sizePolicy)
        self.pushButton_roate_right.setMaximumSize(QSize(0, 0))

        self.verticalLayout_2.addWidget(self.pushButton_roate_right)

        self.pushButton_roate_left = QPushButton(self.groupBox_image_tools)
        self.pushButton_roate_left.setObjectName(u"pushButton_roate_left")
        sizePolicy.setHeightForWidth(self.pushButton_roate_left.sizePolicy().hasHeightForWidth())
        self.pushButton_roate_left.setSizePolicy(sizePolicy)
        self.pushButton_roate_left.setMaximumSize(QSize(0, 0))

        self.verticalLayout_2.addWidget(self.pushButton_roate_left)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setSpacing(0)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.label_10 = QLabel(self.groupBox_image_tools)
        self.label_10.setObjectName(u"label_10")

        self.verticalLayout_10.addWidget(self.label_10)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_angle_value = QLabel(self.groupBox_image_tools)
        self.label_angle_value.setObjectName(u"label_angle_value")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_angle_value.sizePolicy().hasHeightForWidth())
        self.label_angle_value.setSizePolicy(sizePolicy1)
        self.label_angle_value.setMaximumSize(QSize(16777215, 30))
        self.label_angle_value.setTextFormat(Qt.AutoText)
        self.label_angle_value.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_10.addWidget(self.label_angle_value)

        self.label_12 = QLabel(self.groupBox_image_tools)
        self.label_12.setObjectName(u"label_12")

        self.horizontalLayout_10.addWidget(self.label_12)


        self.verticalLayout_10.addLayout(self.horizontalLayout_10)

        self.dial_rotate = QDial(self.groupBox_image_tools)
        self.dial_rotate.setObjectName(u"dial_rotate")
        sizePolicy1.setHeightForWidth(self.dial_rotate.sizePolicy().hasHeightForWidth())
        self.dial_rotate.setSizePolicy(sizePolicy1)
        self.dial_rotate.setMaximumSize(QSize(16777215, 70))
        self.dial_rotate.setMaximum(360)
        self.dial_rotate.setSingleStep(1)
        self.dial_rotate.setPageStep(10)
        self.dial_rotate.setValue(0)
        self.dial_rotate.setOrientation(Qt.Horizontal)

        self.verticalLayout_10.addWidget(self.dial_rotate)


        self.horizontalLayout_9.addLayout(self.verticalLayout_10)

        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.label = QLabel(self.groupBox_image_tools)
        self.label.setObjectName(u"label")

        self.verticalLayout_11.addWidget(self.label)

        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setSpacing(0)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.label_zoom_value = QLabel(self.groupBox_image_tools)
        self.label_zoom_value.setObjectName(u"label_zoom_value")
        sizePolicy1.setHeightForWidth(self.label_zoom_value.sizePolicy().hasHeightForWidth())
        self.label_zoom_value.setSizePolicy(sizePolicy1)
        self.label_zoom_value.setMaximumSize(QSize(16777215, 30))
        self.label_zoom_value.setTextFormat(Qt.AutoText)
        self.label_zoom_value.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_12.addWidget(self.label_zoom_value)

        self.label_13 = QLabel(self.groupBox_image_tools)
        self.label_13.setObjectName(u"label_13")

        self.horizontalLayout_12.addWidget(self.label_13)


        self.verticalLayout_11.addLayout(self.horizontalLayout_12)

        self.dial_zoom = QDial(self.groupBox_image_tools)
        self.dial_zoom.setObjectName(u"dial_zoom")
        self.dial_zoom.setEnabled(True)
        sizePolicy1.setHeightForWidth(self.dial_zoom.sizePolicy().hasHeightForWidth())
        self.dial_zoom.setSizePolicy(sizePolicy1)
        self.dial_zoom.setMaximumSize(QSize(16777215, 70))
        self.dial_zoom.setMinimum(50)
        self.dial_zoom.setMaximum(500)
        self.dial_zoom.setSingleStep(10)
        self.dial_zoom.setValue(100)
        self.dial_zoom.setSliderPosition(100)
        self.dial_zoom.setOrientation(Qt.Horizontal)

        self.verticalLayout_11.addWidget(self.dial_zoom)


        self.horizontalLayout_9.addLayout(self.verticalLayout_11)


        self.verticalLayout_2.addLayout(self.horizontalLayout_9)

        self.pushButton_reset_view = QPushButton(self.groupBox_image_tools)
        self.pushButton_reset_view.setObjectName(u"pushButton_reset_view")

        self.verticalLayout_2.addWidget(self.pushButton_reset_view)

        self.radioButton_inherit_scale = QRadioButton(self.groupBox_image_tools)
        self.radioButton_inherit_scale.setObjectName(u"radioButton_inherit_scale")

        self.verticalLayout_2.addWidget(self.radioButton_inherit_scale)

        self.groupBox_object = QGroupBox(self.groupBox_image_tools)
        self.groupBox_object.setObjectName(u"groupBox_object")
        self.verticalLayout_6 = QVBoxLayout(self.groupBox_object)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 5)
        self.label_6 = QLabel(self.groupBox_object)
        self.label_6.setObjectName(u"label_6")

        self.verticalLayout_6.addWidget(self.label_6)

        self.comboBox_model = QComboBox(self.groupBox_object)
        self.comboBox_model.setObjectName(u"comboBox_model")

        self.verticalLayout_6.addWidget(self.comboBox_model)

        self.label_11 = QLabel(self.groupBox_object)
        self.label_11.setObjectName(u"label_11")

        self.verticalLayout_6.addWidget(self.label_11)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_9 = QLabel(self.groupBox_object)
        self.label_9.setObjectName(u"label_9")

        self.horizontalLayout_5.addWidget(self.label_9)

        self.label_8 = QLabel(self.groupBox_object)
        self.label_8.setObjectName(u"label_8")

        self.horizontalLayout_5.addWidget(self.label_8)

        self.label_7 = QLabel(self.groupBox_object)
        self.label_7.setObjectName(u"label_7")

        self.horizontalLayout_5.addWidget(self.label_7)

        self.horizontalLayout_5.setStretch(0, 35)
        self.horizontalLayout_5.setStretch(1, 60)
        self.horizontalLayout_5.setStretch(2, 5)

        self.verticalLayout_6.addLayout(self.horizontalLayout_5)

        self.pushButton_target_next = QPushButton(self.groupBox_object)
        self.pushButton_target_next.setObjectName(u"pushButton_target_next")
        sizePolicy.setHeightForWidth(self.pushButton_target_next.sizePolicy().hasHeightForWidth())
        self.pushButton_target_next.setSizePolicy(sizePolicy)
        self.pushButton_target_next.setMaximumSize(QSize(0, 0))

        self.verticalLayout_6.addWidget(self.pushButton_target_next)

        self.pushButton_target_prev = QPushButton(self.groupBox_object)
        self.pushButton_target_prev.setObjectName(u"pushButton_target_prev")
        sizePolicy.setHeightForWidth(self.pushButton_target_prev.sizePolicy().hasHeightForWidth())
        self.pushButton_target_prev.setSizePolicy(sizePolicy)
        self.pushButton_target_prev.setMaximumSize(QSize(0, 0))

        self.verticalLayout_6.addWidget(self.pushButton_target_prev)

        self.listWidget_target = QListWidget(self.groupBox_object)
        self.listWidget_target.setObjectName(u"listWidget_target")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.listWidget_target.sizePolicy().hasHeightForWidth())
        self.listWidget_target.setSizePolicy(sizePolicy2)
        self.listWidget_target.setMaximumSize(QSize(16777215, 150))

        self.verticalLayout_6.addWidget(self.listWidget_target)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.pushButton_add_target = QPushButton(self.groupBox_object)
        self.pushButton_add_target.setObjectName(u"pushButton_add_target")

        self.horizontalLayout_4.addWidget(self.pushButton_add_target)

        self.pushButton_remove_target = QPushButton(self.groupBox_object)
        self.pushButton_remove_target.setObjectName(u"pushButton_remove_target")

        self.horizontalLayout_4.addWidget(self.pushButton_remove_target)


        self.verticalLayout_6.addLayout(self.horizontalLayout_4)


        self.verticalLayout_2.addWidget(self.groupBox_object)


        self.verticalLayout_5.addWidget(self.groupBox_image_tools)

        self.groupBox_image = QGroupBox(self.dockWidgetContents_2)
        self.groupBox_image.setObjectName(u"groupBox_image")
        self.groupBox_image.setCheckable(False)
        self.verticalLayout_3 = QVBoxLayout(self.groupBox_image)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.listWidget_image = QListWidget(self.groupBox_image)
        self.listWidget_image.setObjectName(u"listWidget_image")

        self.verticalLayout_3.addWidget(self.listWidget_image)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton_image_prev = QPushButton(self.groupBox_image)
        self.pushButton_image_prev.setObjectName(u"pushButton_image_prev")

        self.horizontalLayout_2.addWidget(self.pushButton_image_prev)

        self.pushButton_image_next = QPushButton(self.groupBox_image)
        self.pushButton_image_next.setObjectName(u"pushButton_image_next")

        self.horizontalLayout_2.addWidget(self.pushButton_image_next)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)


        self.verticalLayout_5.addWidget(self.groupBox_image)

        self.pushButton_save = QPushButton(self.dockWidgetContents_2)
        self.pushButton_save.setObjectName(u"pushButton_save")

        self.verticalLayout_5.addWidget(self.pushButton_save)

        self.dockWidget_tools.setWidget(self.dockWidgetContents_2)

        self.horizontalLayout.addWidget(self.dockWidget_tools)

        self.dockWidget_show = QDockWidget(self.centralwidget)
        self.dockWidget_show.setObjectName(u"dockWidget_show")
        self.dockWidgetContents_4 = QWidget()
        self.dockWidgetContents_4.setObjectName(u"dockWidgetContents_4")
        self.verticalLayout_8 = QVBoxLayout(self.dockWidgetContents_4)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.dockWidget_show.setWidget(self.dockWidgetContents_4)

        self.horizontalLayout.addWidget(self.dockWidget_show)

        self.dockWidget_history = QDockWidget(self.centralwidget)
        self.dockWidget_history.setObjectName(u"dockWidget_history")
        self.dockWidgetContents_3 = QWidget()
        self.dockWidgetContents_3.setObjectName(u"dockWidgetContents_3")
        self.verticalLayout_7 = QVBoxLayout(self.dockWidgetContents_3)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_4 = QLabel(self.dockWidgetContents_3)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_6.addWidget(self.label_4)

        self.label_2 = QLabel(self.dockWidgetContents_3)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_6.addWidget(self.label_2)

        self.label_5 = QLabel(self.dockWidgetContents_3)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_6.addWidget(self.label_5)

        self.horizontalLayout_6.setStretch(0, 20)
        self.horizontalLayout_6.setStretch(1, 50)
        self.horizontalLayout_6.setStretch(2, 30)

        self.verticalLayout_7.addLayout(self.horizontalLayout_6)

        self.listWidget_point_hostory = QListWidget(self.dockWidgetContents_3)
        self.listWidget_point_hostory.setObjectName(u"listWidget_point_hostory")

        self.verticalLayout_7.addWidget(self.listWidget_point_hostory)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pushButton_point_next = QPushButton(self.dockWidgetContents_3)
        self.pushButton_point_next.setObjectName(u"pushButton_point_next")

        self.horizontalLayout_3.addWidget(self.pushButton_point_next)

        self.pushButton_redraw_finish = QPushButton(self.dockWidgetContents_3)
        self.pushButton_redraw_finish.setObjectName(u"pushButton_redraw_finish")

        self.horizontalLayout_3.addWidget(self.pushButton_redraw_finish)

        self.horizontalLayout_3.setStretch(0, 50)
        self.horizontalLayout_3.setStretch(1, 50)

        self.verticalLayout_7.addLayout(self.horizontalLayout_3)

        self.dockWidget_history.setWidget(self.dockWidgetContents_3)

        self.horizontalLayout.addWidget(self.dockWidget_history)

        self.horizontalLayout.setStretch(0, 12)
        self.horizontalLayout.setStretch(1, 73)
        self.horizontalLayout.setStretch(2, 15)
        Form_2D_mark_point_main.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(Form_2D_mark_point_main)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1309, 26))
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName(u"menu")
        self.menu_2 = QMenu(self.menubar)
        self.menu_2.setObjectName(u"menu_2")
        self.menu_3 = QMenu(self.menubar)
        self.menu_3.setObjectName(u"menu_3")
        Form_2D_mark_point_main.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(Form_2D_mark_point_main)
        self.statusbar.setObjectName(u"statusbar")
        self.statusbar.setStyleSheet(u"COLOR:rgb(255, 0, 4)")
        Form_2D_mark_point_main.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menu.menuAction())
        self.menubar.addAction(self.menu_3.menuAction())
        self.menubar.addAction(self.menu_2.menuAction())
        self.menu.addAction(self.action_new)
        self.menu.addAction(self.action_save)
        self.menu_2.addAction(self.action_set_point)
        self.menu_2.addSeparator()
        self.menu_2.addAction(self.action_help)
        self.menu_3.addAction(self.action_history_show)
        self.menu_3.addAction(self.actiond_show_tools)

        self.retranslateUi(Form_2D_mark_point_main)

        QMetaObject.connectSlotsByName(Form_2D_mark_point_main)
    # setupUi

    def retranslateUi(self, Form_2D_mark_point_main):
        Form_2D_mark_point_main.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"MainWindow", None))
        self.action_set_point.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6807\u8bb0\u70b9", None))
        self.actionqq.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8fde\u63a5\u7ebf", None))
        self.action_history_show.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u663e\u793a\u5386\u53f2\u8bb0\u5f55", None))
        self.action_new.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65b0\u5efa(ctrl+N)", None))
        self.actiond_show_tools.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u663e\u793a\u5de5\u5177", None))
        self.action_save.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4fdd\u5b58(ctrl+S)", None))
        self.action_help.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5e2e\u52a9", None))
        self.dockWidget_tools.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5de5\u5177", None))
        self.groupBox_file.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6587\u4ef6\uff1a", None))
        self.pushButton_new.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65b0\u5efa(ctrl+N)", None))
        self.checkBox_read_only.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u53ea\u8bfb", None))
        self.pushButton_load_point_result.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8bfb\u53d6\u8bb0\u5f55", None))
        self.groupBox_image_tools.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u56fe\u50cf\uff1a", None))
        self.pushButton_roate_right.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"+", None))
        self.pushButton_roate_left.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"-", None))
        self.label_10.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u65cb\u8f6c\uff1a", None))
        self.label_angle_value.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"0", None))
        self.label_12.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u00b0", None))
        self.label.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7f29\u653e\uff1a", None))
        self.label_zoom_value.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"100", None))
        self.label_13.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"%", None))
        self.pushButton_reset_view.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u590d\u539f(b)", None))
        self.radioButton_inherit_scale.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7ee7\u627f\u5750\u6807\u5c3a", None))
        self.groupBox_object.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u76ee\u6807:", None))
        self.label_6.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6a21\u578b\uff1a", None))
        self.label_11.setText("")
        self.label_9.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u76ee\u6807\u540d", None))
        self.label_8.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6a21\u578b", None))
        self.label_7.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5df2\u6807\u8bb0", None))
        self.pushButton_target_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"+", None))
        self.pushButton_target_prev.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"-", None))
        self.pushButton_add_target.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6dfb\u52a0(\u7a7a\u683c)", None))
        self.pushButton_remove_target.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5220\u9664(Del)", None))
        self.groupBox_image.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u56fe\u7247:", None))
        self.pushButton_image_prev.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"<(ctrl+A)", None))
        self.pushButton_image_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u">(ctrl+D)", None))
        self.pushButton_save.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4fdd\u5b58(ctrl+s)", None))
        self.dockWidget_history.setWindowTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5386\u53f2\u8bb0\u5f55", None))
        self.label_4.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"id", None))
        self.label_2.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u4f4d\u7f6e", None))
        self.label_5.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u7c7b\u578b", None))
        self.pushButton_point_next.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5feb\u901f\u91cd\u753b\uff08Tab\uff09", None))
        self.pushButton_redraw_finish.setText(QCoreApplication.translate("Form_2D_mark_point_main", u"\u5b8c\u6210(ctrl+Tab)", None))
        self.menu.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u6587\u4ef6", None))
        self.menu_2.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u8bbe\u7f6e", None))
        self.menu_3.setTitle(QCoreApplication.translate("Form_2D_mark_point_main", u"\u89c6\u56fe", None))
    # retranslateUi

