# v0.1

# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > 2022-08-01
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtGui import *
import sys
# sys.path.append('./tools')
# from feature_edit import Feature_Edit
# from x import x

class Target_Item(QWidget):

    # signal_remove_main_id = Signal(str) 
    # signal_edit_main_id=Signal(str)
    def __init__(self):
        super().__init__()
        # QThread.__init__(self)

        self.item = QListWidgetItem()
        # self.feature_edit=Feature_Edit()
        # self.signal_item_edit.connect(self.track_edit.get_data)

        self.initUI()
        self.point_dict=dict()


    
    
    def initUI(self):
        self.horizontalLayoutWidget = QWidget()

        # -----UI INSERT--------------------------------
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(70, 110, 232, 80))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.label_target_name = QLabel(self.horizontalLayoutWidget)
        self.label_target_name.setObjectName(u"label_target_name")

        self.horizontalLayout.addWidget(self.label_target_name)

        self.label_mode_type = QLabel(self.horizontalLayoutWidget)
        self.label_mode_type.setObjectName(u"label_mode_type")

        self.horizontalLayout.addWidget(self.label_mode_type)

        self.label_point_number = QLabel(self.horizontalLayoutWidget)
        self.label_point_number.setObjectName(u"label_point_number")

        self.horizontalLayout.addWidget(self.label_point_number)

        self.horizontalLayout.setStretch(0, 30)
        self.horizontalLayout.setStretch(1, 65)
        self.horizontalLayout.setStretch(2, 5)

        # -----END UI INSERT--------------------------------


        self.setLayout(self.horizontalLayout)
        self.yolo=None

    '''button'''
   

    '''set'''
    def set_target_name(self,name):
        self.label_target_name.setText(name)
    def set_target_yolo(self,yolo_list):
        # self.label_target_name.setText(self.yolo)
        self.yolo=yolo_list

    def set_target_point_group(self,df):
        self.point_dict=df
        self.label_point_number.setText(str(len(df.keys())))
    def set_target_point_type(self,mode_type):
        self.label_mode_type.setText(str(mode_type))

    def create_target_item(self,name,model,points=None,yolo=None):
        self.label_target_name.setText(name)
        self.label_mode_type.setText(model)
        if points is not None:
            self.point_dict=points
            self.label_point_number.setText(str(len(points.keys())))
            self.yolo=yolo
        


    '''get'''
    def get_target_yolo(self):
        return self.yolo
    def get_target_name(self):
        return self.label_target_name.text()
    def get_target_point_group(self):
        return self.point_dict
    def get_target_widget(self):
        return self
    def get_target_point_type(self):
        return self.label_mode_type.text()

    '''Other'''
    def high_light(self):
        self.label_target_name.setStyleSheet(u"color:red")
        self.label_point_number.setStyleSheet(u"color:red")
        self.label_mode_type.setStyleSheet(u"color:red")
    def un_high_light(self):
        self.label_target_name.setStyleSheet(u"color:black")
        self.label_point_number.setStyleSheet(u"color:black")
        self.label_mode_type.setStyleSheet(u"color:black")


    
