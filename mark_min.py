# %% [markdown]
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23



# %%

import sys



from PySide2.QtWidgets import QApplication
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *
# sys.path.append('./ui/')
from ui.ui_2D_mark_point_min import Ui_Form_2D_mark_point_min

# sys.path.append('./tools/')
from main_paint import Graphics_widget




# %% [markdown]
# Windows_main

# %%


class min_form(QWidget):
    signal_is_close=Signal(bool)
    def __init__(self,parent=None):
        super().__init__(parent)
        self.ui = Ui_Form_2D_mark_point_min()
        self.ui.setupUi(self)
        # geom = QDesktopWidget().screenGeometry(self)

        self.image_min=Graphics_widget(self)

        self.select_mod=None

        self.point_df=""
        self.model=""
        self.scale=100
        # self.create_image("D:/data/test_imgs/00042.jpg")
        

        self.ui.comboBox_object.currentIndexChanged.connect(self.choose_object)
        self.ui.pushButton_exit.clicked.connect(self.exit)


    def exit(self):
        self.signal_is_close.emit(True)

    def set_data(self,data,model):
        self.point_df=data
        self.model=model




    def create_image(self,path,model,point_df):
        self.model=model
        self.point_df= point_df
        
        if point_df is not None and point_df!={}:
            for i in self.point_df.keys():
                self.ui.comboBox_object.addItem(str(i))
            current_target=self.ui.comboBox_object.currentText()
        else:
            self.ui.comboBox_object.addItem("空")



        self.image_min.open_image(path)
        self.image_min.original_scale = 25.0
        self.image_min.load_scale(self.scale/100,False)
        if point_df is not None and point_df!={}:
            self.read_point(self.point_df[current_target])
        # self.image_min.set_model(self.model)
            self.image_min.draw_line_2(self.model["pt_line"],self.model["color"])
            self.image_min.ui.graphicsView.setEnabled(False)


        

    def read_point(self,target):
         for i in target.keys():
            point=[target[i][0], target[i][1]]
            self.image_min.load_point(i,point,target[i][2])

    def choose_object(self):
        self.image_min.clear_point()
        
        current_target=self.ui.comboBox_object.currentText()
        if current_target=="空":
            return
        self.read_point(self.point_df[current_target])
        # self.image_min.set_model(self.model)
        self.image_min.draw_line_2(self.model["pt_line"],self.model["color"])



    def resizeEvent(self, event):
        self.image_min.setGeometry(self.ui.label_image_show_min.geometry())
        
        







# %% [markdown]
# Window-end

# # %%
# def main():
#     sys.setrecursionlimit(100000)
#     app = QApplication.instance()
#     # app = QApplication([])
#     if app is None:
#         app = QApplication(sys.argv)
#         app.setStyle(QStyleFactory.create('fusion'))
#     ##step1.create ui_player

#     main_window = min_form()
#     main_window.setWindowFlags(Qt.Window)
#     # ##step2.get baisc screen and set geometry
#     screen =  QDesktopWidget()
#     geom = screen.screenGeometry(main_window)
#     w,h = int( geom.width()*0.90 ),int( geom.height()*0.70 )
#     x,y = int((geom.width()-w)/2),int((geom.height()-h)/2)-20
#     main_window.setGeometry(x,y,w,h)
#     main_window.setFixedSize(w,h)
    
#     # ##step4.show ui_player
#     main_window.show()
#     sys.exit(app.exec_())
    
# if __name__=="__main__":
#     main()
