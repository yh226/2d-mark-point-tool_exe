# %% [markdown]
# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class MyGraphicsLine_ai(QGraphicsPathItem):
    def __init__(self, scene, parent=None):
        super().__init__(parent)
        self.width = 2.0
        self.pos_src = [0, 0]
        self.pos_dst = [0, 0]

        self.pen = QPen(QColor("#000"))
        self.pen.setStyle(Qt.DashDotLine)
        self.pen.setWidthF(self.width)

        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setZValue(1)
        scene.addItem(self)

    def update_positions(self, x1, y1, x2, y2):
        self.set_src(x1, y1)
        self.set_dst(x2, y2)
        # self.update()

    def set_src(self, x, y):
        self.pos_src = [x, y]

    def set_dst(self, x, y):
        self.pos_dst = [x, y]

    def calc_path(self):
        path = QPainterPath(QPointF(self.pos_src[0], self.pos_src[1]))
        path.lineTo(self.pos_dst[0], self.pos_dst[1])
        return path

    def boundingRect(self):
        return self.shape().boundingRect()

    def shape(self):
        return self.calc_path()

    def paint(self, painter, graphics_item, widget=None):
        self.setPath(self.calc_path())
        path = self.path()

        painter.setPen(self.pen)
        painter.drawPath(path)

        self.update()



        

