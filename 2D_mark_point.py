# %% [markdown]
# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23

# %% [markdown]
# package

# %%

import re
import sys, os, time
import json
from tkinter.tix import Tree
from copy import copy
from pymouse import PyMouse
import numpy as np
import cv2
from multiprocessing import Process, Queue

from PySide2.QtWidgets import QApplication
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import js2py
import shutil 
import locale
import re

from ui.ui_2D_mark_point_main import Ui_Form_2D_mark_point_main
from tools.image_item import Image_Item
from tools.target_item import Target_Item
from tools.point_item import Point_Item
from tools.help import help_form
from tools.basic_methods import open_json_basic, save_json,open_json,PATH_CONFIGURATION,manual_position_TEMP_PATH,PATH_NEW_MANUAL_POSITION
# save_json
from mark_min import min_form
from main_paint import Graphics_widget
from setting_dialog import SettingsDialog

os.environ["MCPOSE2D_AUTH"] = r"D:\project\2d-mark-point-tool_exe\resources\mcpose2d_auth_365.bin"
from tools.pose2d_subprocess import cfg, pose2d_runner
from tools.pose2d_main import Pose2dManager

from ai_dialog import ProgressDialog
import threading
from concurrent.futures import ThreadPoolExecutor

from tools.paint import PWidget
# from tools.my_roi_rect import MyRoiRect

# %% [markdown]
# Windows_main

# %%
class main_form(QMainWindow):
    TEMP_PATH='./resources/files/temp/'
   
    # signal_is_inherit_scale = Signal(bool)
    signal_post_point_type = Signal(list)
    signal_post_point_new_location = Signal(list)
    signal_list_count= Signal(int)
    send_finished_signal = Signal(bool)
    # draw_rectangle_ai_signal= Signal()

    def __init__(self,parent=None):
        super(main_form,self).__init__(parent)
        self.ui = Ui_Form_2D_mark_point_main()
        self.ui.setupUi(self)
        # geom = QDesktopWidget().screenGeometry(self)
  
        self.setWindowTitle("2D Mark Point"+"_v"+ "3.0")

        self.fileNames=[]
        self.image_item_list=[]

        self.point_item_list=[]
        self.point_item_list_ai=[]
        self.target_manual=[]
        self.target_ai=[]
        self.compare_target_list=[-1,-1]
   

        self.increas_x=0.1
        self.increas_y=0.1
        self.old_angle=0
        self.zoom_value=[100,100]
        self.is_update_zoom=False
        
        

        self.last_folder_dir=str(self.open_json("./resources/files/recently.json","recently_path"))
        self.last_json_dir=str(self.open_json("./resources/files/recently.json","recently_json_path"))
        self.select_mod=None
        # self.image_temp_path=None
        
        self.mkdir(self.TEMP_PATH)

        self.image_main=Graphics_widget(self.ui.centralwidget)
        self.help_form=help_form()

        self.open_model()

        self.settingsDlg = SettingsDialog(self)
        self.progressDialog = ProgressDialog()
        self.progressDialog.setFixedSize(400, 100) 
        
        
        self.ui.label_ai_mod.setText(" ")
        self.ui.checkBox_read_only.setEnabled(False)
        self.ui.pushButton_ai_draw.setEnabled(False)
     
        '''signal'''
        # receive signal
        self.image_main.signal_return_new_point.connect(self.get_new_point)
        self.image_main.signal_apply_get_is_lock.connect(self.get_button_status)
        self.image_main.signal_return_zoom_value_main.connect(self.change_zoom_value)
        self.image_main.signal_return_new_yolo.connect(self.update_yolo)
        # send signal
        self.signal_post_point_type.connect(self.image_main.return_point_type)
        self.signal_list_count.connect(self.image_main.get_lock_point)
        self.send_finished_signal.connect(self.progressDialog.get_is_finished)
        # self.draw_rectangle_ai_signal.connect(self.image_main.draw_rectangle)
        
        '''menu'''
        self.ui.action_history_show.triggered.connect(self.show_history)  
        self.ui.actiond_show_tools.triggered.connect(self.show_tools)  
        self.ui.action_new.triggered.connect(self.open_folder)
        self.ui.action_save.triggered.connect(self.save_all)
        self.ui.action_help.triggered.connect(self.show_help)
        self.ui.action_set_mod.triggered.connect(self.settingsDlg.show)
        
        '''button'''
        self.ui.pushButton_ai_start.clicked.connect(self.start_ai)
        self.ui.pushButton_new.clicked.connect(self.open_folder)
        self.ui.pushButton_image_next.clicked.connect(self.image_item_next)
        self.ui.pushButton_image_prev.clicked.connect(self.image_item_prev)
        self.ui.pushButton_target_next.clicked.connect(self.taregt_item_next)
        self.ui.pushButton_target_prev.clicked.connect(self.target_item_prev)
        self.ui.pushButton_roate_right.clicked.connect(self.roate_add)
        self.ui.pushButton_roate_left.clicked.connect(self.roate_sub)
        self.ui.pushButton_add_target.clicked.connect(self.add_target_list)  
        self.ui.pushButton_remove_target.clicked.connect(self.remove_target_list)  
        self.ui.pushButton_reset_view.clicked.connect(self.view_reset)  
        self.ui.pushButton_save_all.clicked.connect(self.save_all)  
        self.ui.pushButton_save.clicked.connect(self.save_result)  
        self.ui.pushButton_load_point_result.clicked.connect(self.load_point_manual)
        self.ui.pushButton_point_next.clicked.connect(self.point_next)
        self.ui.pushButton_redraw_finish.clicked.connect(self.redraw_point_finish)
        self.ui.pushButton_ai_draw.clicked.connect(self.ai_draw)
        self.ui.pushButton_add_yolo_rectangle.clicked.connect(self.add_yolo_rectangle)
        self.ui.pushButton_delect_target_ai.clicked.connect(self.remove_target_ai)
        self.ui.pushButton_new_yolo_ai.clicked.connect(self.proccess_new_yolo_ai)
        self.ui.pushButton_compare.clicked.connect(self.compare_manual_ai)

        
        
        
        # lineEdit
        self.ui.lineEdit_folder.returnPressed.connect(self.input_path_folder)
        
        #comboBox and checkBox
        self.ui.checkBox_read_only.clicked.connect(self.is_edit)
        self.ui.comboBox_model.currentIndexChanged.connect(self.choose_model)

        '''listWidget'''
        self.ui.listWidget_image.itemClicked.connect(self.onclick_image_item)
        self.ui.listWidget_target_manual.itemClicked.connect(self.onclick_target_item)
        self.ui.listWidget_point_hostory.itemClicked.connect(self.onclick_point_item)
        self.ui.listWidget_target_ai.itemClicked.connect(self.onclick_target_item_ai)
        self.ui.listWidget_target_ai.currentItemChanged.connect(self.update_compare_target_ai)
        self.ui.listWidget_target_manual.currentItemChanged.connect(self.update_compare_target)
        self.ui.listWidget_point_hostory_ai.setEnabled(False)

        '''dial'''
        self.ui.dial_rotate.valueChanged.connect(self.dial_rotate_method)
        self.ui.dial_zoom.valueChanged.connect(self.dial_zoom_method)

        # dockWidget
        self.addDockWidget(Qt.RightDockWidgetArea, self.ui.dockWidget_history)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.ui.dockWidget_tools)
        self.ui.dockWidget_show.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.ui.dockWidget_show.setWidget(self.image_main)

        # Shortcut
        self.ui.pushButton_new.setShortcut('ctrl+n')
        self.ui.pushButton_point_next.setShortcut('tab')
        self.ui.pushButton_redraw_finish.setShortcut('ctrl+tab')
        self.ui.pushButton_add_target.setShortcut('space')
        self.ui.pushButton_remove_target.setShortcut('del')
        self.ui.pushButton_reset_view.setShortcut('b')
        self.ui.pushButton_target_prev.setShortcut('q')
        self.ui.pushButton_target_next.setShortcut('e')
        self.ui.pushButton_roate_right.setShortcut('t')
        self.ui.pushButton_roate_left.setShortcut('r')
        self.ui.pushButton_image_prev.setShortcut('ctrl+a')
        self.ui.pushButton_image_next.setShortcut('ctrl+d')
        self.ui.pushButton_save_all.setShortcut('ctrl+s')


        self.is_open_ai=True
        self.ai_thread_pool = ThreadPoolExecutor(max_workers=2)
        self.is_run_updata_position=False
        # self.position_thread_pool = ThreadPoolExecutor(max_workers=2)

        self.pose2d_manager = Pose2dManager()


  

    '''menu'''
    def show_help(self):
        self.help_form.show()
        
    def show_history(self):
        self.addDockWidget(Qt.RightDockWidgetArea, self.ui.dockWidget_history)
        self.ui.dockWidget_history.show()

    def show_tools(self):
        self.addDockWidget(Qt.LeftDockWidgetArea, self.ui.dockWidget_tools)
        self.ui.dockWidget_tools.show()

    '''rotate'''
    def dial_rotate_method(self):
        current_angle=self.ui.dial_rotate.value()
        self.ui.label_angle_value.setText(str(current_angle))
        self.image_main.view_rotate(current_angle-self.old_angle)
       
        self.old_angle=self.ui.dial_rotate.value()
    def roate_add(self):
        self.ui.dial_rotate.setValue(self.ui.dial_rotate.value()+1)
    def roate_sub(self):
        self.ui.dial_rotate.setValue(self.ui.dial_rotate.value()-1)

    def dial_zoom_method(self):
        if self.is_update_zoom==False:
            # print("is run?",self.zoom_value)
            self.zoom_value[0]=self.zoom_value[1]
            self.zoom_value[1]=self.ui.dial_zoom.value()
        
        self.ui.label_zoom_value.setText(str(self.zoom_value[1]))
        self.image_main.zoom_scene(self.zoom_value)
        self.is_update_zoom=False


    def view_reset(self):
        if self.ui.listWidget_image.count()<1:
            return

        self.image_main.view_reset()
        self.ui.label_zoom_value.setText("100")
        self.ui.dial_zoom.setValue(100)
        self.zoom_value=[100,100]
        # set_current_zoom_value
        self.is_update_zoom=False
        self.old_angle=0
        self.ui.label_angle_value.setText("0")
        self.ui.dial_rotate.setValue(0)

        if self.ui.label_zoom_value.text()=="500" or self.ui.label_zoom_value.text()=="50":
            self.image_main.view_reset()
            self.ui.label_zoom_value.setText("100")
            self.ui.dial_zoom.setValue(100)
            self.zoom_value=[100,100]
            # set_current_zoom_value
            self.is_update_zoom=False
            self.old_angle=0
            self.ui.label_angle_value.setText("0")
            self.ui.dial_rotate.setValue(0)
     
      
    def get_button_status(self,is_click):
        self.signal_list_count.emit(self.ui.listWidget_target_manual.count()) 



    
    def change_zoom_value(self,new_zoom_value):
        self.is_update_zoom=new_zoom_value[-1]
        self.zoom_value=new_zoom_value
        self.ui.dial_zoom.setValue(new_zoom_value[1])

    def is_inherit_zoom_value(self):
        change_scale=(self.ui.dial_zoom.value()-100)/10
        self.image_main.set_zoom(change_scale)


    def open_model(self):      
        self.mod= self.read_js()
        for i in self.mod:
            self.ui.comboBox_model.addItem(str(i["name"]))

        current_index=self.ui.comboBox_model.currentIndex()
        self.select_mod=self.mod[current_index]


    def choose_model(self):
        current_index=self.ui.comboBox_model.currentIndex()
        self.select_mod=self.mod[current_index]
        image_index=self.ui.listWidget_image.currentRow()    
        if image_index==-1:
            image_index=0
        if self.ui.listWidget_target_manual.count()<1:
            return
        old_target_list=copy(self.target_manual)
        self.target_manual.clear()
        self.ui.listWidget_target_manual.clear()
        
        for i in old_target_list:
            self.create_target_list(i.get_target_name(),self.ui.comboBox_model.currentText(),i.get_target_point_group())
        self.onclick_target_item()
        

       
    def find_exist_json(self,path):
        path=path.replace(".jpg",".json")
        path=path.replace(".png",".json")
        return os.path.exists(path)

    def load_point_manual(self):
        file_json = QFileDialog.getOpenFileName(self, "Open Json", self.last_json_dir, "*.JSON *.json")[0]
        if len(file_json) <= 0:
            return
        self.ui.listWidget_target_manual.clear()
        self.target_manual.clear()
        # result_id=os.path.basename(file_json).replace(".json",".jpg")
        # print(result_id)
        df=self.open_json(file_json)
 
        for i in df["point"].keys():
            self.create_target_list(i,df["model"],df["point"][i])

        
        
    def input_path_folder(self):
        self.open_folder(self.ui.lineEdit_folder.text())

    
    def sort_key(self,filename):
        locale.setlocale(locale.LC_COLLATE, 'en_US.UTF-8')
        filename = re.sub('[\u4e00-\u9fa5]', '', filename)

        number = re.findall(r'\d+', filename)
        if number:
            return int(number[0])  
        else:
            return filename
      
    def open_folder(self,folder_path=None):
        self.message_show("")
        if folder_path ==False:
            fileDialog = QFileDialog(self)
            folder=fileDialog.getExistingDirectory(self, "Open Directory",self.last_folder_dir, QFileDialog.ShowDirsOnly
                                        | QFileDialog.DontResolveSymlinks)  
        else:
            folder=folder_path

        new_fileNames=self.folder_files(folder)
        self.ui.lineEdit_folder.setText(str(folder))
        if len(new_fileNames)<=0:
            self.message_show("Error 100: 没有可用文件")
            return

        self.clean_all()
        for i in range(0,len(new_fileNames)):
            new_fileNames[i]=new_fileNames[i].replace("\\","/")
            # if new_fileNames[i] in self.fileNames:
            #     self.message_show(str(new_fileNames[i])+" has existed")
            # else:
            self.fileNames.append(new_fileNames[i]) 


        try:
        # print(self.fileNames)
            self.fileNames=sorted(self.fileNames, key=self.sort_key)
        # locale.setlocale(locale.LC_COLLATE, 'en_US.UTF-8')
        # self.fileNames= sorted(self.fileNames, key=locale.strxfrm)
        # self.fileNames=sorted(self.fileNames,key=lambda x: int(re.findall(r'\d+',x)[-1]))
        # 
        except:
            self.fileNames=sorted(self.fileNames)
            self.message_show("暂不支持中文字符排序！")

     
        self.last_folder_dir = os.path.dirname(self.fileNames[-1])
        self.last_json_dir = os.path.dirname(self.fileNames[-1])
        # self.last_folder_dir, = os.path.dirname(self.fileNames[-1])
        
        self.save_json({'recently_path':self.last_folder_dir+"/","recently_json_path":self.last_json_dir},'./resources/files/recently.json')
        
        self.create_image_list()
        self.read_images(0)
        self.image_item_list[0].high_light()
    


    def read_images(self,index):
        if len(self.image_item_list)<1:
            self.message_show("Error 100, 没有可能文件")
            return 

        if index>=len(self.image_item_list):
            return
        if self.ui.radioButton_inherit_scale.isChecked()==False:
            self.ui.label_angle_value.setText("0")
            self.ui.dial_rotate.setValue(0)

        path=self.image_item_list[index].get_image_path()
        self.ui.dockWidget_show.setWindowTitle(path)
        self.target_manual.clear()
        self.target_ai.clear()


        image_id=self.image_item_list[index].get_image_name()
        image_id=image_id.replace('.jpg','.json')
        image_id=image_id.replace('.png','.json')
        path_temp=self.TEMP_PATH+os.path.basename(image_id)
        image_id_ai=image_id.replace('.json','_ai.json')
        path_temp_ai=self.TEMP_PATH+os.path.basename(image_id_ai)

        # ai
        if self.find_exist_json(path_temp_ai):
            image_data=self.open_json(path_temp_ai)

            for i in image_data["point"].keys():
                self.create_target_list(df_name=i ,df_model=image_data["model"],df_point=image_data["point"][i],yolo=image_data["yolo"][i])
        
        # 先检查临时文件 manual 
        if self.find_exist_json(path_temp):
        # create a empty json under image file
            image_data=self.open_json(path_temp)

            for i in image_data["point"].keys():
                self.create_target_list(i,image_data["model"],image_data["point"][i])
        else:
        #load json into self.target_item_list and listWidget_target
        # 手动保存的文件
            json_file=path.replace('.jpg','.json')
            json_file=json_file.replace('.png','.json')
            if self.find_exist_json(json_file)==True:
                image_data=self.open_json(json_file)

                for i in image_data["point"].keys():
                    self.create_target_list(i,image_data["model"],image_data["point"][i])
            else:
                image_data={"image_id": image_id,"point":{},"model":self.ui.comboBox_model.currentText()}
                self.save_json(image_data,json_file)

            
        # manual
        self.image_main.open_image(path)
        self.ui.checkBox_read_only.setEnabled(True)
        if len(self.target_manual)>0:
            current_mod_name=self.target_manual[-1].get_target_point_type()
            current_mod_df=self.find_model(current_mod_name)
            self.image_main.set_model(current_mod_df)

        if self.ui.radioButton_inherit_scale.isChecked():
            self.image_main.view_rotate(int(self.ui.label_angle_value.text()))
            self.is_inherit_zoom_value()
        else:
            self.view_reset()
        
        # if self.ui.listWidget_target_manual.count() > 0:
        self.onclick_target_item()
        

    def find_model(self,current_mod):
        for i in self.mod:
            if current_mod==i["name"]:
                return i
    
    '''--------------------save part----------------'''
    def save_result(self):
        show_title = self.ui.dockWidget_show.windowTitle()
        image_id=os.path.basename(show_title)
        path=os.path.dirname(show_title)+"/"
     
        if len(self.target_manual)>0:
            temp={"image_id":image_id,"point": {}, "model": ""}
            for i in self.target_manual:

                new_point_group={i.get_target_name():i.get_target_point_group() }
                temp["point"].update(new_point_group)
                temp["model"]=(i.get_target_point_type())

            image_id_json=image_id.replace(".jpg", ".json")
            image_id_json=image_id_json.replace(".png", ".json")

            self.save_json(temp,path+image_id_json)
            self.message_show("图片 '"+ image_id +"' 结果已经保存")


    def save_all(self):
        # for i in 
        if self.ui.listWidget_image.count()<1:
            self.message_show("Error 102: 没有图片文件")
            return
        # print(self.fileNames)
        source_dir =self.TEMP_PATH
        destination_dir =os.path.dirname(self.fileNames[0])
        for root, dirs, files in os.walk(source_dir):
            for file in files:
                if file.endswith('.json'):
                    source_file = os.path.join(root, file)
                    destination_file = os.path.join(destination_dir, file)
                    shutil.copy(source_file, destination_file)
                    print(f'已复制文件: {source_file} -> {destination_file}')
        

    def save_temporary(self):  
        if len(self.target_manual)<0:
            return 
        image_id=os.path.basename(self.ui.dockWidget_show.windowTitle())
        image_id=image_id.replace(".jpg", ".json")
        image_id=image_id.replace(".png", ".json")

        temp={"image_id":image_id,"point": {}, "model": ""}

        for i in self.target_manual:
            new_point_group={i.get_target_name():i.get_target_point_group() }
            temp["point"].update(new_point_group)
            temp["model"]=(i.get_target_point_type())
            self.mkdir(self.TEMP_PATH)
            self.save_json(temp,self.TEMP_PATH+image_id)

    def save_temporary_ai(self,image_id=None):  
        if len(self.target_ai)<0:
            return 
        if image_id==None:
            image_id=os.path.basename(self.ui.dockWidget_show.windowTitle())
        image_id=image_id.replace(".jpg", "_ai.json")
        image_id=image_id.replace(".png", "_ai.json")
            
        temp={"image_id":image_id,"point": {}, "model": "","yolo":{}}

        for i in self.target_ai:
            new_point_group={i.get_target_name():i.get_target_point_group() }
            temp["point"].update(new_point_group)
            temp["model"]=(i.get_target_point_type())
            new_yolo={i.get_target_name():i.get_target_yolo() }
            temp["yolo"].update(new_yolo)
            self.mkdir(self.TEMP_PATH)
            self.save_json(temp,self.TEMP_PATH+image_id)
    

    '''--------------------save part end----------------'''

    def add_target_list(self):
        # self.ui.comboBox_model.showPopup()
        self.create_target_list()
        self.ui.listWidget_target_manual.setCurrentRow(self.ui.listWidget_target_manual.count()-1)
        self.onclick_target_item()
        
        

    def create_target_list(self,df_name=None,df_model=None,df_point=None,yolo=None,is_multi=False):
        if self.ui.listWidget_image.count()<1:
            self.message_show("Error 102: 没有图片文件")
            return
        if 'ai' in str(df_name):
            target_index=len(self.target_manual)
            if len(self.target_manual)>0:
                target_index=int(self.target_manual[-1].get_target_name()[-1])+1
        
        item_target = Target_Item()
        
        if df_name == None:
            item_target.create_target_item("target_"+str(target_index),self.ui.comboBox_model.currentText())
        else: 
            item_target.create_target_item(df_name,df_model,df_point,yolo)

        if 'ai' in str(df_name):
            if is_multi==False:
                self.ui.listWidget_target_ai.addItem(item_target.item)
                self.ui.listWidget_target_ai.setItemWidget(item_target.item, item_target.get_target_widget())   
            self.target_ai.append(item_target)
        else:
            self.ui.listWidget_target_manual.addItem(item_target.item)
            self.ui.listWidget_target_manual.setItemWidget(item_target.item, item_target.get_target_widget()) 
            self.target_manual.append(item_target)


    '''REMOVE TARGET'''
    def remove_target_list(self):
        index=self.ui.listWidget_target_manual.currentRow()
        if self.ui.listWidget_target_manual.count()<=0 or index==-1:
            return

        del self.target_manual[index]        
        self.ui.listWidget_target_manual.takeItem(index)

        self.save_temporary()

        self.image_main.clear_point()
        self.ui.listWidget_point_hostory.clear()
        self.point_item_list.clear()

        self.onclick_target_item()
        

    def create_image_list(self):
        self.ui.listWidget_image.clear()
        self.image_item_list.clear()

        for i in self.fileNames :
            item_image = Image_Item()
            self.image_item_list.append(item_image)
            item_image.set_image_name(os.path.basename(i))
            item_image.set_image_path(i)
          
            item_image.signal_main_image_path.connect(self.review_open_new)
            self.ui.listWidget_image.addItem(item_image.item)
            self.ui.listWidget_image.setItemWidget(item_image.item,  item_image.get_image_widget())   
        # self.ui.listWidget_image.sortItems(Qt.AscendingOrder)
        
    def update_compare_target(self):
        index=self.ui.listWidget_target_manual.currentRow()
        if index==-1:
            return
        # try:
        self.ui.lineEdit_current_target_manual.setText(self.target_manual[index].get_target_name())
        self.compare_target_list[0]=index
        # except:
        #     pass

    def check_is_loaded_image(self):
        if self.ui.listWidget_image.count()<1:
            self.message_show("Error 102: 没有图片文件")
            return False
        else:
            return True

    '''
    -------------------------AI DLC-------------------------
    '''
    def update_yolo(self,df_new_yolo):
        # id=df_new_yolo[1]
        # for i in range(len(self.target_ai)):
        #     if id==self.target_ai[i].get_target_name():
        index=self.ui.listWidget_target_ai.currentRow()
        if index==-1:
            return
        self.target_ai[index].set_target_yolo(df_new_yolo[0])

    def add_yolo_rectangle(self):

        new_ai_id='target_ai_'+str(len(self.target_ai))
        self.create_target_list(df_name=new_ai_id ,df_model=self.ui.label_ai_mod.text(),df_point={"0":[-1,-1]},yolo=[50,50,150,150])
        # self.ui.listWidget_target_manual.setCurrentRow(self.ui.listWidget_target_manual.count()-1)
        # self.onclick_target_item()
    
    


        # self.image_main.create_yolo_rect(new_yolo_rect,str(target_index),self.image_temp_path) 


    def remove_target_ai(self):
        index=self.ui.listWidget_target_ai.currentRow()
        if self.ui.listWidget_target_ai.count()<=0 or index==-1:
            return
        del self.target_ai[index]        
        self.ui.listWidget_target_ai.takeItem(index)

        self.save_temporary_ai()

        self.image_main.clear_point()
        self.ui.listWidget_point_hostory_ai.clear()
        self.point_item_list.clear()
        self.ui.listWidget_target_ai.setCurrentRow(0)
        self.ui.lineEdit_current_target_ai.clear()
        self.onclick_target_item_ai()
        

    def update_compare_target_ai(self):
        index=self.ui.listWidget_target_ai.currentRow()
        if index==-1 or index>len(self.target_ai):
            return
        self.ui.lineEdit_current_target_ai.setText(self.target_ai[index].get_target_name())
        self.compare_target_list[1]=index


    def ai_pose2d(self):
        if self.is_open_ai==False:
            return
        # self.pose2d_manager = Pose2dManager()
        df=open_json_basic(PATH_CONFIGURATION)

        pose2d_name=df["pose2dModelPath"]
        pose2d_name=pose2d_name.split("/")[-1]
        pose2d_name = pose2d_name.split('-')[3]
        self.ui.label_ai_mod.setText(str(pose2d_name))

        self.pose2d_manager.openProcess(pose2d_runner=pose2d_runner,meta=cfg)

        self.pose2d_manager.input_queue.put([1, './resources/test.jpg'])

        wait_ret=True
        self.ret=[2,100]
        while wait_ret:
            if not self.pose2d_manager.output_queue.empty():
                self.ret = self.pose2d_manager.output_queue.get()

            if self.ret!=[2,100]:
                wait_ret=False
            self.send_finished_signal.emit(wait_ret) 
                
        
 
    def ai_data_switch_point(self,df):
        # x2d
        array =df[1]["x2ds"][:,:,:2]
        expanded_array = np.reshape(np.concatenate([array, np.zeros((array.shape[0], array.shape[1], 1))], axis=2), (array.shape[0], array.shape[1], 3))
        expanded_array[..., 2] = np.where(expanded_array[..., 0] == 0, 2, 0)
        x2d_dict = {}
        for i, arr in enumerate(expanded_array):
            x2d_dict[f"target_ai_{i}"] = {f"{j}": list(arr[j]) for j in range(len(arr))}

        #yolo
        array =df[1]["xyxy"]
        yolo_dict={}
        for i in range(len(array)):
            yolo_dict[f"target_ai_{i}"]=array[i].tolist()

        return x2d_dict,yolo_dict
    def ai_build(self):
        self.message_show("Ai is opening...")
        if self.is_open_ai:
            self.progressDialog.startTask()
            future1 = self.ai_thread_pool.submit(self.ai_pose2d)
            future2 = self.ai_thread_pool.submit(self.progressDialog.exec_())
            # TODO：add cancel function
            self.ai_thread_pool.shutdown()
            self.is_open_ai=False
        

    def start_ai(self):
        # if self.ui.radioButton_ai.isChecked():
        self.ai_build()
        # self.ui.pushButton_ai_draw.setEnabled(True)
        self.message_show("Ai Opeend")
        self.ui.pushButton_ai_start.setText("running..")
        self.ui.pushButton_ai_start.setEnabled(False)
        self.ui.pushButton_ai_draw.setEnabled(True)
        # self.ui.radioButton_ai.setText("Disband Ai")
        # self.ui.radioButton_ai.setText("enable ai")
        # else:
        #     # self.ui.pushButton_ai_draw.setEnabled(False)
        #     self.message_show("Ai Closed")
        #     self.ui.radioButton_ai.setText("Call Ai")
        #     self.pose2d_manager.closeProcess()

    def ai_draw(self):
        if self.ui.pushButton_ai_start.text()=="启动":
            return

        if self.ui.listWidget_image.count()<1:
            self.message_show("Error 102: 没有图片文件")
            return
        self.message_show("识别中....")
        self.ui.listWidget_target_ai.clear()
        if self.ui.radioButton_full_speed.isChecked():
            
            for i in range(len(self.image_item_list)):
                path=self.image_item_list[i].get_image_path()
                image_id=self.image_item_list[i].get_image_name()
                is_save=self.run_pose2d_manager(path,image_id,True)
                
                self.message_show(image_id+" done!")
                if is_save!=False:

                    self.save_temporary_ai(image_id)
                


           
        elif self.ui.radioButton_half_speed.isChecked():
            index=self.ui.listWidget_image.currentRow()
            if index<0:
                index=0
            path=self.image_item_list[index].get_image_path()
            image_id=self.image_item_list[index].get_image_name()
            self.run_pose2d_manager(path,image_id)

        self.message_show("Complete Ai detect!")
        # self.ui.listWidget_image.setCurrentRow(0)
        self.onclick_image_item()
        self.ui.listWidget_target_ai.setCurrentRow(0)
        self.onclick_target_item_ai()

    def run_pose2d_manager(self,path,image_id,is_multi=False):
        self.pose2d_manager.input_queue.put([1, path])
        ret = self.pose2d_manager.output_queue.get()
        if ret==[0, None]:
            # self.message_show("This type image cnnot detect! "+ret)
            return False
        
        df_x2d,df_yolo=self.ai_data_switch_point(ret)
        # image_id=self.image_item_list[index].get_image_name()
        df_ai={"image_id": image_id, "point":df_x2d, "model":self.ui.label_ai_mod.text(),"yolo":df_yolo}

        for i in df_ai["point"].keys():
            self.create_target_list(df_name=i ,df_model=df_ai["model"],df_point=df_ai["point"][i],yolo=df_ai["yolo"][i],is_multi=is_multi)
        

    def proccess_new_yolo_ai(self):
        return 


    def onclick_target_item_ai(self):
        
        if len(self.target_ai)<1:
            return
        self.image_main.set_ai_model(True)

        index=self.ui.listWidget_target_ai.currentRow()
        if index==-1:
            index=0
            self.ui.listWidget_target_ai.setCurrentRow(index)

        self.unclick_target_item_manual()
        self.ui.tabWidget_history.setCurrentIndex(1)
    
        # self.image_temp_path=self.get_ai_temp_json_path()

        self.save_temporary()
        self.save_temporary_ai()
        
        self.image_main.clear_point()
        self.ui.listWidget_point_hostory_ai.clear()
        self.ui.listWidget_point_hostory.clear()
        self.point_item_list_ai.clear()
            
        for i in self.target_ai:
            i.un_high_light()

        self.target_ai[index].high_light()
        point_list=self.target_ai[index].get_target_point_group()

        self.image_main.create_yolo_rect(self.target_ai[index].get_target_yolo(),self.target_ai[index].get_target_name())
        # print(point_list)
        if point_list["0"]==[-1,-1]:
            return

        for i in point_list.keys():
            self.add_point_item_ai(i,point_list[i])
        self.current_point_item_index=0
        target_points=self.target_ai[index].get_target_point_group()
        
        # set mod
        target_mod=self.target_ai[index].get_target_point_type()
        mod={}
        for i in self.mod:
            if target_mod in i["name"]:
                mod=i
        for i in target_points.keys():
            point=[target_points[i][0], target_points[i][1]]
            self.image_main.load_point_ai(i,point,target_points[i][2])
      
        self.image_main.set_model(mod)
        self.image_main.draw_line(mod)
        

        
    def unclick_target_item_ai(self):
        if len(self.target_ai)<0:
            return
        self.ui.listWidget_target_ai.setCurrentRow(-1)
        for i in self.target_ai:
            i.un_high_light()

    def get_ai_temp_json_path(self):
        image_id=os.path.basename(self.ui.dockWidget_show.windowTitle())
        image_id=image_id.replace(".jpg", "_ai.json")
        image_id=image_id.replace(".png", "_ai.json")
        image_temp_path=self.TEMP_PATH+image_id
        return image_temp_path
    
    def clean_posint(self):
        self.image_main.clear_point()
        self.ui.listWidget_point_hostory.clear()
        self.ui.listWidget_point_hostory_ai.clear()
        self.point_item_list.clear()
        self.point_item_list_ai.clear()

    def compare_manual_ai(self):
        if self.ui.lineEdit_current_target_ai.text()=="" or \
            self.ui.lineEdit_current_target_manual.text()=="" or\
            self.ui.listWidget_target_ai.count()<1:
            self.message_show("Ai target is null or compare of ai and manual is null")
            return

        is_link=False
        manual_index=self.compare_target_list[0]
        ai_index=self.compare_target_list[1]
       
        point_list_ai=self.target_ai[ai_index].get_target_point_group()
        point_list_manual=self.target_manual[manual_index].get_target_point_group()
        if self.ui.pushButton_compare.text()=="对比":
            self.ui.pushButton_compare.setText('结束对比')
            self.is_lock_compare(False)

            is_link=True
            self.ui.tabWidget_history.setCurrentIndex(0)
            self.is_lock_compare(False)
            
        elif self.ui.pushButton_compare.text()=="结束对比":
            self.ui.pushButton_compare.setText('对比')
            self.is_lock_compare(True)

            manual_new_position=open_json(PATH_NEW_MANUAL_POSITION)
            if manual_new_position==False:
                return 
            point_list_manual.update(manual_new_position)
            self.target_manual[manual_index].set_target_point_group(point_list_manual)
            self.clean_posint()

            self.ui.listWidget_target_manual.setCurrentRow(manual_index) 
            self.onclick_target_item()

            return 


        self.clean_posint()
      
        for i in point_list_manual.keys():
            self.add_point_item(i,point_list_manual[i])
        for i in point_list_ai.keys():
            self.add_point_item_ai(i,point_list_ai[i])

        target_points_manual=self.target_manual[manual_index].get_target_point_group()
        target_points_ai=self.target_ai[ai_index].get_target_point_group()


        for i in target_points_manual.keys():
            point=[target_points_manual[i][0], target_points_manual[i][1]]
            self.image_main.load_point(i,point,target_points_manual[i][2])

        for i in target_points_ai.keys():
            point=[target_points_ai[i][0], target_points_ai[i][1]]
            self.image_main.load_point_ai(i,point,target_points_ai[i][2])
      
        save_json(target_points_ai,manual_position_TEMP_PATH)
        self.image_main.compare_link_manual_ai(is_link)

    '''
    -------------------------AI DLC end-------------------------
    '''

    def onclick_target_item(self):
        if self.ui.radioButton_inherit_scale.isChecked()==False:
            self.view_reset()
        
        if len(self.target_manual)<1:
            return
        self.image_main.set_ai_model(False)
        self.unclick_target_item_ai()
        self.ui.tabWidget_history.setCurrentIndex(0)

        index=self.ui.listWidget_target_manual.currentRow()
        if index==-1:
            index=0
            self.ui.listWidget_target_manual.setCurrentRow(index)
        if self.ui.radioButton_inherit_scale.isChecked()==False:
            self.ui.label_angle_value.setText("0")
            self.ui.dial_rotate.setValue(0)
    
        self.save_temporary()
        self.save_temporary_ai()

        self.image_main.clear_point()
        self.ui.listWidget_point_hostory.clear()
        self.ui.listWidget_point_hostory_ai.clear()
        self.point_item_list.clear()
            
        for i in self.target_manual:
            i.un_high_light()

        self.target_manual [index].high_light()
        point_list=self.target_manual[index].get_target_point_group()

        for i in point_list.keys():
            self.add_point_item(i,point_list[i])
        self.current_point_item_index=0

        # mod
        target_points=self.target_manual[index].get_target_point_group()
        target_mod=self.target_manual[index].get_target_point_type()
        
        mod={}
        for i in self.mod:
            if target_mod in i["name"]:
                mod=i
        for i in target_points.keys():
            point=[target_points[i][0], target_points[i][1]]
            self.image_main.load_point(i,point,target_points[i][2])
        
        self.image_main.set_model(mod)
        self.image_main.draw_line(mod)

        if self.ui.radioButton_inherit_scale.isChecked():
            self.image_main.view_rotate(int(self.ui.label_angle_value.text()))
            self.is_inherit_zoom_value()
        # self.is_edit()


    def unclick_target_item_manual(self):
        if len(self.target_manual)<0:
            return
        self.ui.listWidget_target_manual.setCurrentRow(-1)
        for i in self.target_manual:
            i.un_high_light()
        
       

    def onclick_point_item(self):
        index=self.ui.listWidget_point_hostory.currentRow()
        point_in_image=self.point_item_list[index].get_point_position()[:-1]

        mouse = PyMouse()
        current_location=mouse.position()
        # if point_in_image[0]<0 or point_in_image[1]<0:
        self.image_main.check_point_pos(index,point_in_image)

        canvas_location=self.image_main.get_point_location(index)
        mouse.click(int(canvas_location[0]),int(canvas_location[1]),1) 
        mouse.move(current_location[0],current_location[1])

        self.point_hight_light(index)
        self.redraw_point_finish()
        self.current_point_item_index = index
        

 

    def add_point_item(self,id,point,is_create=True):
        item_point = Point_Item()
        item_point.create_point_item(id,point)
        item_point.signal_return_point_type.connect(self.get_point_type)
        item_point.signal_return_redraw_point_id.connect(self.redraw_point)
        if is_create:
            self.ui.listWidget_point_hostory.addItem(item_point.item)
            self.point_item_list.append(item_point)
                
        elif is_create==False:
            self.ui.listWidget_point_hostory.takeItem(id)
            del self.point_item_list[id]
            self.ui.listWidget_point_hostory.insertItem(id,item_point.item)
            self.point_item_list.insert(id,item_point)

        self.ui.listWidget_point_hostory.setItemWidget(item_point.item,  item_point.get_point_widget())   

    def add_point_item_ai(self,id,point,is_create=True):
        item_point = Point_Item()
        item_point.create_point_item(id,point)
        item_point.signal_return_point_type.connect(self.get_point_type)
        item_point.signal_return_redraw_point_id.connect(self.redraw_point)
        if is_create:
            self.ui.listWidget_point_hostory_ai.addItem(item_point.item)
            self.point_item_list.append(item_point)
                
        elif is_create==False:
            self.ui.listWidget_point_hostory_ai.takeItem(id)
            del self.point_item_list[id]
            self.ui.listWidget_point_hostory_ai.insertItem(id,item_point.item)
            self.point_item_list.insert(id,item_point)

        self.ui.listWidget_point_hostory_ai.setItemWidget(item_point.item,  item_point.get_point_widget())   


    def point_next(self):
        self.current_point_item_index+=1
        if self.current_point_item_index==len(self.point_item_list):
            self.current_point_item_index=0
        # if self.is_redraw==True:
        #     self.current_point_item_index+=1
        self.redraw_point(self.current_point_item_index)
        
        # self.is_redraw=False


    def point_hight_light(self,index):
        for i in self.point_item_list:
            i.un_high_light()
        self.point_item_list[index].high_light()


    def redraw_point(self,point_id):
        if point_id>=len(self.point_item_list):
            return
        self.point_hight_light(point_id)
        self.image_main.isShowPoint(point_id)
        self.image_main.isShowLine(False)
        # if point_id+1<=len(self.point_item_list):
        #     self.current_point_item_index=point_id+1
        self.current_point_item_index=point_id
        
        # self.is_redraw=True
    def redraw_point_finish(self):
        self.image_main.show_all_point()
        self.image_main.isShowLine(True)

    def current_target_index(self):
        index_manual=self.ui.listWidget_target_manual.currentRow()
        index_ai=self.ui.listWidget_target_ai.currentRow()
        is_ai=False
        if index_ai==-1: # and index_manual!=-1:
            return index_manual,is_ai
        elif index_manual==-1:
            is_ai=True
            return index_ai,is_ai



    def get_point_type(self,point_type):
        
        index,is_ai=self.current_target_index()
        # point_type.append(is_ai)
        self.signal_post_point_type.emit(point_type) 
        # self.ui.listWidget_target_manual.currentRow()
        if is_ai:
            self.target_ai[index].get_target_point_group()[str(point_type[0])][2]=point_type[1]

        else:
            self.target_manual[index].get_target_point_group()[str(point_type[0])][2]=point_type[1]
        

    def get_new_point(self,new_point):
        index,is_ai=self.current_target_index()
        if is_ai:
            old_point_dict=self.target_ai[index].get_target_point_group()
        else:
            old_point_dict=self.target_manual[index].get_target_point_group()

        temp={str(new_point[-1]):new_point[1:-1]}
        old_point_dict.update(temp)
        if is_ai:
            self.target_ai[index].set_target_point_group(old_point_dict)
            self.add_point_item_ai(new_point[-1],new_point[1:-1],new_point[0])
        else:
            self.target_manual[index].set_target_point_group(old_point_dict)
            self.add_point_item(new_point[-1],new_point[1:-1],new_point[0])
       

    def taregt_item_next(self):
        index=self.ui.listWidget_target_manual.currentRow()
        self.ui.listWidget_target_manual.setCurrentRow(index+1)
        self.onclick_target_item()

    def target_item_prev(self):
        index=self.ui.listWidget_target_manual.currentRow()
        self.ui.listWidget_target_manual.setCurrentRow(index-1)
        self.onclick_target_item()


    def onclick_image_item(self):
        index=self.ui.listWidget_image.currentRow()
        self.jump_to_image(index)

    def image_item_next(self):
        index=self.ui.listWidget_image.currentRow()
        self.jump_to_image(index+1)
        
    def image_item_prev(self):
        index=self.ui.listWidget_image.currentRow()
        self.jump_to_image(index-1)

    def jump_to_image(self, index):
        if index>=self.ui.listWidget_image.count() or index<0:
            index=0
        self.save_temporary()
        # self.save_temporary_ai()

        self.target_manual=[]
        self.target_ai=[]

        self.ui.listWidget_target_manual.clear()
        self.ui.listWidget_target_ai.clear()

        self.ui.listWidget_point_hostory.clear()
        self.ui.listWidget_point_hostory_ai.clear()
        self.point_item_list.clear()
        self.point_item_list_ai.clear()
        self.ui.tabWidget_history.setCurrentIndex(0)

        self.ui.listWidget_image.setCurrentRow(index)
        for i in self.image_item_list:
            i.un_high_light()
        self.image_item_list[index].high_light()
        self.read_images(index)


   

    def review_open_new(self,image_path):
        model=None
        point_df=None
        
        # image_id=os.path.basename(image_path)
        json_file=image_path.replace('.jpg','.json')
        json_file=json_file.replace('.png','.json')
        if self.find_exist_json(json_file)==True:
            all_df=self.open_json(json_file)
            point_df=all_df["point"]
            model=self.find_model(all_df["model"])
        
        '''dockWidget_new'''
        self.dockWidget_new = QDockWidget(image_path, self)

        min_2d=min_form(self.dockWidget_new)
        min_2d.create_image(image_path,model,point_df)
        min_2d.signal_is_close.connect(self.close_dock_widget)

        self.dockWidget_new.setGeometry(self.ui.dockWidget_show.geometry().x(),\
            self.ui.dockWidget_show.geometry().y(),\
                min_2d.geometry().width(),min_2d.geometry().height())
        self.dockWidget_new.setFloating(True)

        self.dockWidget_new.setMaximumSize(min_2d.geometry().width(),min_2d.geometry().height())
        self.dockWidget_new.setFixedSize(min_2d.geometry().width(),min_2d.geometry().height())
        
        self.dockWidget_new.setAllowedAreas(Qt.LeftDockWidgetArea |Qt.RightDockWidgetArea)
        self.dockWidget_new.setFeatures(QDockWidget.AllDockWidgetFeatures)
        self.dockWidget_new.show()


    '''others'''
    def open_json(self,path,key=None):
        try:
            file = open(path, 'r')
            content = file.read()
            df = json.loads(content)
            df=self.convert_old_json(df)

            file.close()
            if key!=None:
                return df[key]
            else:
                return df
        except:
            self.message_show("Error 103: 找不到Json文件")
            pass

    def convert_old_json(self,df):
        if "im_name" in df.keys():
            tem={}
            tem2={}
            for i in df["person"]:
                point_id=0
                for j in i["kp"]:
                    
                    tem.update({str(point_id):[j[0],j[1],0]})
                    point_id+=1
                tem2.setdefault(str(i["id"]), {}).update(tem)
            df={"image_id": df["im_name"],"point":tem2,"model":self.ui.comboBox_model.currentText()}
            
        return df

    
    def save_json(self,df,path):
        # df={key:value}
        json_file = json.dumps(df)
        file = open(path, 'w')
        file.write(json_file)
        file.close()

    def read_js(self,path=None):
        result, tempfile = js2py.run_file("./resources/model/config.js")
        result= tempfile.config
        return result['points_lines']


    def mkdir(self,path):
        folder = os.path.exists(path)
        if not folder:                  
            os.makedirs(path)            
            

    def message_show(self,message,is_error=False):
        self.statusBar().showMessage(message)
        # self.ui.label_tips_message.setText(message)

    def folder_files(self,path):
        suffix=[".jpg",".png"]
        # suffix=[".jpg"]
        input_template_All_Path=[]
        for root, dirs, files in os.walk(path, topdown=False):
            for name in files:
                if os.path.splitext(name)[1].lower() in suffix:
                    input_template_All_Path.append(os.path.join(root, name))
            
        return input_template_All_Path
        

    def close_dock_widget(self,is_close):
        if is_close:
            self.dockWidget_new.close()
        

    def is_edit(self,is_enable=None):
        if is_enable==None:
            is_enable=self.ui.checkBox_read_only.isChecked()
        # if self.ui.checkBox_read_only.isChecked():
            # self.image_main.ui.graphicsView.setEnabled(False)
        self.image_main.set_read_only(is_enable)
        self.ui.listWidget_point_hostory.setEnabled(is_enable)
        self.ui.comboBox_model.setEnabled(is_enable)
        self.ui.pushButton_save.setEnabled(is_enable)
        self.ui.pushButton_save_all.setEnabled(is_enable)
        self.ui.action_save.setEnabled(is_enable)
        self.ui.pushButton_add_target.setEnabled(is_enable)
        self.ui.pushButton_remove_target.setEnabled(is_enable)
        self.ui.pushButton_point_next.setEnabled(is_enable)
        self.ui.pushButton_redraw_finish.setEnabled(is_enable)

    def is_lock_compare(self,is_lock=True):
        for child in self.ui.groupBox_file.children():
            child.setEnabled(is_lock)
        for child in self.ui.groupBox_ai_control.children():
            child.setEnabled(is_lock)
        for child in self.ui.groupBox_file.children():
            child.setEnabled(is_lock)
        for child in self.ui.groupBox_traget_manual.children():
            child.setEnabled(is_lock)
        for child in self.ui.groupBox_traget_ai.children():
            child.setEnabled(is_lock)
        for child in self.ui.groupBox_image.children():
            child.setEnabled(is_lock)

        self.ui.listWidget_point_hostory.setEnabled(is_lock)
        self.ui.listWidget_point_hostory_ai.setEnabled(is_lock)
        self.ui.pushButton_point_next.setEnabled(is_lock)
        self.ui.pushButton_redraw_finish.setEnabled(is_lock)   
        self.ui.pushButton_ai_start.setEnabled(False)     
        

    def clean_all(self):
        self.fileNames.clear()
        self.ui.listWidget_target_manual.clear()
        self.ui.listWidget_target_ai.clear()
        self.target_manual.clear()
        self.target_ai.clear()
        self.image_item_list.clear()
    
        if "temp" in os.listdir(self.TEMP_PATH[:-5]):
            shutil.rmtree(self.TEMP_PATH)
  
  
    def closeEvent(self, event):
        choice = QMessageBox.information(self, "EXIT", "Do you want to quit?",
                                         QMessageBox.Yes | QMessageBox.No,
                                         QMessageBox.No)
        if choice == QMessageBox.Yes:
            try:
                self.pose2d_manager.closeProcess()
                self.is_run_updata_position=False
                
                self.image_main.is_stop_read_id=False
                self.image_main.update_id_thread_pool.shutdown()
                # self.position_thread_pool.shutdown()
                # clearOutput
                shutil.rmtree(self.TEMP_PATH)
                
            except:
                pass
            event.accept()
            return super().closeEvent(event)
        else:
            event.ignore()


def main():
    sys.setrecursionlimit(100000)
    app = QApplication.instance()
    # app = QApplication([])
    if app is None:
        app = QApplication(sys.argv)
        app.setStyle(QStyleFactory.create('fusion'))
    ##step1.create ui_player

    main_window = main_form()
    main_window.setWindowFlags(Qt.Window)
    # ##step2.get baisc screen and set geometry
    screen =  QDesktopWidget()
    geom = screen.screenGeometry(main_window)
    w,h = int( geom.width()*0.80 ),int( geom.height()*0.80 )
    x,y = int((geom.width()-w)/2),int((geom.height()-h)/2)-20
    main_window.setGeometry(x,y,w,h)
    # main_window.setFixedSize(w,h)

    # main_window.showFullScreen()
    
    # ##step4.show ui_player
    main_window.show()
    sys.exit(app.exec_())
    
if __name__=="__main__":
    main()
