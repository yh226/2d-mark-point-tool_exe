# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from ui.ui_settings_dialog import Ui_settingsDialog


class set_mod_form(QWidget):

    def __init__(self,parent=None):
        super().__init__(parent)
        self.ui = Ui_settingsDialog()
        self.ui.setupUi(self)


        geom = QDesktopWidget().screenGeometry(self)
        w,h = int( geom.width()*0.5 ),int( geom.height()*0.7 )
        x,y = int((geom.width()-w)/2),int((geom.height()-h)/2)-20
        self.setGeometry(x,y,w,h)
        self.setFixedSize(w,h)
    # main_window.setFixedSize(w,h)