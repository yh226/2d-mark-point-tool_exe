# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from tools.basic_methods import save_json,open_json


class MyRoiRect(QGraphicsEllipseItem):
    """
    自定义矩形
    """

    def __init__(self, scene, l_pos, r_pos, _id,path=None,parent=None):
        super().__init__(parent)
        self.setZValue(3)
        scene.addItem(self)
        self.scene = scene

        self.x1, self.y1 = l_pos
        self.x2, self.y2 = r_pos
        self.id=str(_id)
        self.path=path
    

        self.setPos(l_pos[0], l_pos[1])
        self.setRect(0, 0, 1920, 1080)

        pos_list = [l_pos, r_pos]

        self.obj_dict = {}
        for index, pos in enumerate(pos_list):
            # self.obj_dict[index] = MyCustomPonint(scene,self.path,self.id,index)
            self.obj_dict[index] = MyCustomPonint(self.scene,self.id)
            self.obj_dict[index].setPos(pos[0], pos[1])


    def clear_rect(self):
        self.scene.removeItem(self)
        if len(self.obj_dict) > 0:
            for point_obj in self.obj_dict.values():
                self.scene.removeItem(point_obj)

    def get_yolo_id(self):
        return self.id


    def get_roi_pos(self):
        if self.x1 < self.x2 and self.y1 < self.y2:
            return [self.x1, self.y1, self.x2, self.y2]
        else:
            return [self.x2, self.y2, self.x1, self.y1]

    def getObjLRPos(self):
         if len(self.obj_dict) > 0:
            obj_l = self.obj_dict[0].pos()
            obj_r = self.obj_dict[1].pos()
            self.x1, self.y1 = obj_l.x(), obj_l.y()
            self.x2, self.y2 = obj_r.x(), obj_r.y()


    def paint(self, painter, option, widget=None):
        self.getObjLRPos()

        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(QPen(Qt.green, 1))
        painter.drawRect(0, 0, self.x2 - self.x1, self.y2 - self.y1)
        self.setPos(self.x1, self.y1)
        
        
        painter.setPen(QPen(Qt.red, 1))
        painter.setFont(QFont("Arial", 5))
        painter.drawText(5, -5,self.id)
 


class MyCustomPonint(QGraphicsEllipseItem):
    """
    自定义十字坐标点
    """
    # Yolo_TEMP_PATH='./resources/files/temp/'
# mouseReleased = Signal(int)
    def __init__(self, scene,_id,cross_id=None,parent=None):
        super().__init__(parent)
        

        self.setZValue(3)
        scene.addItem(self)
        self.scene=scene
        
        self.setRect(-10, -10, 30, 30)

        self.setFlag(QGraphicsItem.ItemIsFocusable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setCursor(Qt.SizeAllCursor)
        self.id=_id
        # self.path=path
        # self.cross_id=cross_id

    def add_cross(self):
        self.scene.addItem(self)


    def paint(self, painter, option, widget=None):
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(QPen(Qt.red, 1))
        painter.drawLine(-5, 0, 5, 0)
        painter.drawLine(0, -5, 0, 5)
        # painter.

    # def mouseReleaseEvent(self, event):
    #     if event.button() == Qt.LeftButton:
    #         df=open_json(self.path)
    #         print(df,self.id)
    #         if self.cross_id==0:
    #             df['yolo'][self.id][0]=self.pos().x()
    #             df['yolo'][self.id][1]=self.pos().y()
    #         elif self.cross_id==1:
    #             df['yolo'][self.id][2]=self.pos().x()
    #             df['yolo'][self.id][3]=self.pos().y()
    #         save_json(df,self.path)

    #     super().mouseReleaseEvent(event)

