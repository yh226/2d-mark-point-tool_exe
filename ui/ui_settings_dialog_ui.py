# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_settings_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.3.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QDoubleSpinBox, QGridLayout,
    QHBoxLayout, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QSpacerItem, QSpinBox, QTabWidget,
    QVBoxLayout, QWidget)

class Ui_settingsDialog(object):
    def setupUi(self, settingsDialog):
        if not settingsDialog.objectName():
            settingsDialog.setObjectName(u"settingsDialog")
        settingsDialog.resize(914, 294)
        self.verticalLayout_5 = QVBoxLayout(settingsDialog)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(2, 2, 2, 2)
        self.tabWidget = QTabWidget(settingsDialog)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout = QVBoxLayout(self.tab)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.label_10 = QLabel(self.tab)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_9.addWidget(self.label_10)

        self.lblYoloModel = QLineEdit(self.tab)
        self.lblYoloModel.setObjectName(u"lblYoloModel")
        self.lblYoloModel.setReadOnly(True)

        self.horizontalLayout_9.addWidget(self.lblYoloModel)

        self.btnYoloOpen = QPushButton(self.tab)
        self.btnYoloOpen.setObjectName(u"btnYoloOpen")

        self.horizontalLayout_9.addWidget(self.btnYoloOpen)


        self.verticalLayout.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_13 = QLabel(self.tab)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_10.addWidget(self.label_13)

        self.spbYoloIOU = QDoubleSpinBox(self.tab)
        self.spbYoloIOU.setObjectName(u"spbYoloIOU")
        self.spbYoloIOU.setMinimum(0.100000000000000)
        self.spbYoloIOU.setMaximum(1.000000000000000)
        self.spbYoloIOU.setSingleStep(0.005000000000000)
        self.spbYoloIOU.setValue(0.500000000000000)

        self.horizontalLayout_10.addWidget(self.spbYoloIOU)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer_7)


        self.verticalLayout.addLayout(self.horizontalLayout_10)

        self.horizontalLayout_18 = QHBoxLayout()
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.label_12 = QLabel(self.tab)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_18.addWidget(self.label_12)

        self.spbYoloConf = QDoubleSpinBox(self.tab)
        self.spbYoloConf.setObjectName(u"spbYoloConf")
        self.spbYoloConf.setMinimum(0.100000000000000)
        self.spbYoloConf.setMaximum(1.000000000000000)
        self.spbYoloConf.setSingleStep(0.005000000000000)
        self.spbYoloConf.setValue(0.500000000000000)

        self.horizontalLayout_18.addWidget(self.spbYoloConf)

        self.horizontalSpacer_15 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_18.addItem(self.horizontalSpacer_15)


        self.verticalLayout.addLayout(self.horizontalLayout_18)

        self.verticalSpacer = QSpacerItem(20, 27, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_2 = QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_7 = QLabel(self.tab_2)
        self.label_7.setObjectName(u"label_7")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_11.addWidget(self.label_7)

        self.spbNumMaxPeople = QSpinBox(self.tab_2)
        self.spbNumMaxPeople.setObjectName(u"spbNumMaxPeople")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.spbNumMaxPeople.sizePolicy().hasHeightForWidth())
        self.spbNumMaxPeople.setSizePolicy(sizePolicy1)
        self.spbNumMaxPeople.setMinimumSize(QSize(48, 0))
        self.spbNumMaxPeople.setMinimum(1)
        self.spbNumMaxPeople.setMaximum(5)
        self.spbNumMaxPeople.setValue(1)

        self.horizontalLayout_11.addWidget(self.spbNumMaxPeople)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_11.addItem(self.horizontalSpacer_6)


        self.gridLayout_2.addLayout(self.horizontalLayout_11, 1, 0, 1, 1)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label = QLabel(self.tab_2)
        self.label.setObjectName(u"label")
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(100, 0))

        self.horizontalLayout_6.addWidget(self.label)

        self.lblModelKP = QLineEdit(self.tab_2)
        self.lblModelKP.setObjectName(u"lblModelKP")
        self.lblModelKP.setReadOnly(True)

        self.horizontalLayout_6.addWidget(self.lblModelKP)

        self.btnOpenKP = QPushButton(self.tab_2)
        self.btnOpenKP.setObjectName(u"btnOpenKP")

        self.horizontalLayout_6.addWidget(self.btnOpenKP)


        self.gridLayout_2.addLayout(self.horizontalLayout_6, 0, 0, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_2)

        self.verticalSpacer_2 = QSpacerItem(20, 27, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_2)

        self.tabWidget.addTab(self.tab_2, "")

        self.verticalLayout_5.addWidget(self.tabWidget)

        self.lblWarning = QLabel(settingsDialog)
        self.lblWarning.setObjectName(u"lblWarning")
        self.lblWarning.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.lblWarning)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_save = QPushButton(settingsDialog)
        self.pushButton_save.setObjectName(u"pushButton_save")

        self.horizontalLayout.addWidget(self.pushButton_save)

        self.pushButton_cancel = QPushButton(settingsDialog)
        self.pushButton_cancel.setObjectName(u"pushButton_cancel")

        self.horizontalLayout.addWidget(self.pushButton_cancel)


        self.verticalLayout_5.addLayout(self.horizontalLayout)


        self.retranslateUi(settingsDialog)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(settingsDialog)
    # setupUi

    def retranslateUi(self, settingsDialog):
        settingsDialog.setWindowTitle(QCoreApplication.translate("settingsDialog", u"settingsDialog", None))
        self.label_10.setText(QCoreApplication.translate("settingsDialog", u"model:", None))
        self.lblYoloModel.setText(QCoreApplication.translate("settingsDialog", u"E:/", None))
        self.btnYoloOpen.setText(QCoreApplication.translate("settingsDialog", u"open", None))
        self.label_13.setText(QCoreApplication.translate("settingsDialog", u"IoU threshold:", None))
        self.label_12.setText(QCoreApplication.translate("settingsDialog", u"confidence:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("settingsDialog", u"  Yolo ", None))
        self.label_7.setText(QCoreApplication.translate("settingsDialog", u"max num people:", None))
        self.label.setText(QCoreApplication.translate("settingsDialog", u"model:", None))
        self.lblModelKP.setText(QCoreApplication.translate("settingsDialog", u"E:/", None))
        self.btnOpenKP.setText(QCoreApplication.translate("settingsDialog", u"Open", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("settingsDialog", u"Pose2D", None))
        self.lblWarning.setText(QCoreApplication.translate("settingsDialog", u"TextLabel", None))
        self.pushButton_save.setText(QCoreApplication.translate("settingsDialog", u"Ok", None))
        self.pushButton_cancel.setText(QCoreApplication.translate("settingsDialog", u"Cancel", None))
    # retranslateUi

