# %% [markdown]
# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class MyGraphicsItem(QGraphicsItem):
    # FONT_SIZE=6
    def __init__(self, x, y, width, scene, num, spacing, font_size, _point_type=0,is_ai=False,parent=None):
    # def __init__(self, x, y, width, height, scene, num, spacing, font_size, _point_type=0,parent=None):
        super().__init__(parent)

        self.num = num
        self.spacing = spacing
        self.width = width
        self.font_size = font_size
        self.point_type = _point_type
        transparency=180
        self.is_ai=is_ai
        if self.is_ai:
            self.width=self.width+1
            # transparency=200


        if self.point_type==0:  
            # yellow
            color=QColor (255,255,0,transparency)
        elif self.point_type==1:
            # skyblue
            color=QColor(255,99,71,transparency)
        elif self.point_type==2:  
            # blueviolet
            color=QColor(138,43,226,transparency)

        brush=QBrush(color)

    

        # point
        self.item = My_QGraphicsEllipseItem(0, 0, self.width, self.width)
        if self.is_ai==False:
            self.item.setBrush(brush)  
            # self.item.setPen(color)
        # else:
            # 1self.item.setBrush(brush)  
        self.item.setPen(color)
        self.item.setZValue(2)
 

        # point id
        self.item.setData(self.num, self.num)  
        scene.addItem(self.item)
        self.item_x, self.item_y = self.item.boundingRect().x(), self.item.boundingRect().y()


       
        self.text = QGraphicsSimpleTextItem(str(self.num))
        font = self.text.font()
        font.setPointSize(self.font_size)
        if is_ai:
            self.text.setBrush(QColor(255,0,0))  
        font.setBold(True)
        self.text.setFont(font)
        self.text.setZValue(2)
        self.text.setPos(self.item_x + self.spacing, self.item_y)
        scene.addItem(self.text)



        self.group = QGraphicsItemGroup()
        scene.addItem(self.group)
        self.group.addToGroup(self.text)
        self.group.addToGroup(self.item)
        self.group.setData(2, self.num)
        self.group.setPos(x-(self.width/2), y-(self.width/2))
        self.group.setZValue(2)
        self.group.setFlag(QGraphicsItem.ItemIsFocusable)
        self.group.setFlag(QGraphicsItem.ItemIsMovable)
        self.group.setFlag(QGraphicsItem.ItemIsSelectable)
        scene.clearSelection()
        self.group.setSelected(True)

        self.setFlag(QGraphicsItem.ItemIsFocusable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setCursor(Qt.SizeAllCursor)
        


    # def get_clicked_item(self):
    #     self.scene. . group.focusItem()
    #     #  focusItem()
    def get_is_ai(self):
        return self.is_ai
    def get_point_type(self):
        return self.point_type

    def set_point_type(self,_point_type):
        
        self.point_type=_point_type

    def set_point_psoition(self, x,y):
        self.group.setPos(x, y)

    def set_mouse_left(self,_is_read):
        # self.group.setFlag(QGraphicsItem.ItemIsFocusable,_is_read)
        self.group.setFlag(QGraphicsItem.ItemIsMovable,_is_read)
        self.group.setFlag(QGraphicsItem.ItemIsSelectable,_is_read)


    # def mousePressEvent(self, event):
    #     if event.button() == Qt.LeftButton:
    #         if event.modifiers() == Qt.ShiftModifier:
    #             # self.isCanMove = True
    #             # self.blobColor = Qt.red
    #             print("111")
    #             self.update()
    #         else:
    #             super(MyGraphicsItem, self).mousePressEvent(event)
    #             event.accept()
    #     # else:
    #     #     event.ignore()


    # def paint(self, painter, option, widget=None):
    #     painter.setRenderHint(QPainter.Antialiasing)
    #     painter.setPen(QPen(Qt.red, 1))
    #     painter.drawLine(-5, 0, 5, 0)
    #     painter.drawLine(0, -5, 0, 5)
    #     # painter.

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            print("111")
            # df=open_json(self.path)
            # # print(df,self.id)
            # if self.cross_id==0:
            #     df['yolo'][self.id][0]=self.pos().x()
            #     df['yolo'][self.id][1]=self.pos().y()
            # elif self.cross_id==1:
            #     df['yolo'][self.id][2]=self.pos().x()
            #     df['yolo'][self.id][3]=self.pos().y()
            # save_json(df,self.path)

        super().mouseReleaseEvent(event)
    # def boundingRect(self):
    #     # penWidth = 1
    #     return QRectF(100, 100,200,200)
    
    # def mouseReleaseEvent(self, event):
    #     self.setCursor(Qt.OpenHandCursor)

# QGraphicsEllipseItem
# class My_QGraphicsEllipseItem(QGraphicsEllipseItem):
#     """
#     自定义十字坐标点
#     """
#     # Yolo_TEMP_PATH='./resources/files/temp/'
# # mouseReleased = Signal(int)
#     def __init__(self, 0, 0, .width, self.width,parent=None):
#         super().__init__(parent)
        

#         # self.setZValue(3)
#         # scene.addItem(self)
#         # self.setRect(-10, -10, 30, 30)

#         # self.setFlag(QGraphicsItem.ItemIsFocusable)
#         # self.setFlag(QGraphicsItem.ItemIsMovable)
#         # self.setFlag(QGraphicsItem.ItemIsSelectable)
#         # self.setCursor(Qt.SizeAllCursor)
#         # self.id=_id
#         # self.path=path
#         # self.cross_id=cross_id

#     # def paint(self, painter, option, widget=None):
#     #     painter.setRenderHint(QPainter.Antialiasing)
#     #     painter.setPen(QPen(Qt.red, 1))
#     #     painter.drawLine(-5, 0, 5, 0)
#     #     painter.drawLine(0, -5, 0, 5)
#     #     # painter.

#     def mouseReleaseEvent(self, event):
#         if event.button() == Qt.LeftButton:
#             print("111")
#             # df=open_json(self.path)
#             # # print(df,self.id)
#             # if self.cross_id==0:
#             #     df['yolo'][self.id][0]=self.pos().x()
#             #     df['yolo'][self.id][1]=self.pos().y()
#             # elif self.cross_id==1:
#             #     df['yolo'][self.id][2]=self.pos().x()
#             #     df['yolo'][self.id][3]=self.pos().y()
#             # save_json(df,self.path)

#         super().mouseReleaseEvent(event)
