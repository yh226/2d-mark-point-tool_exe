# v1.4.4c
# 
# 
# > Copyright (C) 2021 MakerCollider
# >
# > The binary release will be delivered to the customer with the permission
# >
# > The source code cannot be distributed without the permission of MakerCollider(Beijing Xuanmi)
# >
# > joint DOF limit of numpy implementation
# >
# > Code Developed by:
# >
# > Yandong.Han yh226@hotmail.com
# >
# > create date: 2022-08-23
from signal import signal
import sys
import math
# import global_var as var
import numpy as np
# from cv_camera import CVCamera

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2.QtCore import Qt

from tools.ui_paint import Ui_Form
# from pynput.mouse import Listener
from time import sleep
import time



class PWidget(QWidget):
    signal_change = Signal()
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.scale = 0
        # var.global_scale
        self.w_frame_rate =2
        # var.w_frame_rate
        self.h_frame_rate = 2
        # var.h_frame_rate
        self._number_point=4
        self.point_list_x= []
        self.point_list_y= []
        


    def create_point(self,_number_point=4,_2d_list=[]):
        self.number_point = _number_point
        self.init_center_pos(_2d_list)
        
        self.init_w_h(self.x1, self.y1, self.x2, self.y2, self.x4, self.y4,self.ui.width_x,self.ui.width_y)
        
        
        
        self.ui.mark_cursor_1.myMouseDrag.connect(self.onMyMouseDrag)
        self.ui.mark_cursor_2.myMouseDrag.connect(self.onMyMouseDrag)
        self.ui.mark_cursor_3.myMouseDrag.connect(self.onMyMouseDrag)
        self.ui.mark_cursor_4.myMouseDrag.connect(self.onMyMouseDrag)

        self.ui.width_x.setEnabled(False)
        self.ui.width_y.setEnabled(False)
        self.ui.width_x.setVisible(False)
        self.ui.width_y.setVisible(False)


        self.ui.mark_cursor_5.setEnabled(False)
        self.ui.mark_cursor_6.setEnabled(False)
        self.ui.high_5_6.setEnabled(False)
        self.ui.width_5_6.setEnabled(False)

        self.ui.mark_cursor_5.setVisible(False)
        self.ui.mark_cursor_6.setVisible(False)
        self.ui.high_5_6.setVisible(False)
        self.ui.width_5_6.setVisible(False)

        self.ui.label_5_inf.setVisible(False)
        self.ui.label_6_inf.setVisible(False)
        self.ui.label_5_inf.setEnabled(False)
        self.ui.label_6_inf.setEnabled(False)



        

        if self.number_point == 6:
            self.ui.mark_cursor_5.setEnabled(True)
            self.ui.mark_cursor_6.setEnabled(True)
            self.ui.high_5_6.setEnabled(True)
            self.ui.width_5_6.setEnabled(True)

            self.ui.mark_cursor_5.setVisible(True)
            self.ui.mark_cursor_6.setVisible(True)
            # self.ui.high_5_6.setVisible(True)
            # self.ui.width_5_6.setVisible(True)

            self.ui.label_5_inf.setVisible(True)
            self.ui.label_6_inf.setVisible(True)
            self.ui.label_5_inf.setEnabled(True)
            self.ui.label_6_inf.setEnabled(True)


            self.ui.mark_cursor_5.myMouseDrag.connect(self.onMyMouseDrag)
            self.ui.mark_cursor_6.myMouseDrag.connect(self.onMyMouseDrag)

            self.init_w_h(self.x2, self.y2, self.x5, self.y5, self.x3, self.y3,self.ui.width_5_6,self.ui.high_5_6)


    @Slot()
    def onMyMouseDrag(self):
        x1, y1 = self.ui.mark_cursor_1.center_x, self.ui.mark_cursor_1.center_y
        x2, y2 = self.ui.mark_cursor_2.center_x, self.ui.mark_cursor_2.center_y
        x4, y4 = self.ui.mark_cursor_4.center_x, self.ui.mark_cursor_4.center_y

        self.init_w_h(x1, y1, x2, y2, x4, y4,self.ui.width_x,self.ui.width_y)

        if self.number_point==6:
            x1, y1 = self.ui.mark_cursor_2.center_x, self.ui.mark_cursor_2.center_y
            x2, y2 = self.ui.mark_cursor_5.center_x, self.ui.mark_cursor_5.center_y
            x4, y4 = self.ui.mark_cursor_3.center_x, self.ui.mark_cursor_3.center_y

            self.init_w_h(x1, y1, x2, y2, x4, y4,self.ui.width_5_6,self.ui.high_5_6)

        self.get_point_position()

       
    def init_center_pos(self,_2d_list=[]):
        self.font = QFont()
        self.font.setPointSize(60)

       

        cursor_1_x = self.ui.mark_cursor_1.geometry().x()*self.w_frame_rate
        cursor_1_y = self.ui.mark_cursor_1.geometry().y()*self.h_frame_rate
        cursor_1_w = self.ui.mark_cursor_1.geometry().width()*self.w_frame_rate
        cursor_1_h = self.ui.mark_cursor_1.geometry().height()*self.h_frame_rate
        # print(cursor_1_w,cursor_1_h)
        self.ui.mark_cursor_1.setGeometry(cursor_1_x, cursor_1_y, cursor_1_w, cursor_1_h)
        self.ui.mark_cursor_1.setFont(self.font)

        cursor_2_x = self.ui.mark_cursor_2.geometry().x()*self.w_frame_rate
        cursor_2_y = self.ui.mark_cursor_2.geometry().y()*self.h_frame_rate
        cursor_2_w = self.ui.mark_cursor_2.geometry().width()*self.w_frame_rate
        cursor_2_h = self.ui.mark_cursor_2.geometry().height()*self.h_frame_rate
        self.ui.mark_cursor_2.setGeometry(cursor_2_x, cursor_2_y, cursor_2_w, cursor_2_h)
        self.ui.mark_cursor_2.setFont(self.font)

        cursor_3_x = self.ui.mark_cursor_3.geometry().x()*self.w_frame_rate
        cursor_3_y = self.ui.mark_cursor_3.geometry().y()*self.h_frame_rate
        cursor_3_w = self.ui.mark_cursor_3.geometry().width()*self.w_frame_rate
        cursor_3_h = self.ui.mark_cursor_3.geometry().height()*self.h_frame_rate
        self.ui.mark_cursor_3.setGeometry(cursor_3_x, cursor_3_y, cursor_3_w, cursor_3_h)
        self.ui.mark_cursor_3.setFont(self.font)

        cursor_4_x = self.ui.mark_cursor_4.geometry().x()*self.w_frame_rate
        cursor_4_y = self.ui.mark_cursor_4.geometry().y()*self.h_frame_rate
        cursor_4_w = self.ui.mark_cursor_4.geometry().width()*self.w_frame_rate
        cursor_4_h = self.ui.mark_cursor_4.geometry().height()*self.h_frame_rate
        self.ui.mark_cursor_4.setGeometry(cursor_4_x, cursor_4_y, cursor_4_w, cursor_4_h)
        self.ui.mark_cursor_4.setFont(self.font)


      


        width_x_1 = self.ui.width_x.geometry().width()*self.w_frame_rate
        width_x_2 = self.ui.width_y.geometry().width()*self.h_frame_rate
        self.ui.width_x.resize(width_x_1,30)
        self.ui.width_y.resize(width_x_2,30)

        width_inf_1=self.ui.label_1_inf.geometry().width()*self.w_frame_rate
        width_inf_2=self.ui.label_2_inf.geometry().width()*self.w_frame_rate
        width_inf_3=self.ui.label_3_inf.geometry().width()*self.w_frame_rate
        width_inf_4=self.ui.label_4_inf.geometry().width()*self.w_frame_rate

        self.ui.label_1_inf.resize(width_inf_1,30)
        self.ui.label_2_inf.resize(width_inf_2,30)
        self.ui.label_3_inf.resize(width_inf_3,30)
        self.ui.label_4_inf.resize(width_inf_4,30)



        width_x1, height_y1 = cursor_1_w / 2, cursor_1_h / 2
        width_x2, height_y2 = cursor_2_w / 2, cursor_2_h / 2
        width_x3, height_y3 = cursor_3_w / 2, cursor_3_h / 2
        width_x4, height_y4 = cursor_4_w / 2, cursor_4_h / 2
        self.x1, self.y1 = cursor_1_x + width_x1, cursor_1_y + height_y1
        self.x2, self.y2 = cursor_2_x + width_x2, cursor_2_y + height_y2
        self.x3, self.y3 = cursor_3_x + width_x3, cursor_3_y + height_y3
        self.x4, self.y4 = cursor_4_x + width_x4, cursor_4_y + height_y4


        if _2d_list !=[]:
        # self.set_cursor_position(_2d_list)


            self.ui.mark_cursor_1.setGeometry(_2d_list[0][0], _2d_list[0][1], cursor_1_w, cursor_1_h)
            self.ui.mark_cursor_2.setGeometry(_2d_list[1][0], _2d_list[1][1], cursor_2_w, cursor_2_h)
            self.ui.mark_cursor_3.setGeometry(_2d_list[2][0], _2d_list[2][1], cursor_3_w, cursor_3_h)
            self.ui.mark_cursor_4.setGeometry(_2d_list[3][0], _2d_list[3][1], cursor_4_w, cursor_4_h)
            self.x1,self.y1=_2d_list[0][0]+ width_x1, _2d_list[0][1]+height_y1
            self.x2,self.y2=_2d_list[1][0]+ width_x2, _2d_list[1][1]+height_y2
            self.x3,self.y3=_2d_list[2][0]+ width_x3, _2d_list[2][1]+height_y3
            self.x4,self.y4=_2d_list[3][0]+ width_x4, _2d_list[3][1]+height_y4
       

            


            if self.number_point ==6:
                self.ui.mark_cursor_5.setGeometry(_2d_list[4][0], _2d_list[4][1], cursor_5_w, cursor_5_h)
                self.ui.mark_cursor_6.setGeometry(_2d_list[5][0], _2d_list[5][1], cursor_6_w, cursor_6_h)
                self.ui.mark_cursor_5.center_x, self.ui.mark_cursor_5.center_y = _2d_list[4][0], _2d_list[4][1]
                self.ui.mark_cursor_6.center_x, self.ui.mark_cursor_6.center_y = _2d_list[5][0], _2d_list[5][1]



        self.ui.mark_cursor_1.center_x, self.ui.mark_cursor_1.center_y = self.x1, self.y1
        self.ui.mark_cursor_2.center_x, self.ui.mark_cursor_2.center_y = self.x2, self.y2
        self.ui.mark_cursor_3.center_x, self.ui.mark_cursor_3.center_y = self.x3, self.y3
        self.ui.mark_cursor_4.center_x, self.ui.mark_cursor_4.center_y = self.x4, self.y4

       






        if self.number_point ==6: 
            cursor_5_x = self.ui.mark_cursor_5.geometry().x()*self.w_frame_rate
            cursor_5_y = self.ui.mark_cursor_5.geometry().y()*self.h_frame_rate
            cursor_5_w = self.ui.mark_cursor_5.geometry().width()*self.w_frame_rate
            cursor_5_h = self.ui.mark_cursor_5.geometry().height()*self.h_frame_rate
            self.ui.mark_cursor_5.setGeometry(cursor_5_x, cursor_5_y, cursor_5_w, cursor_5_h)
            self.ui.mark_cursor_5.setFont(self.font)

            cursor_6_x = self.ui.mark_cursor_6.geometry().x()*self.w_frame_rate
            cursor_6_y = self.ui.mark_cursor_6.geometry().y()*self.h_frame_rate
            cursor_6_w = self.ui.mark_cursor_6.geometry().width()*self.w_frame_rate
            cursor_6_h = self.ui.mark_cursor_6.geometry().height()*self.h_frame_rate
            self.ui.mark_cursor_6.setGeometry(cursor_6_x, cursor_6_y, cursor_6_w, cursor_6_h)
            self.ui.mark_cursor_6.setFont(self.font)

            width_x5, heigh_y5 = cursor_5_w / 2, cursor_5_h / 2
            width_x6, heigh_y6 = cursor_6_w / 2, cursor_6_h / 2


        
            self.x5, self.y5 = cursor_5_x + width_x5, cursor_5_y + heigh_y5    
            self.x6, self.y6 = cursor_6_x + width_x6, cursor_6_y + heigh_y6

            self.ui.mark_cursor_5.center_x, self.ui.mark_cursor_5.center_y = self.x5, self.y5
            self.ui.mark_cursor_6.center_x, self.ui.mark_cursor_6.center_y = self.x6, self.y6

            width_inf_5=self.ui.label_5_inf.geometry().width()*self.w_frame_rate
            width_inf_6=self.ui.label_6_inf.geometry().width()*self.w_frame_rate

            self.ui.label_5_inf.resize(width_inf_5,30)
            self.ui.label_6_inf.resize(width_inf_6,30)
         





            # "2d": [[558.0, 627.0], [755.0, 603.0], [1191.0, 695.0], [961.0, 752.0]], 
        


    def init_w_h(self, x1, y1, x2, y2, x4, y4,ui_width,ui_high):
        width_x = round((math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)) / self.scale, 1)
        height_y = round((math.sqrt((x4 - x1) ** 2 + (y4 - y1) ** 2)) / self.scale, 1)
        # print("画线宽高",width_x,height_y)
        ui_width.setText(str(width_x))
        ui_high.setText(str(height_y))
        ui_width.move(x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 2)
        ui_high.move(x1 + (x4 - x1) / 2, y1 + (y4 - y1) / 2)

        self.ui.label_1_inf.setText(" P1, X:"+str(int(self.ui.mark_cursor_1.center_x))+", Y: "+str(int(self.ui.mark_cursor_1.center_y)))
        self.ui.label_2_inf.setText(" P2, X:"+str(int(self.ui.mark_cursor_2.center_x))+", Y: "+str(int(self.ui.mark_cursor_2.center_y)))
        self.ui.label_3_inf.setText(" P3, X:"+str(int(self.ui.mark_cursor_3.center_x))+", Y: "+str(int(self.ui.mark_cursor_3.center_y)))
        self.ui.label_4_inf.setText(" P4, X:"+str(int(self.ui.mark_cursor_4.center_x))+", Y: "+str(int(self.ui.mark_cursor_4.center_y)))

        
        self.ui.label_1_inf.move(self.ui.mark_cursor_1.center_x, self.ui.mark_cursor_1.center_y)
        self.ui.label_2_inf.move(self.ui.mark_cursor_2.center_x, self.ui.mark_cursor_2.center_y)
        self.ui.label_3_inf.move(self.ui.mark_cursor_3.center_x, self.ui.mark_cursor_3.center_y)
        self.ui.label_4_inf.move(self.ui.mark_cursor_4.center_x, self.ui.mark_cursor_4.center_y)

        if self.number_point == 6:

            self.ui.label_5_inf.setText(" P5, X:"+str(int(self.ui.mark_cursor_5.center_x))+", Y: "+str(int(self.ui.mark_cursor_5.center_y)))
            self.ui.label_6_inf.setText(" P6, X:"+str(int(self.ui.mark_cursor_6.center_x))+", Y: "+str(int(self.ui.mark_cursor_6.center_y)))
     

            
            self.ui.label_5_inf.move(self.ui.mark_cursor_5.center_x, self.ui.mark_cursor_5.center_y)
            self.ui.label_6_inf.move(self.ui.mark_cursor_6.center_x, self.ui.mark_cursor_6.center_y)

    def on_click(self,x, y, button, is_press):

        
        self.get_plane_grid(self.data["K"],self.data["R"],self.data["T"],self.x_range,self.y_range)
        # self.ui._2d_point.show()
        print("is_listening")
        
        

    def mouse_listener(self,_data,x_range,y_range):
        self.data=dict()
        self.data=_data
        self.x_range=x_range
        self.y_range=y_range
        # self.listener = Listener(on_click=self.on_click)
        # self.listener.start()
            
        time.sleep(1)
    # def mouse_listener_stop(self):
    #     self.listener.stop()

    

    # def init_point_info():
    def get_plane_grid(self,K,R,T,x_range,y_range):
        self.point_list_x.clear()
        self.point_list_y.clear()

        camera = CVCamera(K, R, T)

        # x_range=[0, 5, 1]
        # y_range=[0, 1, 0.1]

        xx = np.arange(x_range[0], x_range[1], x_range[2])
        yy = np.arange(y_range[0], y_range[1], y_range[2])
        XX = xx.shape[0]
        YY = yy.shape[0]
        x3ds = np.zeros((XX, YY, 3))    
        for i in range(XX):
            for j in range(YY):
                x3ds[i, j, :2] = [xx[i], yy[j]]
        x2ds = camera.world_to_screen(x3ds.reshape(-1, 3)).reshape(XX,YY,2)#.astype(np.int32)
        N=len(xx)
        M=len(yy)

        x2ds.reshape(N,M,-1)
        
        for i in range(N):

            p0=np.array(x2ds[i,0],dtype=np.float64)
            p1=np.array(x2ds[i,-1],dtype=np.float64)
            self.point_list_x.append(p0)
            self.point_list_x.append(p1)

        for j in range(M):
            p0=np.array(x2ds[0,j],dtype=np.float64)
            p1=np.array(x2ds[-1,j],dtype=np.float64)
            self.point_list_y.append(p0)
            self.point_list_y.append(p1)


        # self.signal_change.emit()


    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        pen = QPen(Qt.green, 2, Qt.DotLine)
        painter.setPen(pen)

        for i in range(len(self.point_list_x)-1):
            if i%2==0:

                self.ui.mark_cursor_rx = CurcleLable(self)
                self.ui.mark_cursor_ry= CurcleLable(self)


                self.ui.mark_cursor_rx.center_x=self.point_list_x[i][0]
                self.ui.mark_cursor_rx.center_y=self.point_list_x[i][1]

                self.ui.mark_cursor_ry.center_x=self.point_list_x[i+1][0]
                self.ui.mark_cursor_ry.center_y=self.point_list_x[i+1][1]

                painter.drawLine(self.ui.mark_cursor_rx.center_x, self.ui.mark_cursor_rx.center_y,
                                self.ui.mark_cursor_ry.center_x, self.ui.mark_cursor_ry.center_y)


        for j in range(len(self.point_list_y)-1):
            if j%2==0:
                # self.ui.mark_cursor_3 = CurcleLable(self)
                # self.ui.mark_cursor_4 = CurcleLable(self)

                self.ui.mark_cursor_rx.center_x=self.point_list_y[j][0]
                self.ui.mark_cursor_rx.center_y=self.point_list_y[j][1]

                self.ui.mark_cursor_ry.center_x=self.point_list_y[j+1][0]
                self.ui.mark_cursor_ry.center_y=self.point_list_y[j+1][1]

                painter.drawLine(self.ui.mark_cursor_rx.center_x, self.ui.mark_cursor_rx.center_y,
                            self.ui.mark_cursor_ry.center_x, self.ui.mark_cursor_ry.center_y)

            

        self.update()




    def get_point_position(self):
        # print(self.ui.label_1_inf.text())
        # _point_position={"p1":{"x":self.ui.mark_cursor_1.center_x,"y":self.ui.mark_cursor_1.center_y},
        # "p2":{"x":self.ui.mark_cursor_2.center_x,"y":self.ui.mark_cursor_2.center_y},
        # "p3":{"x":self.ui.mark_cursor_3.center_x,"y":self.ui.mark_cursor_3.center_y},
        # "p4":{"x":self.ui.mark_cursor_4.center_x,"y":self.ui.mark_cursor_4.center_y}}
        _point_position=[[self.ui.mark_cursor_1.center_x,self.ui.mark_cursor_1.center_y],
        [self.ui.mark_cursor_2.center_x,self.ui.mark_cursor_2.center_y],
        [self.ui.mark_cursor_3.center_x,self.ui.mark_cursor_3.center_y],
        [self.ui.mark_cursor_4.center_x,self.ui.mark_cursor_4.center_y]]

        if self.number_point == 6:
            _point_position=[[self.ui.mark_cursor_1.center_x,self.ui.mark_cursor_1.center_y],
        [self.ui.mark_cursor_2.center_x,self.ui.mark_cursor_2.center_y],
        [self.ui.mark_cursor_3.center_x,self.ui.mark_cursor_3.center_y],
        [self.ui.mark_cursor_4.center_x,self.ui.mark_cursor_4.center_y],
        [self.ui.mark_cursor_5.center_x,self.ui.mark_cursor_5.center_y],
        [self.ui.mark_cursor_6.center_x,self.ui.mark_cursor_6.center_y]]


        return _point_position

class CurcleLable(QLabel):
    myMouseDrag = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.flag = None
        self.new_x = 0
        self.new_y = 0
        self.center_x = 20
        self.center_y = 20
        self.last_x = 0
        self.info = None

    # 鼠标点击事件
    def mousePressEvent(self, event):
        self.flag = True
        self.last_x = (event.x(), event.y())

    # 鼠标移动事件
    def mouseMoveEvent(self, event):
        # scale = var.global_scale
        if self.flag:
            x, y = event.x(), event.y()
            dx, dy = x - self.last_x[0], y - self.last_x[1]
            geom = self.geometry()
            self.new_x = geom.x() + dx
            self.new_y = geom.y() + dy
            # self.setGeometry(new_x,new_y,geom.width(),geom.height())
            self.center_x, self.center_y = self.new_x + geom.width() / 2, self.new_y + geom.height() / 2
            self.move(self.new_x, self.new_y)
            self.myMouseDrag.emit()
            # print(self.new_x, self.new_y)
            # print(self.center_x, self.center_y)

    # 鼠标释放事件
    def mouseReleaseEvent(self, event):
        self.flag = False
        # print(self.new_x, self.new_y)
        # print(self.center_x, self.center_y)


    def mouseDoubleClickEvent(self, event):
        mouse_pos = self.mapToParent(event.pos())
        if mouse_pos.x() > self.center_x+3:
            self.center_x += 1
            self.new_x += 1
        elif mouse_pos.x() < self.center_x-3:
            self.center_x -= 1
            self.new_x -= 1
        elif mouse_pos.y() > self.center_y+3:
            self.center_y += 1
            self.new_y += 1
        elif mouse_pos.y() < self.center_y-3:
            self.center_y -= 1
            self.new_y -= 1

        self.myMouseDrag.emit()
        self.move(self.new_x,self.new_y)

    




if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = PWidget()
    # window.resize(400, 200)
    window.show()
    sys.exit(app.exec_())
